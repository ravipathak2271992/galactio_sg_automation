import io.appium.java_client.TouchAction;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by QI 33 on 17-05-2016.
 */
public class REPORTED_INCIDENTS_FUNCTION {

    @Test(priority = 1)
    public void C001_Verify_the_Report_Incident_from_Options_When_User_is_Not_Login() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();
        // Click on Option Button
        TestBase.getObject("Option_Button").click();
        // Click on Reported_Incidents Button
        TestBase.getObject("Verify_Reported_Incidents").click();
        Thread.sleep(2000);
        //Screenshot for Favourites_Weather_Screen
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C001_Verify_the_Report_Incident_from_Options_When_User_is_Not_Login/" + "Pop Up Login map synq account" + ".jpg");//Before login into Map synq account
    }

    @Test(priority = 2)
    public void C002_Verify_the_Report_Incident_from_Options_When_User_is_Login() throws Exception {

        TestBase.getObject("Option_Button").click();

        //Call login_function from TesBase class for login into mapsynq account
        TestBase.login_function();
        TestBase.getObject("Back_button").click();

        // TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C002_Verify_the_Report_Incident_from_Options_When_User_is_Login/" + "Report_Screen" + ".jpg");

    }


    @Test(priority = 3)
    public void C003_Verify_the_Report_Incident_Page_UI() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();
        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();

        //Verify  for first three options under TYPE section
        Assert.assertTrue(TestBase.isElementPresent("Type_Text"), "C002:Type_Text is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Congestion_Text"), "C002:Congestion_text is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Accident_Text"), "C002:Accident_Text is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Closure_Text"), "C002:Closure_Text is not present in Report screen page");

        //screenshot for mainly verify the under Type options logo for first three options
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C003_Verify_the_Report_Incident_Page_UI/" + "Report_Screen_1" + ".jpg");

        //swipe function from closure to congestion
        TouchAction action = new TouchAction(TestBase.driver);
        WebElement element1 = TestBase.getObject("Verify_Closure_Text");
        int startY = element1.getLocation().getY() + (element1.getSize().getHeight() / 2);
        int startX = element1.getLocation().getX() + (element1.getSize().getWidth() / 2);
        WebElement element2 = TestBase.getObject("Verify_Congestion_Text");
        int endX = element2.getLocation().getX() + (element2.getSize().getWidth() / 2);
        int endY = element2.getLocation().getY() + (element2.getSize().getHeight() / 2);
        action.press(startX, startY).waitAction(2000).moveTo(endX, endY).release().perform();

        //Verify the next two options under TYPE section
        Assert.assertTrue(TestBase.isElementPresent("Verify_Breakdown_Text"), "C002:Breakdown_Text is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Flashflood_Text"), "C002:Flashflood_Text is not present in Report screen page");

        //screenshot for mainly verify the under Type options logo for next two options
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C003_Verify_the_Report_Incident_Page_UI/" + "Report_Screen_1" + ".jpg");

        //swipe function from  flashfoood to closure
        WebElement element3 = TestBase.getObject("Verify_Flashflood_Text");
        int start_Y = element3.getLocation().getY() + (element1.getSize().getHeight() / 2);
        int start_X = element3.getLocation().getX() + (element1.getSize().getWidth() / 2);
        action.press(start_X, start_Y).waitAction(2000).moveTo(endX, endY).release().perform();

        //Verify last two options
        Assert.assertTrue(TestBase.isElementPresent("Verify_Roadwork_Text"), "C002:Roadwork_Text is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Others_Text"), "C002:Others_Text is not present in Report screen page");

        //screenshot for mainly verify the under Type options logo for last two options
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C003_Verify_the_Report_Incident_Page_UI/" + "Report_Screen_1" + ".jpg");

        //Verify Lane Type options
        Assert.assertTrue(TestBase.isElementPresent("Lane_Text"), "C002:Lane_Text is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Current_Text"), "C002:Current_Text is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Opposite_Text"), "C002:Opposite_Text is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Others_Roads_Text"), "C002:Others_Roads_Text is not present in Report screen page");

        //Verify How Far options
        Assert.assertTrue(TestBase.isElementPresent("How_Far_Text"), "C002:How_Far_Text is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Behind_Text"), "C002:Behind_Text is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Ahead_Text"), "C002:Ahead_Text is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Distance_1"), "C002:Distance_1 is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Distance_2"), "C002:Distance_2 not present in Report screen page");

        // Screenshot for full report screen page
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C003_Verify_the_Report_Incident_Page_UI/" + "Full_Report_Screen_1" + ".jpg");

        // scroll to details field and Verify Details options
        TestBase.driver.scrollTo("Describe the incident here ...");
        Assert.assertTrue(TestBase.isElementPresent("Details_Text"), "C002:Details_Text is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Describe_the_incidents_here_text"), "C002:Describe_the_incidents_here_text is not present in Report screen page");

        //Verify Add photo and submit button
        Assert.assertTrue(TestBase.isElementPresent("Add_Photo_Button"), "C002:Add_Photo_Button is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Submit_Button"), "C002:Submit_Button is not present in Report screen page");
        // Screenshot for full report screen page
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C003_Verify_the_Report_Incident_Page_UI/" + "Full_Report_Screen_2" + ".jpg");
    }



    @Test(priority = 4)
    public void C004_Checking_the_Report_Incident_Functionality_For_RoadWorks_Using_Opposite_Lane() throws Exception {
        //click
        //TestBase.getObject("Verify_Congestion_Text").click();
        //  TestBase.getObject("Verify_Accident_Text").click();
        TestBase.getObject("Verify_Roadwork_Text").click();


        TestBase.getObject("Opposite_Text").click();
        TestBase.getObject("Far_Ahead_Button").click();
        Thread.sleep(1000);
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C004_Checking_the_Report_Incident_Functionality_For_RoadWorks/" + "" + ".jpg");
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C004_Checking_the_Report_Incident_Functionality_For_RoadWorks/" + "Details text  and image " + ".jpg");
        TestBase.getObject("Submit_Button").click();
        Thread.sleep(7000);
        Assert.assertTrue(TestBase.isElementPresent("Succesfully_Submitted_The_Report_text"), "C002:Submit_Button is not present in Report screen page");
        TestBase.getObject("Close_Button").click();
        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Incidents_Button").click();
        TestBase.getObject("Verify_User_Reported_Tab").click();
            TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C004_Verify_the_Add_Photo_function/" + "User reported screen for congestion road" + ".jpg");
        TestBase.getObject("Back_button").click();
    }


    @Test(priority = 5)
    public void C005_Checking_the_Report_Incident_Functionality_For_Accident() throws Exception {

        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TestBase.getObject("Verify_Congestion_Text").click();

        TestBase.getObject("Current_Text").click();
        TestBase.getObject("Far_Behind_Button").click();
        Thread.sleep(1000);
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C005_Checking_the_Report_Incident_Functionality_For_Second_Accident/" + "Report_incident_for_accident" + ".jpg");
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C005_Checking_the_Report_Incident_Functionality_For_Second_Accident/" + "Details for text and image " + ".jpg");
        TestBase.getObject("Submit_Button").click();
        Thread.sleep(7000);
        Assert.assertTrue(TestBase.isElementPresent("Succesfully_Submitted_The_Report_text"), "C002:Submit_Button is not present in Report screen page");
        TestBase.getObject("Close_Button").click();
        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Incidents_Button").click();
        TestBase.getObject("Verify_User_Reported_Tab").click();
        Assert.assertTrue(TestBase.isElementPresent("Verify_Accident_Text"), "C002:Submit_Button is not present in Report screen page");
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C005_Checking_the_Report_Incident_Functionality_For_Second_Accident/" + "User reported screen for accident road" + ".jpg");
        TestBase.getObject("Back_button").click();
    }

    @Test(priority = 6)
    public void C006_Checking_the_Report_Incident_Functionality_For_Closure() throws Exception {
        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TestBase.getObject("Verify_Closure_Text").click();

        TestBase.getObject("Others_Roads_Text").click();
        TestBase.getObject("Far_Behind_Button").click();
        Thread.sleep(1000);
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C006_Checking_the_Report_Incident_Functionality_For_Closure/" + "Report_incident_for_closure" + ".jpg");
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C006_Checking_the_Report_Incident_Functionality_For_Closure/" + "Details text and image " + ".jpg");
        TestBase.getObject("Submit_Button").click();
        Thread.sleep(7000);
        Assert.assertTrue(TestBase.isElementPresent("Succesfully_Submitted_The_Report_text"), "C002:Submit_Button is not present in Report screen page");
        TestBase.getObject("Close_Button").click();
        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Incidents_Button").click();
        TestBase.getObject("Verify_User_Reported_Tab").click();
        Assert.assertTrue(TestBase.isElementPresent("Verify_Closure_Text"), "C002:Submit_Button is not present in Report screen page");
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C006_Checking_the_Report_Incident_Functionality_For_Closure_Accident/" + "User reported screen fo closure road" + ".jpg");
        TestBase.getObject("Back_button").click();
    }


    @Test(priority = 7)
    public void C007_Checking_the_Report_Incident_Functionality_For_Breakdown() throws Exception {

        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TouchAction action = new TouchAction(TestBase.driver);
        WebElement element1 = TestBase.getObject("Verify_Closure_Text");
        int startY = element1.getLocation().getY() + (element1.getSize().getHeight() / 2);
        int startX = element1.getLocation().getX() + (element1.getSize().getWidth() / 2);
        WebElement element2 = TestBase.getObject("Verify_Congestion_Text");
        int endX = element2.getLocation().getX() + (element2.getSize().getWidth() / 2);
        int endY = element2.getLocation().getY() + (element2.getSize().getHeight() / 2);
        action.press(startX, startY).waitAction(2000).moveTo(endX, endY).release().perform();
        TestBase.getObject("Verify_Breakdown_Text").click();
        TestBase.getObject("Current_Text").click();
        TestBase.getObject("Far_Behind_Button").click();
        Thread.sleep(1000);
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C005_Checking_the_Report_Incident_Functionality_For_Second_Accident/" + "Report_incident_for_accident_1" + ".jpg");
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C005_Checking_the_Report_Incident_Functionality_For_Second_Accident/" + "Details text and image " + ".jpg");
        TestBase.getObject("Submit_Button").click();
        Thread.sleep(7000);
        Assert.assertTrue(TestBase.isElementPresent("Succesfully_Submitted_The_Report_text"), "C002:Submit_Button is not present in Report screen page");
        TestBase.getObject("Close_Button").click();
        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Incidents_Button").click();
        TestBase.getObject("Verify_User_Reported_Tab").click();
        Assert.assertTrue(TestBase.isElementPresent("Verify_Breakdown_Text"), "C002:Submit_Button is not present in Report screen page");
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C005_Checking_the_Report_Incident_Functionality_For_Second_Accident/" + "User reported screen for accident road" + ".jpg");
        TestBase.getObject("Back_button").click();
    }

    @Test(priority = 8)
    public void C008_Checking_the_Report_Incident_Functionality_For_Others() throws Exception {
        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TouchAction action = new TouchAction(TestBase.driver);

        WebElement element1 = TestBase.getObject("Verify_Closure_Text");
        int startY = element1.getLocation().getY() + (element1.getSize().getHeight() / 2);
        int startX = element1.getLocation().getX() + (element1.getSize().getWidth() / 2);
        WebElement element2 = TestBase.getObject("Verify_Congestion_Text");
        int endX = element2.getLocation().getX() + (element2.getSize().getWidth() / 2);
        int endY = element2.getLocation().getY() + (element2.getSize().getHeight() / 2);
        action.press(startX, startY).waitAction(2000).moveTo(endX, endY).release().perform();

        //swipe function from  flashfoood to closure
        WebElement element3 = TestBase.getObject("Verify_Flashflood_Text");
        int start_Y = element3.getLocation().getY() + (element1.getSize().getHeight() / 2);
        int start_X = element3.getLocation().getX() + (element1.getSize().getWidth() / 2);
        action.press(start_X, start_Y).waitAction(2000).moveTo(endX, endY).release().perform();

        TestBase.getObject("Verify_Others_Text").click();
        TestBase.getObject("Opposite_Text").click();
        Thread.sleep(1000);
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C005_Checking_the_Report_Incident_Functionality_For_Second_Accident/" + "Report_incident_for_accident_1" + ".jpg");
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C005_Checking_the_Report_Incident_Functionality_For_Second_Accident/" + "Details text and image " + ".jpg");
        TestBase.getObject("Camera_Image").click();

        Assert.assertTrue(TestBase.isElementPresent("Do_You_Want_Image_Text"), "C002:Submit_Button is not present in Report screen page");
        TestBase.getObject("Cancel_Button").click();

        TestBase.getObject("Take_New_Button").click();
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
        Thread.sleep(7000);
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C008_Checking_the_Report_Incident_Functionality_For_Breakdown/" + "Take new image and replace the older one " + ".jpg");

        TestBase.getObject("Camera_Image").click();
        TestBase.getObject("Delete_Button").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C008_Checking_the_Report_Incident_Functionality_For_Breakdown/" + "Checking the add photo section after deleting " + ".jpg");

        TestBase.getObject("Submit_Button").click();
        Thread.sleep(7000);
        Assert.assertTrue(TestBase.isElementPresent("Succesfully_Submitted_The_Report_text"), "C002:Submit_Button is not present in Report screen page");
        TestBase.getObject("Close_Button").click();
        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Incidents_Button").click();
        TestBase.getObject("Verify_User_Reported_Tab").click();
        Assert.assertTrue(TestBase.isElementPresent("Verify_Others_Text"), "C002:Submit_Button is not present in Report screen page");
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C005_Checking_the_Report_Incident_Functionality_For_Second_Accident/" + "User reported screen for road work " + ".jpg");
        TestBase.getObject("Back_button").click();
    }

}
