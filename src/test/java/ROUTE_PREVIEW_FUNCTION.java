import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

/**
 * Created by QI 33 on 05-05-2016.
 */
public class ROUTE_PREVIEW_FUNCTION {
    @Test(priority = 1)

    public void C001_Creating_Route_Preview() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();
        TestBase.getObject("Search_option").click();
        TestBase.getObject("POI").click();
        TestBase.getObject("Restaurant").click();
        TestBase.getObject("Fast_Food_Text").click();
        TestBase.getObject("RestaurantSubcategories1").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C001_Creating_Route_Preview/" + "Map view screen By POI" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Preview_Button"), "C001:Preview text is not present in the Map_View_Screen");
        TestBase.getObject("Preview_Button").click();
        Thread.sleep(3000);
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C001_Creating_Route_Preview/" + "Route_Preview_Screen" + ".jpg");

    }
    @Test(priority = 2)

    public void C002_Checking_the_RoutePreview_Screen_Function() throws Exception {
        Assert.assertTrue(TestBase.isElementPresent("Route_Preview_Text"), "C001:Route_Preview text is not present in the header of Route preview screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Destination_Text"), "C002:Destination text is not present in the Map_View_Screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Destination_Name"), "C003:Destination text is not present in the Map_View_Screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Fastest_Text"), "C004:Fastest text is not present in the Map_View_Screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Shortest_Text"), "C005:Shortest text is not present in the Map_View_Screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Toll_Avoidance_Text"), "C007:Toll_Avoidance text is not present in the Map_View_Screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Toll_Minimised_Text"), "C008:Toll_Minimised text is not present in the Map_View_Screen");
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C002_Checking_the_RoutePreview_Screen_Function/" + "swipe1" + ".jpg");
        TestBase.swipe_function();
        Assert.assertTrue(TestBase.isElementPresent("Verify_Traffic_Aware_Text"), "C009:Traffic_Aware text is not present in the Map_View_Screen");
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C002_Checking_the_RoutePreview_Screen_Function/" + "swipe2" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Go_Text"), "C0010:Go_Text is not present in the Map_View_Screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Simulate_Text"), "C0011:Simulate_Tex is not present in the Map_View_Screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Zoom_Out_button"), "C0012:Zoom_Out_button is not present in the Map_View_Screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Zoom_In_button"), "C0013:Zoom_In_button is not present in the Map_View_Screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Traffic_Aware_button"), "C0014:Traffic_Aware_button is not present in the Map_View_Screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Current_Location_button"), "C0015:Current_Location_button is not present in the Map_View_Screen");
        TestBase.getObject("Back_button").click();
        Thread.sleep(4000);
        Assert.assertFalse(TestBase.isElementPresent("Map_View_text"), "C002:Map_View screen  is not present after back from preview screen");
        for(int i=0;i<4;i++){
            TestBase.getObject("Back_button").click();
        }
    }
    @Test(priority = 3)

    public void C003_Checking_the_functionality_for_Route_Preview_Screen() throws Exception {

        TestBase.getObject("Search_option").click();
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Location_1"));
        TestBase.getObject("Search_button").click();
        TestBase.getObject("Location_Id").click();
        TestBase.getObject("Preview_Button").click();
        TestBase.getObject("Verify_Fastest_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Fastest_Route_Preview_View_screen_Location_1" + ".jpg");
        TestBase.getObject("Verify_Shortest_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Shortest_Route_Preview_View_screen_Location_1" + ".jpg");
        TestBase.getObject("Verify_Toll_Avoidance_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Toll_Avoidance_Route_Preview_View_screen_Location_1" + ".jpg");
        TestBase.swipe_function();
        TestBase.getObject("Verify_Toll_Minimised_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Toll_Minimised_Route_Preview_View_screen_Location_1" + ".jpg");
        TestBase.getObject("Verify_Traffic_Aware_Text").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Traffic_Aware_Route_Preview_View_screen_Location_1" + ".jpg");

        for(int i=0;i<3;i++){
        TestBase.getObject("Verify_Zoom_Out_button").click();}
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Max_Zoom_Out_Route_Preview_Screen_without_traffic_aware" + ".jpg");

        for(int i=0;i<=7;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();}
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Max_Zoom_In_Route_Preview_Screen" + ".jpg");
        TestBase.getObject("Verify_Current_Location_button").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Current_Location_Preview_Screen" + ".jpg");
        for(int i=0;i<3;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();}
        for(int i=0;i<=10;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();}

        for(int i=1;i<=3;i++){
            TestBase.getObject("Back_button").click();
        }


        //Route preview for location 2
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Location_2"));
        TestBase.getObject("Search_button").click();
        TestBase.getObject("Location_Id").click();
        TestBase.getObject("Preview_Button").click();
        Thread.sleep(3000);
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Comparing Fastest_Route and shortest_Route_for _Location_2" + ".jpg");

        for(int i=0;i<3;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();
        }

        for(int i=0;i<=10;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();
        }

        for(int i=1;i<=3;i++){
            TestBase.getObject("Back_button").click();
        }

        //Route preview for location 3
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Location_3"));
        TestBase.getObject("Search_button").click();
        TestBase.getObject("Location_Id").click();
        TestBase.getObject("Preview_Button").click();
        Thread.sleep(3000);
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Comparing Fastest_Route and shortest_Route_for _Location_3" + ".jpg");

        for(int i=0;i<3;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();
        }

        for(int i=0;i<=10;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();
        }

        for(int i=1;i<=3;i++){
            TestBase.getObject("Back_button").click();
        }

        //Route preview for location 4
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Location_4"));
        TestBase.getObject("Search_button").click();
        TestBase.getObject("Location_Id").click();
        TestBase.getObject("Preview_Button").click();
        Thread.sleep(3000);
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Comparing Fastest_Route and shortest_Route_for _Location_4" + ".jpg");

        for(int i=0;i<3;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();
        }

        for(int i=0;i<=10;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();
        }

        for(int i=1;i<=3;i++){
            TestBase.getObject("Back_button").click();
        }

        //Route preview for location 5
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Location_5"));
        TestBase.getObject("Search_button").click();
        TestBase.getObject("Location_Id").click();
        TestBase.getObject("Preview_Button").click();
        Thread.sleep(3000);
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Comparing Fastest_Route and shortest_Route_for _Location_5" + ".jpg");

        for(int i=0;i<3;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();
        }

        for(int i=0;i<=10;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();
        }

        for(int i=1;i<=3;i++){
            TestBase.getObject("Back_button").click();
        }

        //Route preview for location 6
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Location_6"));
        TestBase.getObject("Search_button").click();
        TestBase.getObject("Location_Id").click();
        TestBase.getObject("Preview_Button").click();
        Thread.sleep(3000);
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Comparing Fastest_Route and shortest_Route_for _Location_6" + ".jpg");

        for(int i=0;i<3;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();}

        for(int i=0;i<=10;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();}

        for(int i=1;i<=4;i++){
            TestBase.getObject("Back_button").click();
        }
        TestBase.clear_data();
        }

    @AfterTest
    public void quit(){

        if((TestBase.driver)!=null){
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        TestBase.driver.quit();
        System.out.println("exit");
    }

}
