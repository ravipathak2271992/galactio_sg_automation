/**
 * Created by QI 33 on 13-12-2016.
 */

    import Negative_Test_Cases.*;
   // import Negative_Test_Cases.TestBase;
    //import Negative_Test_Cases.TestBase;
    import io.appium.java_client.MobileElement;
    import io.appium.java_client.TouchAction;
    import net.sourceforge.tess4j.Tesseract;
    import net.sourceforge.tess4j.TesseractException;
    import org.openqa.selenium.*;
    import org.testng.Assert;
    import org.testng.annotations.AfterTest;
    import org.testng.annotations.Test;
    import org.testng.asserts.SoftAssert;

    import java.io.File;
    import java.io.IOException;
    import java.util.HashMap;
    import java.util.List;

    //import static Negative_Test_Cases.TestBase.driver;


public class FAVOURITE_FUNCTION {

    @Test(priority = 1)

    public void C001_Accessing_Favourite_from_Main_Menu() throws Exception {


        SoftAssert softAssert= new SoftAssert();
        TestBase utill = new TestBase();
        utill.initialize();
        softAssert.assertTrue(TestBase.isElementPresent("Favourites"), "C001: Favourites_text is not present in the Set Home popup");
        TestBase.getObject("Favourites").click();
        softAssert.assertTrue(TestBase.isElementPresent("no_favourites_msg1"), "C001: No Favourites yet... is not present in the Set Home popup");
        //Assert.assertTrue(TestBase.isElementPresent("no_favourites_msg2"), "C001: Pull to Sync is not present in the Set Home popup");

        TestBase.getObject("Back_button").click();
        TestBase.getObject("Search_option").click();

        //Add two locations for hospital POI as a favourite and take screenshots in map view screen
        TestBase.getObject("POI").click();
        TestBase.getObject("Emergency_Text").click();

        //First location
        TestBase.getObject("Hospital").click();
        TestBase.getObject("HospitalSubcategories1").click();


        String POI_1 = TestBase.getObject("verify_POI").getText();
        TestBase.getObject("Favourite_Star_Button").click();
        System.out.println(POI_1);
        TestBase.getObject("Ok_Button").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C001_Accessing_Favourite_from_Main_Menu/" + "Saved to Favourites toast message for Hospital_Location_1" + ".jpg");
        TestBase.getObject("Back_button").click();

        //Second location
        TestBase.getObject("HospitalSubcategories2").click();
        String POI2 = TestBase.getObject("verify_POI").getText();
        TestBase.getObject("Favourite_Star_Button").click();
        System.out.println(POI2);
        TestBase.getObject("Ok_Button").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C001_Accessing_Favourite_from_Main_Menu/" + "Saved to Favourites toast message for Hospital_Location_2" + ".jpg");

        //click back button four times
        for (int i = 1; i <=5; i++) {
            TestBase.getObject("Back_button").click();
        }

        //click Favourites button and verify that two location added as favourite is same or not
        TestBase.getObject("Favourites").click();
        if((TestBase.getObject("HospitalSubcategories1").getText().equals(POI_1))&&(TestBase.getObject("HospitalSubcategories2").getText().equals(POI2)))
        {
            System.out.println("passed");
        }else{
            softAssert.assertTrue(1>2, "Wrong Favourites are added in Favourites section");
        }
        Thread.sleep(2000);
        TestBase.getObject("Back_button").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C001_Accessing_Favourite_from_Main_Menu/" + "Favourites_items_On_Suggested For You Box" + ".jpg");
        Thread.sleep(2000);
        TestBase.clear_data();
        TestBase.getObject("Back_button").click();

    }



    @Test(priority = 2)

    public void C002_Accessing_Favourite_from_Search_page_when_DataAvailable() throws Exception {

        SoftAssert softAssert= new SoftAssert();
        TestBase.getObject("Search_option").click();
        TestBase.getObject("POI").click();

        //Add Policestation  POI as a favourite and take screenshots for saved to favourite toast message
        TestBase.getObject("Emergency_Text").click();
        TestBase.getObject("Policestation").click();
        TestBase.getObject("PolicestationSubcategories1").click();
        String POI1 = TestBase.getObject("verify_POI").getText();
        TestBase.getObject("Favourite_Star_Button").click();
        TestBase.getObject("Ok_Button").click();
        Thread.sleep(1000);
        System.out.println(POI1);
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C002_Accessing_Favourite_from_Search_page_when_DataAvailable/" + "Saved to favourite toast message for first location" + ".jpg");
        TestBase.getObject("Back_button").click();

        //Add two locations for POI as a favourite and take screenshots for saved to favourite toast message
        TestBase.getObject("PolicestationSubcategories2").click();
        TestBase.getObject("Favourite_Star_Button").click();
        TestBase.getObject("Ok_Button").click();
        Thread.sleep(1000);
        String POI2 = TestBase.getObject("verify_POI").getText();
        System.out.println(POI2);
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C002_Accessing_Favourite_from_Search_page_when_DataAvailable/" + "Saved to favourite toast message for second location" + ".jpg");
        for (int i = 1; i < 5; i++) {
            TestBase.getObject("Back_button").click();
        }

        //Add Coordinates as favourite
        TestBase.getObject("Coordinates").click();
        TestBase.getObject("Latitude").click();
        TestBase.getObject("Latitude").sendKeys(TestBase.TestData.getProperty("Latitude1"));
        TestBase.getObject("Longitude").click();
        TestBase.getObject("Longitude").sendKeys(TestBase.TestData.getProperty("Longitude1"));
        TestBase.getObject("Ok_Button").click();
        TestBase.getObject("Favourite_Star_Button").click();

        TestBase.getObject("Add_To_Favourite_Edit_Field").click();
        TestBase.getObject("Add_To_Favourite_Edit_Field").sendKeys(TestBase.TestData.getProperty("Ravi"));
        TestBase.getObject("Ok_Button").click();
        Thread.sleep(1000);
        String POI_Coordinate = TestBase.getObject("verify_POI_address").getText();
        System.out.println(POI_Coordinate);
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C002_Accessing_Favourite_from_Search_page_when_DataAvailable/" + "Saved to favourite toast message for coordinates location" + ".jpg");

        //click back button thrice
        for (int i = 1; i <= 3; i++) {
            TestBase.getObject("Back_button").click();

        }

        TestBase.getObject("Favourites").click();

        //click Favourites button and verify that three locations are  added as favourite is same or not
        if((TestBase.getObject("PolicestationSubcategories1").getText().equals(POI1))&&(TestBase.getObject("PolicestationSubcategories2").getText().equals(POI2)))
        {
            System.out.println("passed");
        }else{
            softAssert.assertTrue(1>2, "Wrong Favourites are added in Favourites section");
        }

        TestBase.getObject("Back_button").click();

        softAssert.assertAll();


    }



    @Test(priority = 3)

    public void C003_Checking_the_display_of_Favourite_window() throws Exception {

        SoftAssert softAssert= new SoftAssert();
        TestBase.getObject("Favourites").click();
        softAssert.assertTrue(TestBase.isElementPresent("Favourites"), "C003: Favourites text is not present in the header of the favourite screen from dashboard");
        softAssert.assertTrue(TestBase.isElementPresent("Sorting_button"), "C003: Sorting_button is not present in the the favourite screen from dashboard");
        softAssert.assertTrue(TestBase.isElementPresent("Back_button"), "C003: Back_button is not present in the the favourite screen from dashboard");
        TestBase.getObject("Back_button").click();


        TestBase.getObject("Search_option").click();
        TestBase.getObject("Favourites").click();
        softAssert.assertTrue(TestBase.isElementPresent("Favourites"), "C003: Favourites text is not present in the header of the favourite screen from search destination");
        softAssert.assertTrue(TestBase.isElementPresent("Sorting_button"), "C003: Sorting_button is not present in the the favourite screen from search destination");
        softAssert.assertTrue(TestBase.isElementPresent("Back_button"), "C003: Back_button is not present in the the favourite screen from search destination");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.clear_data();
        TestBase.getObject("Back_button").click();
        softAssert.assertAll();
    }






    @Test(priority = 4)

    public void C004_Adding_Location_In_Favourite_List_and_Checking_The_Successful_Addition_In_Favourite_List() throws Exception {

        SoftAssert softAssert= new SoftAssert();

        TestBase.getObject("Search_option").click();


        //Add favourite by search destination for location_1
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Location_1"));
        TestBase.selectFavourite();
        Thread.sleep(1000);

        String POI_1=TestBase.getObject("verify_POI").getText();
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C004_Adding_Location_In_Favourite_List_and_Checking_The_Successful_Addition_In_Favourite_List/" + "Saved to favourite toast message for coordinates location_1" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Clear_Data_button").click();

        //Add favourite by search destination for location_2
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Location_2"));
        TestBase.selectFavourite();
        Thread.sleep(1000);
        String POI_2=TestBase.getObject("verify_POI").getText();


        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C004_Adding_Location_In_Favourite_List_and_Checking_The_Successful_Addition_In_Favourite_List/" + "Saved to favourite toast message for coordinates location_2" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Clear_Data_button").click();

        //Add favourite by search destination for location_3
        TestBase.getObject("Search_destination_field").click();
        TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Location_3"));
        TestBase.selectFavourite();
        Thread.sleep(1000);
        String POI_3=TestBase.getObject("verify_POI").getText();
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C004_Adding_Location_In_Favourite_List_and_Checking_The_Successful_Addition_In_Favourite_List/" + "Saved to favourite toast message for coordinates location_3" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Clear_Data_button").click();

        //Add favourite by search destination for location_3
        TestBase.getObject("Search_destination_field").click();
        TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Location_4"));
        TestBase.selectFavourite();
        Thread.sleep(1000);
        String POI_4=TestBase.getObject("verify_POI").getText();
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C004_Adding_Location_In_Favourite_List_and_Checking_The_Successful_Addition_In_Favourite_List/" + "Saved to favourite toast message for coordinates location_4" + ".jpg");

        for(int i=0;i<=2;i++) {
            TestBase.getObject("Back_button").click();
        }


        //Verify locations are successfully added in Favourite screen from search destination
        TestBase.getObject("Favourites").click();

        if(TestBase.getObject("Location_1_text").getText().equals(POI_1)&&TestBase.getObject("Location_2_text").getText().equals(POI_2)&&TestBase.getObject("Location_3_text").getText().equals(POI_3)&&TestBase.getObject("Location_4_text").getText().equals(POI_4)){

            System.out.println("C004_Adding_Location_In_Favourite_List_and_Checking_The_Successful_Addition_In_Favourite_List: Locations are successfully added in favourites screen from search destination ");}
        else{
            softAssert.assertTrue(1>2, "Wrong Favourites are added in Favourites section");}
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        softAssert.assertAll();

    }



    @Test(priority = 5)

    public void C005_Check_The_Sorting_Functionality() throws Exception {

        SoftAssert softAssert= new SoftAssert();
        TestBase.getObject("Favourites").click();
        //Checking the sorting functionality
        TestBase.getObject("Sorting_button").click();
        softAssert.assertTrue(TestBase.isElementPresent("Sort_By_Time"), "C005:Time text is not present in the Search_Destination_screen");

        //Checking the sort by time functionality and take screenshot
        TestBase.getObject("Sort_By_Time").click();
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C005_Check_The_Sorting_Functionality/" + "Favourite Data Sort By Time" + ".jpg");
        TestBase.getObject("Sorting_button").click();

        //Checking the sort by Distance functionality and take screenshot
        softAssert.assertTrue(TestBase.isElementPresent("Sort_By_Distance"), "C005:Distance text is not present in the Search_Destination_screen");
        TestBase.getObject("Sort_By_Distance").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C005_Check_The_Sorting_Functionality/" + "Favourite Data Sort By Distance" + ".jpg");
        TestBase.getObject("Sorting_button").click();

        //Checking the sort by Frequency functionality and take screenshot
        softAssert.assertTrue(TestBase.isElementPresent("Sort_By_Frequency"), "C005:Frequency text is not present in the Search_Destination_screen");
        TestBase.getObject("Sort_By_Frequency").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C005_Check_The_Sorting_Functionality/" + "Favourite Data Sort By Frequency" + ".jpg");
        softAssert.assertAll();

    }


    @Test(priority = 6)

    public void C006_Check_The_Edit_functionality_In_Favourite ()throws Exception {


        SoftAssert softAssert= new SoftAssert();
        // TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C007_Check_The_Edit_functionality_In_Favourite/" + "Favourite data from search screen" + ".jpg");
        TouchAction action = new TouchAction(TestBase.driver);

        // Verify blank data toast message without editing the location
        action.longPress(TestBase.getObject("Edit_Location_1")).perform();

        softAssert.assertTrue(TestBase.isElementPresent("Edit_Location_1"), " C006:Same location name is not present in the Edit Favourite popup_screen");
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Delete_Favourite_Text"), "C006:Delete_Favourite text is not present in the Search_Destination_screen");
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Edit_Favourite_Text"), " C006:Edit_Favourite text is not present in the Edit Favourite popup_screen");
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Cancel_Button_Text"), " C006:Cancel_Favourite text is not present in the Edit Favourite popup_screen");
        TestBase.getObject("Verify_Cancel_Button_Text").click();
        Thread.sleep(2000);

        action.longPress(TestBase.getObject("Edit_Location_1")).perform();

        TestBase.getObject("Edit_Favourites_Button").click();
        TestBase.getObject("Edit_Field").click();
        TestBase.getObject("Edit_Field").clear();
        TestBase.getObject("Edit_Field").sendKeys(TestBase.TestData.getProperty("Edit_Favourite_name1"));
        String Edit_favourite_text=TestBase.getObject("Edit_Field").getText();
        TestBase.getObject("Ok_Button").click();
        if(TestBase.getObject("MyFavourite_1").getText().equals(Edit_favourite_text)) {
            System.out.println("C006_Check_The_Edit_functionality_In_Favourite: Favourite Successfully edited");
        }
        else{
            System.out.println("Failed");
        }
        TestBase.getObject("MyFavourite_1").click();
        softAssert.assertTrue(TestBase.isElementPresent("MyFavourite_1"), " C006:MyFavourite_1 text is not present under Map View Header");
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C006_Check_The_Edit_functionality_In_Favourite/" + "Edited Favourite map view screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        softAssert.assertAll();
    }



    @Test(priority = 7)

    public void C007_Check_The_Delete_functionality_In_Favourite ()throws Exception {

        SoftAssert softAssert= new SoftAssert();

        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C007_Check_The_Delete_functionality_In_Favourite/" + "Locations in Favourite screen before delete" + ".jpg");
        TouchAction action = new TouchAction(TestBase.driver);
        action.longPress(TestBase.getObject("Delete_Location_1")).perform();

        softAssert.assertTrue(TestBase.isElementPresent("Delete_Location_1"), " C006:Delete_Location_1 is not present in the popup_screen");
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Delete_Favourite_Text"), "C006:Delete_Favourite text is not present in the Search_Destination_screen");
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Edit_Favourite_Text"), " C006:Edit_Favourite text is not present in the Edit Favourite popup_screen");
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Cancel_Button_Text"), " C006:Cancel_Favourite text is not present in the Edit Favourite popup_screen");

        TestBase.getObject("Delete_Favourite_Button").click();
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Delete_Favourite_Text"), "C002:Delete_Favourite text is not present in the Search_Destination_screen");
        softAssert.assertTrue(TestBase.isElementPresent("Are_You_Delete_This_Text"), "C002:Are_You_Delete_This text is not present in the Search_Destination_screen");
        TestBase.getObject("Verify_Cancel_Button_Text").click();

        action.longPress(TestBase.getObject("Delete_Location_1")).perform();
        TestBase.getObject("Delete_Favourite_Button").click();
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Delete_Favourite_Text"), "C002:Delete_Favourite text is not present in the Search_Destination_screen");
        softAssert.assertTrue(TestBase.isElementPresent("Are_You_Delete_This_Text"), "C002:Are_You_Delete_This text is not present in the Search_Destination_screen");
        TestBase.getObject("Ok_Button").click();
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C007_Check_The_Delete_functionality_In_Favourite/" + "Locations in Favourite screen after deleted one location" + ".jpg");

        TestBase.getObject("Back_button").click();
        softAssert.assertAll();
    }


    @Test(priority = 8)

    public void C008_Checking_The_Back_Function ()throws Exception {
        TestBase.getObject("Search_option").click();
        TestBase.getObject("Favourites").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        //TestBase.getObject("Back_button").click();
        TestBase.clear_data();
        TestBase.getObject("Back_button").click();
    }



    @Test(priority = 9)
    public void C009_Accessing_Favourite_from_Search_page_when_DataNotAvailable ()throws Exception {


        SoftAssert softAssert= new SoftAssert();

        TestBase.getObject("Search_option").click();
        TestBase.getObject("Favourites").click();
        softAssert.assertTrue(TestBase.isElementPresent("no_favourites_msg1"), "C002:no_favourites_msg1 text is not present in the favourite screen from dashboard screen");
        TestBase.getObject("Back_button").click();
        softAssert.assertAll();
    }




    @Test(priority = 10)

    public void C010_Check_The_Delete_functionality_From_Favourite_when_OneFavouriteAvailable ()throws Exception {

        SoftAssert softAssert= new SoftAssert();

        //Add favourite by search destination for location_1
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Location_2"));
        TestBase.selectFavourite();
        for(int i=0;i<=2;i++) {
            TestBase.getObject("Back_button").click();
        }

        //click favourite button from search destination
        TestBase.getObject("Favourites").click();

        //Click on location in map view screen and unchecked the favourite star button
        TestBase.getObject("DeleteLocation").click();
        TestBase.getObject("Favourite_Star_Button").click();

        Thread.sleep(2000);
        //Screenshot for toast message after unchecked the star button
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C010_Check_The_Delete_functionality_From_Favourite_when_OneFavouriteAvailable/" + "Verify toast message when unchecked the star button for location_3" + ".jpg");
        TestBase.getObject("Back_button").click();

        //Verify no yet favourite after back to favourite screen
        softAssert.assertTrue(TestBase.isElementPresent("no_favourites_msg1"), "C009:no_favourites_msg1 is not present in  Favourite screen when back from map view screen");
        TestBase.getObject("Back_button").click();

        TestBase.getObject("Back_button").click();
        softAssert.assertAll();
    }




    @Test(priority = 11)

    public void C011_Check_The_Edit_functionality_In_Favourite_with_SameName() throws Exception {
        SoftAssert softAssert= new SoftAssert();

        TestBase.getObject("Search_option").click();

        //Add favourite by search destination for location_5
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Location_5"));
        TestBase.selectFavourite();

        //Take screenshot for location5 map view screen
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C011_Check_The_Edit_functionality_In_Favourite_with_SameName/" + "Saved to favourites toast message for location_5 " + ".jpg");

        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Clear_Data_button").click();


        //Add favourite by search destination for location_2
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Location_6"));
        TestBase.selectFavourite();

        //Take screenshot for location5 map view screen
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C011_Check_The_Edit_functionality_In_Favourite_with_SameName/" + "Saved to favourites toast message for location_6" + ".jpg");

        for (int i = 0; i <= 2; i++) {
            TestBase.getObject("Back_button").click();
        }

        TestBase.getObject("Favourites").click();

        //verify location_5 and location_6 are added as favourite in favourite screen
        softAssert.assertTrue(TestBase.isElementPresent("Location_5_text"), "C011:Location_5 is not present in  Favourite screen" );
        softAssert.assertTrue(TestBase.isElementPresent("Location_6_text"), "C011:Location_6 is not present in  Favourite screen" );


        TouchAction action= new TouchAction(TestBase.driver);
        action.longPress(TestBase.getObject("Location_5_text")).perform();

        //Edit the location_5 name as My favourite_1
        TestBase.getObject("Edit_Favourites_Button").click();
        TestBase.getObject("Edit_Field").clear();
        TestBase.getObject("Edit_Field").sendKeys(TestBase.TestData.getProperty("Edit_Favourite_name1"));
        TestBase.getObject("Ok_Button").click();

        action.longPress(TestBase.getObject("Location_6_text")).perform();

        //Edit the location_6 name as My favourite_1
        TestBase.getObject("Edit_Favourites_Button").click();
        TestBase.getObject("Edit_Field").clear();
        TestBase.getObject("Edit_Field").sendKeys(TestBase.TestData.getProperty("Edit_Favourite_name1"));
        TestBase.getObject("Ok_Button").click();

        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C011_Check_The_Edit_functionality_In_Favourite_with_SameName/" + "Favourites already exist.Choose a different name " + ".jpg");

        TestBase.getObject("Verify_Cancel_Button_Text").click();
        //Verify the Myfavourite_1 locations are present after editing location_5 and location_6
        softAssert.assertTrue(TestBase.isElementPresent("MyFavourite_1"), "C011:MyFavourite_1 is not present in  Favourite screen after editing ");
        softAssert.assertTrue(TestBase.isElementPresent("Location_6_text"), "C011:Location_6_text is not present in  Favourite screen after click n cancel while editing ");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

        //Verify the Myfavourite_1 locations are present after editing location_5 and location_6 in favourite screen from dashboard screen
        TestBase.getObject("Favourites").click();
        softAssert.assertTrue(TestBase.isElementPresent("MyFavourite_1"), "C011:MyFavourite_1 is not present in  Favourite screen after editing ");
        softAssert.assertTrue(TestBase.isElementPresent("Location_6_text"), "C011:Location_6_text is not present in  Favourite screen after click n cancel while editing ");
        TestBase.getObject("Back_button").click();
        //TestBase.clear_data();
        //TestBase.getObject("Back_button").click();
    }

    @Test(priority = 12)
    public void C012_Verify_Editing_OneName_from_SameName_Doesnot_Change_OtherName() throws Exception {

        SoftAssert softAssert= new SoftAssert();
        TestBase.getObject("Favourites").click();
        // Assert.assertTrue(TestBase.isElementPresent("MyFavourite_1"), "C009:MyFavourite_2 is not present in  Favourite screen");
        // Assert.assertTrue(TestBase.isElementPresent("MyFavourite_1"), "C009:MyFavourite_2 is not present in  Favourite screen");
        //Take screenshot for location5 map view screen
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C012_Verify_Editing_OneName_from_SameName_Doesnot_Change_OtherName/" + "Two Favourite_Locations with same name in favourite screen" + ".jpg");
        TouchAction action = new TouchAction(TestBase.driver);
        action.longPress(TestBase.getObject("MyFavourite_1")).perform();

        //Edit first location as My_Favourite_1
        TestBase.getObject("Edit_Favourites_Button").click();
        TestBase.getObject("Edit_Field").click();
        TestBase.getObject("Edit_Field").clear();
        TestBase.getObject("Edit_Field").sendKeys(TestBase.TestData.getProperty("Edit_Favourite_name2"));
        TestBase.getObject("Ok_Button").click();

        //Verify first location is successfully edited as My_Favourite_1
        softAssert.assertTrue(TestBase.isElementPresent("Location_6_text"), "C009:Location_6_text is not present in Favourite screen after editing My_Favourite_1 text as My_Favourite_2 ");
        softAssert.assertTrue(TestBase.isElementPresent("MyFavourite_2"), "C009:MyFavourite_2 is not present in  Favourite screen ");
        TestBase.getObject("Back_button").click();

        ////Verify newly edited location and exisiting location is present in favourite screen
        TestBase.getObject("Favourites").click();
        softAssert.assertTrue(TestBase.isElementPresent("Location_6_text"), "C009:Location_6_text is not present in Favourite screen after editing My_Favourite_1 text as My_Favourite_2 ");
        softAssert.assertTrue(TestBase.isElementPresent("MyFavourite_2"), "C011:MyFavourite_2 is not present in  Favourite screen after editing My_Favourite_1 text as MyFavourite_2");
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C012_Verify_Editing_OneName_from_SameName_Doesnot_Change_OtherName/" + "After editing first location in favourite screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        // TestBase.clear_data();
        //TestBase.getObject("Back_button").click();
        softAssert.assertAll();
    }


    @Test(priority = 13)
    public void C013_Verify_Editing_SameName_Doesnot_allow_for_SameAddress() throws Exception {

        SoftAssert softAssert= new SoftAssert();
        TouchAction action = new TouchAction(TestBase.driver);
        TestBase.getObject("Favourites").click();
        action.longPress(TestBase.getObject("Location_6_text")).perform();
        TestBase.getObject("Edit_Favourites_Button").click();
        TestBase.getObject("Edit_Field").click();
        TestBase.getObject("Ok_Button").click();
        Thread.sleep(1000);
        // TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C013_Verify_Editing_SameName_Doesnot_allow_for_SameAddress/" + "Favourite already popup message after editing same POI name" + ".jpg");
        // TestBase.getObject("Verify_Cancel_Button_Text").click();
        softAssert.assertTrue(TestBase.isElementPresent("Location_6_text"), "C009:Location_6_text is not present in Favourite screen");
        softAssert.assertTrue(TestBase.isElementPresent("MyFavourite_2"), "C011:MyFavourite_2 is not present in  Favourite screen");
        TestBase.getObject("Back_button").click();
        TestBase.clear_data();
        TestBase.getObject("Back_button").click();
        softAssert.assertAll();
    }

    @Test(priority = 14)

    public void C014_Check_The_Edit_Favourite_functionality_During_Adding_Favourite() throws Exception {

        SoftAssert softAssert= new SoftAssert();
        TestBase.getObject("Search_option").click();

        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Location_7"));
        TestBase.getObject("Search_button").click();
        TestBase.getObject("Location_Id").click();
        TestBase.getObject("Favourite_Star_Button").click();
        TestBase.getObject("Add_To_Favourite_Edit_Field").clear();
        TestBase.getObject("Add_To_Favourite_Edit_Field").sendKeys(TestBase.TestData.getProperty("Edit_Favourite_name1"));
        TestBase.getObject("Ok_Button").click();

        //Take screenshot for location5 map view screen
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C011_Check_The_Edit_functionality_In_Favourite_with_SameName/" + "Saved to favourites toast message for location_7 " + ".jpg");
        for(int i=0;i<=2;i++){
            TestBase.getObject("Back_button").click();
        }

        TestBase.getObject("Favourites").click();
        //verify Myfavourite_1 name location as a favourite is present in favourite screen
        softAssert.assertTrue(TestBase.isElementPresent("MyFavourite_1"),"C014:MyFavourite_1 is not present in  Favourite screen after editing My_Favourite_2 text as MyFavourite_1");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();


        TestBase.getObject("Favourites").click();
        //verify Myfavourite_1 name location as a favourite is present in favourite screen
        softAssert.assertTrue(TestBase.isElementPresent("MyFavourite_1"),"C014:MyFavourite_1 is not present in  Favourite screen after editing My_Favourite_2 text as MyFavourite_1");
        TestBase.getObject("Back_button").click();
        softAssert.assertAll();

    }


    @Test(priority = 15)

    public void C015_Check_The_Remove_Favourite_functionality_After_Adding_Favourite() throws Exception {

        SoftAssert softAssert= new SoftAssert();
        TestBase.getObject("Favourites").click();
        TestBase.getObject("MyFavourite_1").click();
        TestBase.getObject("Favourite_Star_Button").click();
        Thread.sleep(2000);
        //Take screenshot when removed any location as a favourite in map view screen
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C015_Check_The_Remove_Favourite_functionality_After_Adding_Favourite/" + "Removed to favourites toast message in map view screen  " + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        softAssert.assertAll();
    }

    @Test(priority = 16)

    public void C016_Check_The_Edit_Favourite_functionality_with_No_Name() throws Exception {


        SoftAssert softAssert= new SoftAssert();

        TestBase.getObject("Search_option").click();

        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Location_8"));
        TestBase.selectFavourite();
        for (int i = 0; i <= 2; i++) {
            TestBase.getObject("Back_button").click();
        }
        TestBase.getObject("Favourites").click();
        softAssert.assertTrue(TestBase.isElementPresent("Location_8_text"), "C014:Location_8 is not present in  Favourite screen");

        TouchAction action = new TouchAction(TestBase.driver);
        action.longPress(TestBase.getObject("Location_8_text")).perform();

        //Edit first location as My_Favourite_1
        TestBase.getObject("Edit_Favourites_Button").click();
        TestBase.getObject("Edit_Field").click();
        TestBase.getObject("Edit_Field").clear();
        TestBase.getObject("Edit_Field").sendKeys(TestBase.TestData.getProperty(" "));
        TestBase.driver.navigate().back();
        TestBase.getObject("Ok_Button").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C016_Check_The_Edit_Favourite_functionality_with_No_Name/" + "Verify validation message after giving blank data in favourite screen1" + ".jpg");


        TestBase.getObject("Verify_Cancel_Button_Text").click();
        softAssert.assertTrue(TestBase.isElementPresent("Location_8_text"), "C014:Location_8 is not present in  Favourite screen");

        TestBase.getObject("Location_8_text").click();
        TestBase.getObject("Favourite_Star_Button").click();
        TestBase.getObject("Favourite_Star_Button").click();

        TestBase.getObject("Add_To_Favourite_Edit_Field").clear();
        TestBase.getObject("Add_To_Favourite_Edit_Field").sendKeys(TestBase.TestData.getProperty(" "));
        TestBase.driver.navigate().back();
        TestBase.getObject("Ok_Button").click();
        Thread.sleep(1000);
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C016_Check_The_Edit_Favourite_functionality_with_No_Name/" + "Verify validation message Please key in a favourite name" + ".jpg");

        TestBase.getObject("Verify_Cancel_Button_Text").click();
        TestBase.getObject("Favourite_Star_Button").click();
        TestBase.getObject("Ok_Button").click();
        for (int i = 0; i <= 2; i++) {
            TestBase.getObject("Back_button").click();
        }
       /* TestBase.clear_data();
        TestBase.getObject("Back_button").click();*/
        softAssert.assertAll();
    }





    @Test(priority = 17)

    public void C017_Check_OriginalLocationName_after_Removing_Favourite_and_again_Adding_as_favourite_in_Map_View_Screen() throws Exception {

        SoftAssert softAssert= new SoftAssert();

        TestBase.getObject("Favourites").click();
        TestBase.getObject("Location_8_text").click();
        String text = TestBase.getObject("verify_POI").getText();
        TestBase.getObject("Favourite_Star_Button").click();
        TestBase.getObject("Favourite_Star_Button").click();
        TestBase.getObject("Add_To_Favourite_Edit_Field").clear();
        TestBase.getObject("Add_To_Favourite_Edit_Field").sendKeys(TestBase.TestData.getProperty("Edit_Favourite_name1"));
        Thread.sleep(2000);
        String text1 = TestBase.getObject("Add_To_Favourite_Edit_Field").getText();
        TestBase.getObject("Ok_Button").click();
        TestBase.getObject("Back_button").click();
        softAssert.assertTrue(TestBase.isElementPresent("MyFavourite_1"), "C011:MyFavourite_1 is not present in  Favourite screen after first time editing the  name in map view screen");
        TestBase.getObject("MyFavourite_1").click();

        if (TestBase.getObject("verify_POI").getText().equals(text1)) {
            System.out.println("Edited location name is displaying in map view screen under header");
            TestBase.getObject("Favourite_Star_Button").click();
            TestBase.getObject("Favourite_Star_Button").click();
            softAssert.assertTrue(TestBase.isElementPresent("Location_8_text"), "C011:Location_8 is not present in add to favourite popup");
            TestBase.getObject("Ok_Button").click();
            TestBase.getObject("Back_button").click();
            softAssert.assertTrue(TestBase.isElementPresent("Location_8_text"), "C011:Original POI name is not present in  Favourite screen after second time editing the name in map view screen");
            TestBase.getObject("Location_8_text").click();
            if((TestBase.getObject("verify_POI").getText().equals(text))){
                System.out.println("Original location name is displaying in map view screen under header");
            }
            else {
                System.out.println("Original location name is not displaying in map view screen under header");
            }
        }

        else{
            System.out.println("Edited location name is not displaying in map view screen under header");
        }
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        Thread.sleep(1000);
        TestBase.clear_data();
        TestBase.getObject("Back_button").click();
        softAssert.assertAll();
    }



    @Test(priority = 18)
    public void C018_Verify_that_user_is_able_to_add_a_favourite_from_Navigation_screen() throws Exception {


        //Open any location in Map view and start the simulation
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(3000);
        TestBase.getObject("Location_Name").click();
        Thread.sleep(5000);
        TestBase.getObject("Simulate_Button").click();
        Thread.sleep(9000);

        try {
            if (TestBase.getObject("ParkingPredictoion").getText().contentEquals("Close")) {
                TestBase.getObject("ParkingPredictoion").click();
            }
        } catch (Exception e) {
            System.out.println("No Parking Prediction Popup Displaying");
        }

        Thread.sleep(3000);



        //Click on the Map view
        WebElement element = TestBase.driver.findElement(By.id("com.galactio.mobile.sg:id/surface_view_only"));
        //should be switched to name instead of text JavascriptExecutor
        JavascriptExecutor js = (JavascriptExecutor) TestBase.driver;
        Point p = element.getLocation();
        Dimension size = element.getSize();
        //double x = p.getX() + size.getWidth() / 2.0, y = p.getY() + size.getHeight() / 2.0;
        HashMap<String , Double> point = new HashMap<String , Double>();
        point.put("x" ,295.0); point.put("y" , 465.0); js.executeScript("mobile: tap", point);
        Thread.sleep(1000);


        TestBase.driver.navigate().back();
        Thread.sleep(2000);
        TestBase.clear_data();
        TestBase.getObject("Back_button").click();


    }

    @Test(priority = 19)
    public void C019_Verify_that_Readdition_of_Favourite_after_editing_doesnot_change_the_original_POIname_in_Popup() throws Exception {

        //Open the location in Map view
        TestBase.getObject("Search_option").click();
        TestBase.getObject("POI").click();

        TestBase.getObject("Emergency_Text").click();
        TestBase.getObject("Policestation").click();
        TestBase.getObject("PolicestationSubcategories1").click();
        Thread.sleep(3000);
        TestBase.getObject("Favourite_Star_Button").click();
        Thread.sleep(1000);

        //String Edited= TestBase.getObject("Edit_Field").getText();


        TestBase.getObject("Edit_Field").clear();
        TestBase.getObject("Edit_Field").sendKeys(TestBase.TestData.getProperty("Edit_Favourite_name2"));
        TestBase.getObject("Ok_Button").click();

        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C19046_TC002_Verify_that_Readdition_of_Favourite_after_editing_doesnot_change_the_original_POIname_in_Popup/" + "ADD_FAVOURITE_TOAST" + ".jpg");
        TestBase.getObject("Back_button").click();

        //Again click on the same location in Map view
        TestBase.getObject("PolicestationSubcategories1").click();
        Thread.sleep(3000);

        //click on the favourite icon again
        TestBase.getObject("Favourite_Star_Button").click();
        TestBase.get_Screenshot("src\\test\\resource\\FAVOURITES\\C19046_TC002_Verify_that_Readdition_of_Favourite_after_editing_doesnot_change_the_original_POIname_in_Popup/" + "Delete_FAVOURITE_TOAST" + ".jpg");

        //Again click on the same and verify the previous location name still intact
        TestBase.getObject("Favourite_Star_Button").click();
        Thread.sleep(1000);
        Assert.assertTrue(TestBase.isElementPresent("Location_Name"),"C19046: Location Name is not present in");
        Thread.sleep(1000);
        TestBase.getObject("Cancel_Button").click();

        //Click on Back
        for (int i = 0; i <= 4; i++) {
            TestBase.getObject("Back_button").click();
        }

        //Clear all the data
        TestBase.clear_data();
        TestBase.getObject("Back_button").click();

    }

    @Test(priority = 20)
    public void C020_Verify_that_Readdition_of_edited_Favourite_shows_the_original_POI_name_in_Pop_up() throws Exception {


        //Add any location as Favourite
        TestBase.getObject("Search_option").click();
        TestBase.getObject("POI").click();

        TestBase.getObject("Emergency_Text").click();
        TestBase.getObject("Policestation").click();
        TestBase.getObject("PolicestationSubcategories1").click();
        Thread.sleep(3000);
        TestBase.getObject("Favourite_Star_Button").click();
        Thread.sleep(1000);
        TestBase.getObject("Ok_Button").click();

        //Back to Favourite dashboard page
        for (int i = 0; i <2; i++) {
            TestBase.getObject("Back_button").click();
        }

        //Click on Favourite
        TestBase.getObject("Favourite_Tab").click();

        //Open the selected Favorite location
        TestBase.getObject("Location_Name").click();
        Thread.sleep(3000);
        TestBase.getObject("Favourite_Star_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Favourite_Star_Button").click();
        Assert.assertTrue(TestBase.isElementPresent("Location_Name"),"C19048: Location Name is not present in");
        Thread.sleep(1000);
        TestBase.getObject("Cancel_Button").click();

        //Back to Favourite dashboard page
        for (int i = 0; i <= 4; i++) {
            TestBase.getObject("Back_button").click();
        }

        //Clear all the data
        TestBase.clear_data();
        TestBase.getObject("Back_button").click();

    }

    @Test(priority = 21)
    public void C021_Verify_that_the_user_is_able_to_navigate_to_Favourite_section_from_Dashboard_Page() throws Exception {
        SoftAssert softAssert= new SoftAssert();

        //Verify the Favourite from Dash-Board

        softAssert.assertTrue(TestBase.isElementPresent("Favourites"), "C19260: Favourites_text is not present in the Set Home popup");
        TestBase.getObject("Favourites").click();
        softAssert.assertTrue(TestBase.isElementPresent("no_favourites_msg1"), "C19260: No Favourites yet... is not present in the Set Home popup");

        TestBase.getObject("Back_button").click();

        softAssert.assertAll();

    }


    @Test(priority = 22)
    public void C022_Verify_that_no_cards_are_displayed_on_the_dashboard_when_user_logged_in_for_the_first_time() throws Exception {

        SoftAssert softAssert = new SoftAssert();
        //Verify the SUGGESTED FOR YOU Section
        softAssert.assertTrue(TestBase.isElementPresent("Suggested_For_You"), "C19263: Suggested for you section is not present");
        softAssert.assertFalse(TestBase.isElementPresent("LayOut"), "Favourite still present");
        softAssert.assertAll();

    }

    @Test(priority = 23)
    public void C023_Verify_that_only_1_Favourite_cards_is_displayed_on_the_dashboard_when_1_Favourite_is_added() throws Exception {


        SoftAssert softAssert = new SoftAssert();

        //Click on Favourite
        TestBase.getObject("Search_option").click();
        TestBase.getObject("POI").click();

        TestBase.getObject("Emergency_Text").click();
        TestBase.getObject("Policestation").click();
        TestBase.getObject("PolicestationSubcategories1").click();
        Thread.sleep(3000);
        TestBase.getObject("Favourite_Star_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Ok_Button").click();
        Thread.sleep(2000);

        //Back to Favourite dashboard page
        for (int i = 0; i <= 3; i++) {
            TestBase.getObject("Back_button").click();
        }

        //Click on Favourite
        TestBase.getObject("Favourite_Tab").click();

        //Verify the location added successfully
        softAssert.assertTrue(TestBase.isElementPresent("Fav_Location"), "Favourite still present");

        softAssert.assertAll();
    }





      @AfterTest
    public void quit(){

        if((TestBase.driver)!=null){
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        TestBase.driver.quit();
        System.out.println("exit");
    }
}






