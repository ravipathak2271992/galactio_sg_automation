import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by Soumik on 6/22/2016.
 */
public class SEARCH_FILTER {
    @Test(priority = 1)
    public void C001_Verify_the_Search_Filter_Button() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();
        Thread.sleep(2000);

        //"Continue Journey" popup handled

        try {
            if (TestBase.getObject("Popup").getText().contentEquals("No")) {
                TestBase.getObject("Popup").click();
            }
        }
        catch(Exception e){
            System.out.println("No Journey Popup Displaying");
        }

        //Verify the Search Filter button present in the screen
        TestBase.getObject("Search_Button").click();
        Thread.sleep(1000);
        //Search
        TestBase.getObject("Search_destination_field").click();
        TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Location"));
        Thread.sleep(2000);
        TestBase.getObject("Search_button").click();

        //Verify the Search Filter Button

        Assert.assertTrue(TestBase.isElementPresent("Search_Filter_Button"), "Search_Filter_Button is not present in screen");
        Thread.sleep(1000);

    }

    @Test(priority = 2)
    public void C002_Tap_on_Search_Filter_button_and_verify_all_the_options() throws Exception {

        //Click on Search Filter button
        TestBase.getObject("Search_Filter_Button").click();
        Thread.sleep(2000);

        //Verify all the Options
        //All Options verification for search filter options
        Assert.assertTrue(TestBase.isElementPresent("BY_CATEGORY_Category"), "BY_CATEGORY_Category is not present in screen");
        Assert.assertTrue(TestBase.isElementPresent("POI_Option"), "POI_Option is not present in screen");
        Assert.assertTrue(TestBase.isElementPresent("Address_Option"), "Address_Option is not present in screen");
        Assert.assertTrue(TestBase.isElementPresent("Roads_Option"), "Roads_Option is not present in screen");
        Assert.assertTrue(TestBase.isElementPresent("City_Options"), "City_Options is not present in screen");
        Assert.assertTrue(TestBase.isElementPresent("State_Option"), "State_Option is not present in screen");
        Assert.assertTrue(TestBase.isElementPresent("Country_Option"), "Country_Option is not present in screen");
        Assert.assertTrue(TestBase.isElementPresent("BY_LOCATION_Category"), "BY_LOCATION_Category is not present in screen");

    }

        @Test(priority = 3)
        public void C003_Verify_the_Search_Filter_Options_and_UI_Checking() throws Exception {
        //Take a screen shots for UI verification
        TestBase.get_Screenshot("src\\test\\resource\\SEARCH_FILTER\\C003_Verify_the_Search_Filter_Options_and_UI_Checking/" + "Refine_Search_UI" + ".jpg");

            //  Creating Loop to back from the screen
            for (int i = 0; i < 4; i++) {
                TestBase.getObject("Back_Button").click();
            }

    }

    @Test(priority = 4)
    public void C004_Check_the_display_of_Filter_window_when_accessed_from_GlobalSearch() throws Exception {

        //Click on Search Button
        TestBase.getObject("Search_Button").click();
        //Global Search
        TestBase.getObject("Search_destination_field").click();
        TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("GLOBAL_SEARCH"));
        Thread.sleep(3000);
        TestBase.getObject("Search_button").click();

        Assert.assertTrue(TestBase.isElementPresent("Search_Filter_Button"), "Search_Filter_Button is not present in screen");
        TestBase.getObject("Search_Filter_Button").click();
        Thread.sleep(2000);

        //Take a screen shots for search filter
        TestBase.get_Screenshot("src\\test\\resource\\SEARCH_FILTER\\C004_Check_the_display_of_Filter_window_when_accessed_from_GlobalSearch/" + "SearchOptions_GlobalSearch" + ".jpg");

        //Click on Cross button
        TestBase.getObject("SearchFilter_Cross_Button").click();

        //  Creating Loop to back from the screen
        for (int i = 0; i < 2; i++) {
            TestBase.getObject("Back_Button").click();
        }

    }

    @Test(priority = 5)
    public void C005_Check_the_display_of_Filter_window_when_accessed_from_Address_Search() throws Exception {

        //Address Search
        TestBase.getObject("Search_destination_field").click();
        TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Address_Search"));
        Thread.sleep(3000);
        TestBase.getObject("Search_button").click();

        //Click Search Filter Button
        TestBase.getObject("Search_Filter_Button").click();
        Thread.sleep(2000);

        //Take a screen shots for address search filter criteria
        TestBase.get_Screenshot("src\\test\\resource\\SEARCH_FILTER\\C004_Check_the_display_of_Filter_window_when_accessed_from_Address_Search/" + "Address_Search_FiltrCriterea" + ".jpg");

        //Click Back(<) button and return to the search page
        TestBase.getObject("SearchFilter_Cross_Button").click();

        for (int i = 0; i < 2; i++) {
            TestBase.getObject("Back_Button").click();
        }
    }

    @Test(priority = 6)
    public void C006_Check_the_display_of_Filter_window_when_accessed_from_PostalCode() throws Exception {

        //Postal_Search
        TestBase.getObject("Search_destination_field").click();
        TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Postal_Search"));
        Thread.sleep(3000);
        TestBase.getObject("Search_button").click();

        //Click Search Filter Button
        TestBase.getObject("Search_Filter_Button").click();
        Thread.sleep(2000);

        //Take a screen shots for address search filter criteria
        TestBase.get_Screenshot("src\\test\\resource\\SEARCH_FILTER\\C006_Check_the_display_of_Filter_window_when_accessed_from_PostalCode/" + "Postal_Search_FiltrCriterea" + ".jpg");

        //Click on Cross button
        TestBase.getObject("SearchFilter_Cross_Button").click();

        for (int i = 0; i < 2; i++) {
            TestBase.getObject("Back_Button").click();
        }

    }

    @Test(priority = 7)
    public void C007_Check_the_display_of_Filter_window_when_accessed_for_POI_Search() throws Exception {
        //POI_Search
        TestBase.getObject("Search_destination_field").click();
        TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("POI_Search"));
        Thread.sleep(3000);
        TestBase.getObject("Search_button").click();

        //Click Search Filter Button
        TestBase.getObject("Search_Filter_Button").click();
        Thread.sleep(2000);

        //Take a screen shots for address search filter criteria
        TestBase.get_Screenshot("src\\test\\resource\\SEARCH_FILTER\\C007_Check_the_display_of_Filter_window_when_accessed_for_POI_Search/" + "POI_Search_FiltrCriterea" + ".jpg");

        //Click on Cross button
        TestBase.getObject("SearchFilter_Cross_Button").click();

        for (int i = 0; i < 2; i++) {
            TestBase.getObject("Back_Button").click();
        }

    }

    @Test(priority = 8)
    public void C008_Verify_the_Text_For_By_Category_Options() throws Exception {
        //Global Search
        TestBase.getObject("Search_destination_field").click();
        TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("GLOBAL_SEARCH"));

        TestBase.getObject("Search_button").click();
        Thread.sleep(3000);

        //Click Search Filter Button
        TestBase.getObject("Search_Filter_Button").click();

        //Verify the BY CATEGORY text
        Assert.assertTrue(TestBase.isElementPresent("BY_CATEGORY_Category"), "BY_CATEGORY_Category option is not present in search filter options");

    }

    @Test(priority = 9)
    public void C009_Applying_Filter_Category_for_POI_Options_and_Verify_the_Results() throws Exception {

        //Select POI Options from Search Filter
        TestBase.getObject("POI_More").click();
        Thread.sleep(2000);
        //Take a screen shots for all the POI sub categories in Filter options
        TestBase.get_Screenshot("src\\test\\resource\\SEARCH_FILTER\\C009_Applying_Filter_Category_for_POI_Options_and_Verify_the_Results/" + "POI_SubCategory1" + ".jpg");

        //Select 'Emergency' sub category options to filter the result
        TestBase.getObject("Emergeny_Subcategory").click();
        TestBase.getObject("All_Options").click();

        //Take a screen shots after filter by Emergency
        TestBase.get_Screenshot("src\\test\\resource\\SEARCH_FILTER\\C009_Applying_Filter_Category_for_POI_Options_and_Verify_the_Results/" + "Emergency_All_Category_Select" + ".jpg");
        Thread.sleep(2000);
        //Result Screen Verification
        TestBase.get_Screenshot("src\\test\\resource\\SEARCH_FILTER\\C009_Applying_Filter_Category_for_POI_Options_and_Verify_the_Results/" + "Emergency_Result_After_Filter" + ".jpg");


        //Tap on Search Filter button to verify the screen
        TestBase.getObject("Search_Filter_Button").click();

        //Verify the selected category in search filter
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\SEARCH_FILTER\\C009_Applying_Filter_Category_for_POI_Options_and_Verify_the_Results/" + "selected_category_Verify_SearchFilter" + ".jpg");
        Thread.sleep(2000);

        for (int i = 0; i < 3; i++) {
            TestBase.getObject("Back_Button").click();
        }



    }

    @Test(priority = 10)
    public void C010_Applying_Filter_Category_for_Address_Options_and_Verify_the_Results() throws Exception {

        //Global Search
        TestBase.getObject("Search_destination_field").click();
        TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("GLOBAL_SEARCH"));
        Thread.sleep(3000);
        TestBase.getObject("Search_button").click();

        Assert.assertTrue(TestBase.isElementPresent("Search_Filter_Button"), "Search_Filter_Button is not present in screen");
        TestBase.getObject("Search_Filter_Button").click();
        Thread.sleep(2000);

        //Select Address options from Search Filter
        TestBase.getObject("Address_Option").click();

        //Take screen shot and verify the address filter
        TestBase.get_Screenshot("src\\test\\resource\\SEARCH_FILTER\\C010_Applying_Filter_Category_for_Address_Options_and_Verify_the_Results/" + "Address_Filter_Result" + ".jpg");
        TestBase.getObject("Search_Filter_Button").click();
        Thread.sleep(2000);

        TestBase.get_Screenshot("src\\test\\resource\\SEARCH_FILTER\\C010_Applying_Filter_Category_for_Address_Options_and_Verify_the_Results/" + "Filter_Option_after_Select_Address" + ".jpg");
    }

    @Test(priority = 11)
    public void C011_Applying_Filter_Category_for_Road_Options_and_Verify_the_Results() throws Exception {

        //Click for Back(<)
        TestBase.getObject("Cross_button").click();
        for (int i = 0; i < 1; i++) {
            TestBase.getObject("Back_Button").click();
        }

        //Open the result list
        Thread.sleep(2000);
        TestBase.getObject("Search_button").click();
        Thread.sleep(1000);
        TestBase.getObject("Search_Filter_Button").click();
        Thread.sleep(2000);

        //Click on Roads option from Search options
        TestBase.getObject("Roads_Option").click();
        Thread.sleep(2000);

        //Verify the Roads Result after applying the filter
        TestBase.get_Screenshot("src\\test\\resource\\SEARCH_FILTER\\C011_Applying_Filter_Category_for_Road_Options_and_Verify_the_Results/" + "Result_list_Roads" + ".jpg");
        Thread.sleep(1000);
        TestBase.getObject("Search_Filter_Button").click();
        Thread.sleep(1000);
        TestBase.get_Screenshot("src\\test\\resource\\SEARCH_FILTER\\C011_Applying_Filter_Category_for_Road_Options_and_Verify_the_Results/" + "Roads_option_Selected" + ".jpg");

    }

    @Test(priority = 12)
    public void C012_Verify_the_Text_For_By_Location_category_Options() throws Exception {

        //Click for Back(<)
        TestBase.getObject("Cross_button").click();
        for (int i = 0; i < 1; i++) {
            TestBase.getObject("Back_Button").click();
        }

        //Open the result list
        Thread.sleep(2000);
        TestBase.getObject("Search_button").click();
        Thread.sleep(1000);
        TestBase.getObject("Search_Filter_Button").click();
        Thread.sleep(2000);

        //Verify the BY LOCATION Text verification
        Assert.assertTrue(TestBase.isElementPresent("BY_LOCATION_Category"), "BY_LOCATION_Category is not present in screen");

    }

    @Test(priority = 13)
    public void C013_Applying_Filter_category_for_City_options_and_Verify_the_Results() throws Exception {

        //verify and select the City options
        Assert.assertTrue(TestBase.isElementPresent("City_Options"), "City_Options is not present in screen");
        TestBase.getObject("City_Options").click();

        //Verify the City category.
        TestBase.get_Screenshot("src\\test\\resource\\SEARCH_FILTER\\C013_Applying_Filter_category_for_City_options_and_Verify_the_Results/" + "City_Category_List" + ".jpg");

        //Select WEST city
        TestBase.getObject("West_City").click();
        Thread.sleep(2000);

        //Verify the Result list after select the City Filter
        TestBase.get_Screenshot("src\\test\\resource\\SEARCH_FILTER\\C013_Applying_Filter_category_for_City_options_and_Verify_the_Results/" + "Result_After_Select_City" + ".jpg");
        Thread.sleep(2000);
        TestBase.getObject("Search_Filter_Button").click();
        TestBase.get_Screenshot("src\\test\\resource\\SEARCH_FILTER\\C013_Applying_Filter_category_for_City_options_and_Verify_the_Results/" + "Selected_City_in_Filter" + ".jpg");

    }

    @Test(priority = 14)
    public void C014_Verify_the_Clear_Filter_Functionality_for_BY_LOCATION() throws Exception {
        SoftAssert softAssert=new SoftAssert();
        //Verify the Clear button
        softAssert.assertTrue(TestBase.isElementPresent("Clear_ByLocation"), "Clear_ByLocation is not present in screen");
        TestBase.getObject("Clear_ByLocation").click();

        //verify after clear the Location filter
        softAssert.assertFalse(TestBase.isElementPresent("Clear_ByLocation"), "Clear_ByLocation is present in screen");

        //Click for Back(<)
        for (int i = 0; i < 1; i++) {
            TestBase.getObject("Back_Button").click();
        }

        softAssert.assertAll();
    }

    @Test(priority = 15)
    public void C015_Verify_the_Clear_Filter_Functionality_for_BY_CATEGORY() throws Exception {

        SoftAssert softAssert=new SoftAssert();
        TestBase.getObject("Search_Filter_Button").click();
        Thread.sleep(2000);

        //Select POI for test the Clear filter feature
        TestBase.getObject("POI_More").click();
        TestBase.getObject("Parking_option").click();
        TestBase.getObject("Parking_MSCP").click();
        Thread.sleep(2000);
        TestBase.getObject("Search_Filter_Button").click();

        //Clear Filter option
        softAssert.assertTrue(TestBase.isElementPresent("Clear_ByCategory"), "Clear_ByCategory is not present in screen");
        TestBase.getObject("Clear_ByCategory").click();

        //Verify after clear the filter
        softAssert.assertFalse(TestBase.isElementPresent("Clear_ByCategory"), "Clear_ByCategory is present in screen");
        softAssert.assertAll();
    }

    @Test(priority = 16)
    public void C016_Tap_on_Cross_button_function_for_closing_the_filter_slide() throws Exception {
        //Verify the Cross Button present
        Assert.assertTrue(TestBase.isElementPresent("Cross_button"), "Cross_button is present in screen");
        TestBase.getObject("Cross_button").click();

        Assert.assertFalse(TestBase.isElementPresent("Cross_button"), "Cross_button is still present in screen");
    }

    @Test(priority = 17)
    public void C017_Verify_the_Back_button_functionality_using_in_searchFilter() throws Exception {

        //Click for Back(<)
        for (int i = 0; i < 3; i++) {
            TestBase.getObject("Back_Button").click();
        }

        //Clear all the data Functionality Flow:
        TestBase.clear_data();
        TestBase.getObject("Back_Button").click();

    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }

}




