import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Created by QI 33 on 10-05-2016.
 */
public class INCIDENTS_FUNCTIONS {

    @Test(priority = 1)

    public void C001_Accessing_Incidents_Screen() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();
        //Click on Option_Button
        TestBase.getObject("Option_Button").click();
        //Click on Incidents_Button
        TestBase.getObject("Verify_Incidents_Button").click();
        Thread.sleep(2000);
        // Screenshot for Incidents screen
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C001_Accessing_Incidents_Screen/" + "Incidents_screen1" + ".jpg");
        //Click on back button
        TestBase.getObject("Back_button").click();
    }

    @Test(priority = 2)

    public void C002_Accessing_Incidents_Screen_Dashboard() throws Exception {
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C002_Accessing_Incidents_Screen_Dashboard/" + "Incidents View_1 on Dashboard_Screen" + ".jpg");
        Thread.sleep(2000);
        TestBase.getObject("Dashboard_Incidents").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C002_Accessing_Incidents_Screen_Dashboard/" + "Incidents View Map view_1 on Dashboard" + ".jpg");
        TestBase.getObject("Back_button").click();
        Thread.sleep(7000);
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C002_Accessing_Incidents_Screen_Dashboard/" + "Incidents View_2 on Dashboard_Screen" + ".jpg");
        Thread.sleep(7000);
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C002_Accessing_Incidents_Screen_Dashboard/" + "Incidents View_3 on Dashboard_Screen" + ".jpg");
        Thread.sleep(7000);
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C002_Accessing_Incidents_Screen_Dashboard/" + "Incidents View_4 on Dashboard_Screen" + ".jpg");
        Thread.sleep(7000);
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C002_Accessing_Incidents_Screen_Dashboard/" + "Incidents View_5 on Dashboard_Screen" + ".jpg");
        Thread.sleep(7000);
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C002_Accessing_Incidents_Screen_Dashboard/" + "Incidents View_6 on Dashboard_Screen" + ".jpg");

        //Click on incident icon on dashboard screen and verify the incident screen
        TestBase.getObject("Dashboard_incidents_Icon").click();
        Assert.assertTrue(TestBase.isElementPresent("Incident_text"), "C001: Incidents text  is not present in the header of the Incidents screen");
        Assert.assertTrue(TestBase.isElementPresent("Sorting_button"), "C002: Sorting_button  is not present in the Incidents screen");
        Assert.assertTrue(TestBase.isElementPresent("Refresh_Button"), "C003: Refresh_Button  is not present in Incidents screen");
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C002_Accessing_Incidents_Screen_Dashboard/" + "Incidents screen" + ".jpg");

        List<MobileElement> incidents = TestBase.driver.findElementsByAndroidUIAutomator("UiSelector().resourceId(\"com.galactio.mobile.sg:id/incidentDesFrameLayout\")");

        //Click on first incident
        incidents.get(0).click();
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C002_Accessing_Incidents_Screen_Dashboard/" + "Incident_View_Screen_first_location" + ".jpg");

        for(int i=0;i<=1;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();
        }
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C002_Accessing_Incidents_Screen_Dashboard/" + "Max_Zoom_In_Incident_View_Screen_first_location" + ".jpg");

        for(int i=0;i<=9;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();
        }
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C002_Accessing_Incidents_Screen_Dashboard/" + "Max_Zoom_Out_Incident_View_Screen_first_location" + ".jpg");

        TestBase.getObject("Verify_Current_Location_button").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C002_Accessing_Incidents_Screen_Dashboard/" + "Current_Location_Incident_View_Screen_first_location" + ".jpg");
        for(int i=0;i<=1;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();}
        for(int i=0;i<=9;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();}
        TestBase.getObject("Back_button").click();

        //Click on second incident
        incidents.get(1).click();
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C002_Accessing_Incidents_Screen_Dashboard/" + "Incident_View_Screen_Second_Location" + ".jpg");

        for(int i=0;i<=1;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();}
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C002_Accessing_Incidents_Screen_Dashboard/" + "Max_Zoom_In_Incident_View_Screen_Second_Location" + ".jpg");

        for(int i=0;i<=9;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();}
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C002_Accessing_Incidents_Screen_Dashboard/" + "Max_Zoom_Out_Incident_View_Screen_Second_Location" + ".jpg");
        TestBase.getObject("Verify_Current_Location_button").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C002_Accessing_Incidents_Screen_Dashboard/" + "Current_Location_Incident_View_Screen_Second_Location" + ".jpg");
        for(int i=0;i<=1;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();}
        for(int i=0;i<=9;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();}
        TestBase.getObject("Back_button").click();

        //Click on third incident
        incidents.get(2).click();
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C002_Accessing_Incidents_Screen_Dashboard/" + "Incident_View_Screen_third_Location" + ".jpg");

        for(int i=0;i<=1;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();}
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C002_Accessing_Incidents_Screen_Dashboard/" + "Max_Zoom_In_Incident_View_Screen_third_Location" + ".jpg");

        for(int i=0;i<=9;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();}
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C002_Accessing_Incidents_Screen_Dashboard/" + "Max_Zoom_Out_Incident_View_Screen_third_Location" + ".jpg");
        TestBase.getObject("Verify_Current_Location_button").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C002_Accessing_Incidents_Screen_Dashboard/" + "Current_Location_Incident_View_Screen_third_Location" + ".jpg");
        for(int i=0;i<=1;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();}
        for(int i=0;i<=9;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();}
        TestBase.getObject("Back_button").click();

        //Click on fourth incident
        incidents.get(3).click();
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C002_Accessing_Incidents_Screen_Dashboard/" + "Incident_View_Screen_fourth_Location" + ".jpg");

        for(int i=0;i<=1;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();}
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C002_Accessing_Incidents_Screen_Dashboard/" + "Max_Zoom_In_Incident_View_Screen_fourth_Location" + ".jpg");

        for(int i=0;i<=9;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();}
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C002_Accessing_Incidents_Screen_Dashboard/" + "Max_Zoom_Out_Incident_View_Screen_fourth_Location" + ".jpg");
        TestBase.getObject("Verify_Current_Location_button").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C002_Accessing_Incidents_Screen_Dashboard/" + "Current_Location_Incident_View_Screen_fourth_Location" + ".jpg");
        for(int i=0;i<=1;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();}
        for(int i=0;i<=9;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();}
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
    }



    @Test(priority = 3)

    public void C003_Checking_the_display_of_Incidents_Screen() throws Exception {

        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Incidents_Button").click();
        Assert.assertTrue(TestBase.isElementPresent("Incident_text"), "C001: Incidents text  is not present in the header of the Incidents screen");
        Assert.assertTrue(TestBase.isElementPresent("Sorting_button"), "C002: Sorting_button  is not present in the Incidents screen");
        Assert.assertTrue(TestBase.isElementPresent("Refresh_Button"), "C003: Refresh_Button  is not present in Incidents screen");
        //Screenshot for list of incidents to verify time and distance of Incidents
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C003_Checking_the_display_of_Incidents_Screen/" + "List of incidents in Incident screen" + ".jpg");
        TestBase.getObject("Back_button").click();
    }


    @Test(priority = 4)

    public void C004_Checking_the_functionality_of_Incident_view_Screen() throws Exception {


        //Click on Option_Button
        TestBase.getObject("Option_Button").click();
        //Click on Incidents_Button
        TestBase.getObject("Verify_Incidents_Button").click();
        List<MobileElement> options = TestBase.driver.findElementsByAndroidUIAutomator("UiSelector().resourceId(\"com.galactio.mobile.sg:id/incidentDesFrameLayout\")");

        //Click on first incident
        options.get(0).click();
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C004_Checking_the_functionality_of_Incident_view_Screen/" + "Incident_View_Screen_first_location " + ".jpg");

        for(int i=0;i<=1;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();}
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C004_Checking_the_functionality_of_Incident_view_Screen/" + "Max_Zoom_In_Incident_View_Screen_first_location " + ".jpg");

        for(int i=0;i<=9;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();}
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C004_Checking_the_functionality_of_Incident_view_Screen/" + "Max_Zoom_Out_Incident_View_Screen_first_location " + ".jpg");
        TestBase.getObject("Verify_Current_Location_button").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Current_Location_Incident_View_Screen_first_location " + ".jpg");
        for(int i=0;i<=1;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();}
        for(int i=0;i<=9;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();}
        TestBase.getObject("Back_button").click();

        //Click on Second incident
        options.get(1).click();
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C004_Checking_the_functionality_of_Incident_view_Screen/" + "Incident_View_Screen_Second location " + ".jpg");

        for(int i=0;i<=1;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();}
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C004_Checking_the_functionality_of_Incident_view_Screen/" + "Max_Zoom_In_Incident_View_Screen_Second location" + ".jpg");

        for(int i=0;i<=9;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();}
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C004_Checking_the_functionality_of_Incident_view_Screen/" + "Max_Zoom_Out_Incident_View_Screen_Second location" + ".jpg");
        TestBase.getObject("Verify_Current_Location_button").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Current_Location_Incident_View_Screen_Second location" + ".jpg");
        for(int i=0;i<=1;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();}
        for(int i=0;i<=9;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();}
        TestBase.getObject("Back_button").click();

        /*//Click on Third incident
        options.get(2).click();
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C004_Checking_the_functionality_of_Incident_view_Screen/" + "Incident_View_Screen_third location " + ".jpg");

        for(int i=0;i<=1;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();}
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C004_Checking_the_functionality_of_Incident_view_Screen/" + "Max_Zoom_In_Incident_View_Screen_third location" + ".jpg");

        for(int i=0;i<=9;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();}
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C004_Checking_the_functionality_of_Incident_view_Screen/" + "Max_Zoom_Out_Incident_View_Screen_third location" + ".jpg");
        TestBase.getObject("Verify_Current_Location_button").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Current_Location_Incident_View_Screen_third location" + ".jpg");
        for(int i=0;i<=1;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();}
        for(int i=0;i<=9;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();}
        TestBase.getObject("Back_button").click();
*/
        /*/*//*//*Click on fourth incident
        options.get(3).click();
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C004_Checking_the_functionality_of_Incident_view_Screen/" + "Incident_View_Screen_fourth_location " + ".jpg");

        for(int i=0;i<=1;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();}
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C004_Checking_the_functionality_of_Incident_view_Screen/" + "Max_Zoom_In_Incident_View_Screen_fourth_location" + ".jpg");

        for(int i=0;i<=9;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();}
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C004_Checking_the_functionality_of_Incident_view_Screen/" + "Max_Zoom_Out_Incident_View_Screen_fourth_location" + ".jpg");
        TestBase.getObject("Verify_Current_Location_button").click();
        TestBase.get_Screenshot("src\\test\\resource\\ROUTE_PREVIEW_FUNCTION\\C003_Checking_the_functionality_for_Route_Preview_Screen/" + "Current_Location_Incident_View_Screen_fourth_location" + ".jpg");
        for(int i=0;i<=1;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();}
        for(int i=0;i<=9;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();}
        TestBase.getObject("Back_button").click();*/

    }


    @Test(priority = 5)

    public void C005_Checking_The_Sort_Functionality() throws Exception {

        TestBase.getObject("Sorting_button").click();
        Assert.assertTrue(TestBase.isElementPresent("SortBy_Text"), "C002:SortBy_Text is not present in the sorting pop up");
        Assert.assertTrue(TestBase.isElementPresent("Sort_By_Time"), "C002:Sort_By_Time text is not present in the sorting pop up");
        Assert.assertTrue(TestBase.isElementPresent("Sort_By_Distance"), "C002:Sort_By_Distance text is not present in the sorting pop up");
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C005_Checking_The_Sort_Functionality/" + "Incidents_Sorting_Pop_Up" + ".jpg");
        TestBase.getObject("Sort_By_Time").click();
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C005_Checking_The_Sort_Functionality/" + "Incidents_Screen1_Sort_By_Time" + ".jpg");
        TestBase.getObject("Sorting_button").click();
        TestBase.getObject("Sort_By_Distance").click();
        TestBase.get_Screenshot("src\\test\\resource\\INCIDENTS_FUNCTIONS\\C005_Checking_The_Sort_Functionality/" + "Incidents_Screen1_Sort_By_Distance" + ".jpg");
    }

    @AfterTest
    public void quit(){

        if((TestBase.driver)!=null){
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        TestBase.driver.quit();
        System.out.println("exit");
    }

}