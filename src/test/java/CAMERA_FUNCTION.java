import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import javax.xml.soap.SOAPFactory;
import java.util.List;

/**
 * Created by QI 33 on 11-05-2016.
 */
public class CAMERA_FUNCTION {
    @Test(priority = 1)

    public void C001_Verifying_Cameras_Button_From_Options() throws Exception {

        SoftAssert softAssert=new SoftAssert();
        TestBase utill = new TestBase();
        utill.initialize();
        //Click on Options button
        TestBase.getObject("Option_Button").click();
        Thread.sleep(2000);
        //Screenshot for Groups_Cameras_screen
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C001_Checking_display_of_Options_menu/" + "Option_Menu_Screen" + ".jpg");
        //Verify the Camera text in option screen
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Camera_Button"), "C001:Camera_Text is not present in the option list  ");
        softAssert.assertAll();
    }

    @Test(priority = 2)

    public void C002_Checking_display_of_Cameras_Screen_UI() throws Exception {

        SoftAssert softAssert=new SoftAssert();
        //verify Group_Tab
        TestBase.getObject("Verify_Camera_Button").click();

        softAssert.assertTrue(TestBase.isElementPresent("Verify_Camera_Text"), "C001:Verify_Camera_Text is not present in the header of the Camera screen ");
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Groups_Tab"), "C001:Groups_Text is not present in the Camera screen ");
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Nearby_Camera_Tab"), "C001:Nearby_Text_Tab is not present in the Camera screen ");
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Favourites_Camera_Tab"), "C001:Favourites_Text_Tab is not present in the Camera screen ");
        softAssert.assertTrue(TestBase.isElementPresent("Refresh_Button"), "C001:Refresh_Button is not present in the Camera screen ");
        softAssert.assertTrue(TestBase.isElementPresent("Map_Button"), "C001:Map_Button is not present in the Camera screen ");
        Thread.sleep(3000);
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C002_Checking_display_of_Cameras_Screen_UI/" + "Camera screen" + ".jpg");
        softAssert.assertAll();
    }


    @Test(priority = 3)

    public void C003_Verifying_The_Functionality_for_Groups() throws Exception {


        SoftAssert softAssert=new SoftAssert();
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C003_Checking_the_functionality_Groups/" + "Real_time_Camera_list_for_First_location" + ".jpg");

       //Click first real camera for first location and take screenshot for it
        List<MobileElement> elements = TestBase.driver.findElements(By.className("android.widget.ImageView"));
        MobileElement element = elements.get(0);
        element.click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C003_Checking_the_functionality_Groups/" + "First_location_Real_time_Camera_pop_up_1" + ".jpg");
        softAssert.assertTrue(TestBase.isElementPresent("Close_Button"), "C001:Close text is not present in the real camera popup");
        TestBase.getObject("Close_Button").click();

        //Click Second real camera for first location and take screenshot for it
        MobileElement element1 = elements.get(1);
        element1.click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C003_Checking_the_functionality_Groups/" + "First_location_Second_Real_time_Camera_pop_up" + ".jpg");
        TestBase.getObject("Close_Button").click();

        //Click Third real camera for first location and take screenshot for it
        MobileElement element2 = elements.get(2);
        element2.click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C003_Checking_the_functionality_Groups/" + "First_location_Third_Real_time_Camera_pop_up" + ".jpg");
        TestBase.getObject("Close_Button").click();


        //Click on group edit field
        TestBase.getObject("Group_edit_field").click();

        //Verify location_2 is present in the list and click on that location and take screenshot
        softAssert.assertTrue(TestBase.isElementPresent("Location_name_2"), "C001:Location_name_2 is not present in the Camera screen ");
        TestBase.getObject("Location_name_2").click();
        Thread.sleep(5000);
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C003_Checking_the_functionality_Groups/" + "Real_time_Camera_list_for_Second_location" + ".jpg");

        //Click First real camera for Second location and take screenshot for it
        MobileElement element3 = elements.get(0);
        element3.click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C003_Checking_the_functionality_Groups/" + "Second_location_Real_time_Camera_pop_up_1" + ".jpg");
        TestBase.getObject("Close_Button").click();

        //Click First real camera for Second location and take screenshot for it
        MobileElement element4 = elements.get(1);
        element4.click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C003_Checking_the_functionality_Groups/" + "Second_location_Real_time_Camera_pop_up_2" + ".jpg");
        TestBase.getObject("Close_Button").click();


        //Click on group edit field
        TestBase.getObject("Group_edit_field").click();


        //Verify location_3 is present in the list and click on that location and take screenshot
        softAssert.assertTrue(TestBase.isElementPresent("Location_name_3"), "C001:Location_name_3 is not present under Group_Tab");
        TestBase.getObject("Location_name_3").click();
        Thread.sleep(5000);
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C003_Checking_the_functionality_Groups/" + "Real_time_Camera_list_for_third_location" + ".jpg");

        //Click First real camera for third location and take screenshot for it
        MobileElement element5 = elements.get(0);
        element5.click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C003_Checking_the_functionality_Groups/" + "Third_location_Real_time_Camera_pop_up_1" + ".jpg");
        TestBase.getObject("Close_Button").click();


        //Click on group edit field
        TestBase.getObject("Group_edit_field").click();

        //Verify location_4 is present in the list and click on that location and take screenshot
        softAssert.assertTrue(TestBase.isElementPresent("Location_name_4"), "C001:Location_name_4 is not present under Group_Tab ");
        TestBase.getObject("Location_name_4").click();
        Thread.sleep(5000);
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C003_Checking_the_functionality_Groups/" + "Real_time_Camera_list_for_Fourth_location" + ".jpg");

        //Click First real camera for fourth location and take screenshot for it
        MobileElement element6 = elements.get(0);
        element6.click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C003_Checking_the_functionality_Groups/" + "Fourth_location_Real_time_Camera_pop_up_1" + ".jpg");
        TestBase.getObject("Close_Button").click();

        //Click Second real camera for fourth location and take screenshot for it
        MobileElement element7 = elements.get(0);
        element7.click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C003_Checking_the_functionality_Groups/" + "Fourth_location_Real_time_Camera_pop_up_2" + ".jpg");
        TestBase.getObject("Close_Button").click();

        softAssert.assertAll();
    }



    @Test(priority = 4)

    public void C004_Checking_the_functionality_for_NEARBY() throws Exception {


        //Click on Nearby Camera Tab and take screenshot
        TestBase.getObject("Verify_Nearby_Camera_Tab").click();
        Thread.sleep(7000);
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C004_Checking_the_functionality_NEARBY/" + "Real_time_Camera_list_for_Nearby_Location" + ".jpg");


        List<MobileElement> elements = TestBase.driver.findElements(By.className("android.widget.ImageView"));
        MobileElement element1 = elements.get(0);
        element1.click();
        Thread.sleep(5000);
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C004_Checking_the_functionality_NEARBY/" + "Real_time_Camera_pop up_1_for_Nearby_Camera_tab" + ".jpg");
        TestBase.getObject("Close_Button").click();

        //Click first real camera for nearby location and take screenshot for it
        MobileElement element2 = elements.get(1);
        element2.click();
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C004_Checking_the_functionality_NEARBY/" + "Real_time_Camera_pop up_2_for_Nearby_Camera_tab" + ".jpg");
        TestBase.getObject("Close_Button").click();


        //Click first real camera for nearby location and take screenshot for it
        MobileElement element3 = elements.get(2);
        element3.click();
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C004_Checking_the_functionality_NEARBY/" + "Real_time_Camera_pop up_3_for_Nearby_Camera_tab" + ".jpg");
        TestBase.getObject("Close_Button").click();

        //Click first real camera for nearby location and take screenshot for it
        MobileElement element4 = elements.get(3);
        element4.click();
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C004_Checking_the_functionality_NEARBY/" + "Real_time_Camera_pop up_4_for_Nearby_Camera_tab" + ".jpg");
        TestBase.getObject("Close_Button").click();

    }




    @Test(priority = 5)

    public void C005_Checking_The_Favourites_Tab_UI_message_From_Camera_Option_When_No_Favourites_Added() throws Exception {

        SoftAssert softAssert=new SoftAssert();
        TestBase.getObject("Verify_Favourites_Camera_Tab").click();
        softAssert.assertTrue(TestBase.isElementPresent("no_favourites_msg1"), "C002:no_favourites_msg1 Text is not present in favourite screen");
        softAssert.assertAll();
    }



    @Test(priority = 6)

    public void C006_Verify_The_Real_Camera_Are_Successfully_Added_In_Favourites_Tab() throws Exception {


        TestBase.getObject("Verify_Nearby_Camera_Tab").click();
        TestBase.getObject("Real_Time_Camera").click();
        TestBase.getObject("Camera_favourite").click();
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C006_Checking_The_Real_Camera_Are_Successfully_Added_In_Favourites_Tab/" + "Toll Tip message for successfully added Real camera as a favourite from nearby" + ".jpg");
        String text_1=TestBase.getObject("verify_real_camera_location_text").getText();
        Thread.sleep(2000);
        TestBase.getObject("Close_Button").click();

        //Added Favourite from group real camera
        TestBase.getObject("Verify_Groups_Tab").click();
        TestBase.getObject("Real_Time_Camera").click();
        TestBase.getObject("Camera_favourite").click();
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C006_Checking_The_Real_Camera_Are_Successfully_Added_In_Favourites_Tab/" + "Toll Tip message for successfully added Real camera as a favourite from group" + ".jpg");
        String text_2=TestBase.getObject("verify_real_camera_location_text").getText();
        System.out.println("text_2");
        Thread.sleep(2000);
        TestBase.getObject("Close_Button").click();

        //Verify two real camera are added as favourite in favourite screen
        TestBase.getObject("Verify_Favourites_Camera_Tab").click();
        Thread.sleep(4000);
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C006_Verify_The_Real_Camera_Are_Successfully_Added_In_Favourites_Tab/" + "Check real camera added in Favourites_Tab" + ".jpg");

        List<MobileElement> elements = TestBase.driver.findElements(By.className("android.widget.ImageView"));
        MobileElement element1 = elements.get(0);
        element1.click();
        if(TestBase.getObject("verify_real_camera_location_text").getText().equals(text_2)) {
            System.out.println("Real camera is added as favourite for nearby location");
            //TestBase.getObject("Real_Time_Camera").click();
            TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C006_Checking_The_Real_Camera_Are_Successfully_Added_In_Favourites_Tab/" + "Real_Camera_popup_1_form_favourite_screen" + ".jpg");
            Thread.sleep(1000);
            TestBase.getObject("Close_Button").click();
        }
        else{
            System.out.println("Same real camera is not added as favourite");
        }


        MobileElement element2 = elements.get(1);
        element2.click();

        if(TestBase.getObject("verify_real_camera_location_text").getText().equals(text_1)) {
            System.out.println("Real camera is added as favourite for group location");
            //TestBase.getObject("Real_Time_Camera").click();
            TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C006_Checking_The_Real_Camera_Are_Successfully_Added_In_Favourites_Tab/" + "Real_Camera_popup_2_form_favourite_screen" + ".jpg");
            TestBase.getObject("Close_Button").click();
        }
        else{
            System.out.println("Same real camera is not added as favourite");
        }
        }


    @Test(priority = 7)

    public void C007_Checking_the_functionality_for_MapButton() throws Exception {

        SoftAssert softAssert=new SoftAssert();
        TestBase.getObject("Map_Button").click();
        Thread.sleep(4000);
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Camera_Text"), "C002:Verify_Camera_Text is not present in the header of cameras screen");
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C007_Checking_the_functionality_for_MapButton/" + "Cameras map view screen" + ".jpg");

        for(int i=0;i<=1;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();}
        for(int i=0;i<=9;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();}
        TestBase.getObject("Back_button").click();
        softAssert.assertAll();
    }


    @Test(priority = 8)

    public void C008_Checking_the_functionality_for_Refresh() throws Exception {

        TestBase.getObject("Refresh_Button").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C008_Checking_the_functionality_for_Refresh/" + "Refresh page" + ".jpg");
    }

    @Test(priority = 9)

    public void
    C009_Verifying_Toast_Message_For_Removed_Favourite_From_Favourite_Tab() throws Exception {

        TestBase.getObject("Verify_Nearby_Camera_Tab").click();
        String text_1=TestBase.getObject("Real_Camera_Text").getText();
        TestBase.getObject("Real_Time_Camera").click();
        TestBase.getObject("Camera_favourite").click();
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C009_Verifying_Toast_Message_For_Removed_Favourite_From_Favourite_Tab/" + "Toll Tip message for successfully removed Real camera as a favourite for nearby" + ".jpg");
        Thread.sleep(2000);

        TestBase.getObject("Close_Button").click();

        TestBase.getObject("Verify_Groups_Tab").click();
        String text_2=TestBase.getObject("Real_Camera_Text").getText();

        TestBase.getObject("Verify_Favourites_Camera_Tab").click();

       // try{
           if((TestBase.getObject("Real_Camera_Text").getText().equals(text_1))){
            System.out.println("Nearby real camera is not removed as a favourite ");

        }
       // catch(Exception e)
       else{
                System.out.println("Nearby real camera is successfully removed as a favourite");
        }

       // try {
            if (TestBase.getObject("Real_Camera_Text").getText().equals(text_2)) {

                System.out.println("Real camera is not removed as a favourite");
            }

            //catch(Exception e){

        else{
                System.out.println("Group real camera is removed as a favourite");
        }
    }


    @Test(priority = 10)

    public void C010_Verifying_different_Actions_performed_on_Map_view() throws Exception {

        SoftAssert softAssert=new SoftAssert();

        //Screenshot for Groups_Cameras_screen
        TestBase.getObject("Map_Button").click();
        Thread.sleep(5000);

        //LIst of Cameras verification
        List<MobileElement> elements = TestBase.driver.findElements(By.className("android.widget.ImageView"));
        MobileElement element1 = elements.get(0);
        element1.click();

        //Verify Close and ShowOnMap button verification
        softAssert.assertTrue(TestBase.isElementPresent("Close_Button"), "C002:Close_Button Text is not present in real camera popup");
        softAssert.assertTrue(TestBase.isElementPresent("Show_On_Map_button"), "C002:Show_On_Map Text is not present in real camera popup");
        Thread.sleep(2000);
        TestBase.getObject("Close_Button").click();

        //Show On Map button functionality:
        element1.click();
        TestBase.getObject("Show_On_Map_button").click();
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C010_Verifying_different_Actions_performed_on_Map_view/" + "Real camera map view screen1" + ".jpg");


        //Zoomin_ZoomOut functionality check:
        for(int i=0;i<=1;i++){
            TestBase.getObject("Verify_Zoom_In_button").click();}
        for(int i=0;i<=9;i++){
            TestBase.getObject("Verify_Zoom_Out_button").click();}
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C010_Verifying_different_Actions_performed_on_Map_view/" + "Real camera map view screen for max zoomout " + ".jpg");
        TestBase.getObject("Verify_Current_Location_button").click();
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C010_Verifying_different_Actions_performed_on_Map_view/" + "Real camera map view screen after tapping current location " + ".jpg");
        softAssert.assertAll();

    }

    @Test(priority = 11)

    public void C011_Verifying_BackButton_Functionality() throws Exception {

        SoftAssert softAssert=new SoftAssert();
        softAssert.assertTrue(TestBase.isElementPresent("Back_button"), "C002:Back_button is not present in camera screen");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        softAssert.assertAll();
    }

    @Test(priority = 12)

    public void C012_Camera_Delete_functionality_From_Favourite_when_OneFavouriteAvailable() throws Exception {

        SoftAssert softAssert=new SoftAssert();
        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Camera_Button").click();
        TestBase.getObject("Verify_Favourites_Camera_Tab").click();
        TestBase.getObject("Real_Time_Camera").click();
        TestBase.getObject("Camera_favourite").click();
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C014_Camera_Delete_functionality_From_Favourite_when_OneFavouriteAvailable/" + "Toll Tip message for successfully removed Real camera as a favourite" + ".jpg");
        Thread.sleep(2000);
        TestBase.getObject("Close_Button").click();
        softAssert.assertTrue(TestBase.isElementPresent("no_favourites_msg1"), "C002:no_favourites_msg1 Text is not present in favourite screen");
        softAssert.assertAll();
    }


    @Test(priority = 13)

    public void C013_Verify_that_the_user_is_able_to_mark_an_image_favourite_from_Map_page() throws Exception {

        TestBase.getObject("Map_Button").click();
        List<MobileElement> elements = TestBase.driver.findElements(By.className("android.widget.ImageView"));
        MobileElement element1 = elements.get(0);
        element1.click();

        String text_1 = TestBase.getObject("camera_text").getText();
        TestBase.getObject("Camera_favourite").click();
        Thread.sleep(1000);
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C013_Verify_that_the_user_is_able_to_mark_an_image_favourite_from_Map_page/" + "Toll Tip message for successfully add map view camera as a favourite" + ".jpg");
        TestBase.getObject("Close_Button").click();

        TestBase.getObject("Back_Button").click();

        TestBase.getObject("Real_Time_Camera").click();

        if ((TestBase.getObject("camera_text").getText().equals(text_1))) {
            System.out.println("Map View camera is successfully added as favourite");

        } else {
            System.out.println("Map View camera is not successfully added as favourite");
        }
        TestBase.getObject("Close_Button").click();
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C013_Verify_that_the_user_is_able_to_mark_an_image_favourite_from_Map_page/" + "Camera in Favourites tab"  + ".jpg");
    }

    @Test(priority = 14)

    public void C014_Verifying_that_the_user_is_able_to_Remove_Favourite_From_Map_page() throws Exception {

        SoftAssert softAssert=new SoftAssert();
        TestBase.getObject("Map_Button").click();
        List<MobileElement> elements = TestBase.driver.findElements(By.className("android.widget.ImageView"));
        MobileElement element1 = elements.get(0);
        element1.click();

        
        TestBase.getObject("Camera_favourite").click();
        Thread.sleep(1000);
        TestBase.get_Screenshot("src\\test\\resource\\CAMERA_FUNCTION\\C013_Verify_that_the_user_is_able_to_mark_an_image_favourite_from_Map_page/" + "Toll Tip message for successfully removed map view camera as a favourite" + ".jpg");
        TestBase.getObject("Close_Button").click();

        TestBase.getObject("Back_Button").click();

        softAssert.assertTrue(TestBase.isElementPresent("no_favourites_msg1"), "C002:no_favourites_msg1 Text is not present in favourite screen");

        softAssert.assertAll();
 
    }


    @AfterTest
    public void quit(){

        if((TestBase.driver)!=null){
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        TestBase.driver.quit();
        System.out.println("exit");
    }
}


















