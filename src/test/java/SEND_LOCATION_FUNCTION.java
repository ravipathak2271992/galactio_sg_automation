import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidKeyCode;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

/**
 * Created by QI 33 on 16-05-2016.
 */
public class SEND_LOCATION_FUNCTION {


    public static void scrollToa(String selector) {
        String selectorString = String.format("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView("+ selector +")");

        TestBase.driver.findElement(MobileBy.AndroidUIAutomator(selectorString));
    }

     @Test(priority = 1)

    public void C001_Verify_the_Send_Location_Label_And_Icon_from_Options_Menu() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();
        TestBase.getObject("Option_Button").click();
        TestBase.driver.scrollTo("Send Location");
        Assert.assertTrue(TestBase.isElementPresent("Send_Location_Button"), "C001:SEnd_Location_text is not present in Option screen page");
        TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C001_Verify_the_Send_Location_Label_And_Icon_from_Options_Menu/" + "Send location ICON" + ".jpg");

    }

    @Test(priority = 2)

    //Verify all the text in serach
    public void C002_Verify_the_Search_Location_Screen_After_Tapping_Send_location_Option() throws Exception {

        // Click on Send_Location_Button
        TestBase.getObject("Send_Location_Button").click();
        TestBase.driver.navigate().back();
        Assert.assertTrue(TestBase.isElementPresent("Current_Location_Button"), "C002:Current_Location text is not present in the Search_Location_Screen_");
        Assert.assertTrue(TestBase.isElementPresent("Recent_button"), "C002:Recent text is not present in the Search_Location_Screen_");
        Assert.assertTrue(TestBase.isElementPresent("POI"), "C003: POI text is not present in the Search_Location_Screen_");
        Assert.assertTrue(TestBase.isElementPresent("Favourites"), "C004: Favourites text is not present in the Search_Location_Screen_");
        Assert.assertTrue(TestBase.isElementPresent("Coordinates"), "C005: Coordinates text is not present in the Search_Location_Screen_");
        Assert.assertTrue(TestBase.isElementPresent("My_Home_text_1"), "C006: My_Home text is not present in the Search_Location_Screen_");
        Assert.assertTrue(TestBase.isElementPresent("My_Home_text_2"), "C006: Not set text is not present in the Search_Location_Screen_");
        Assert.assertTrue(TestBase.isElementPresent("My_Office_text_1"), "C006: My_Office_text is not present in the Search_Location_Screen_");
        Assert.assertTrue(TestBase.isElementPresent("My_Office_text_2"), "C007: Not set text is not present in the Search_Location_Screen_");
        TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C002_Verify_the_Send_Location_Icon_from_Options/" + "Verification_Send location ICON" + ".jpg");
    }

    @Test(priority = 3)

    public void C003_Verify_the_Send_Location_Push2Car_and_Pick_Me_Up_Options_Screen_After_Tapping_Current_location() throws Exception {

        //Verify send location feature from current location
        TestBase.getObject("Current_Location_Button").click();

        //verify share location, pick me up and push2car options in send location map view from  current location
        Assert.assertTrue(TestBase.isElementPresent("Send_Location_Button"), "C002:Send_Location_text is not present in Send Location map view screen");//this_need_to_be_change_as_send_location
        Assert.assertTrue(TestBase.isElementPresent("Pick_Me_Up_Button"), "C002:Pick_Me_Up__text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Push2Car_Button"), "C002:Push2Car_Text is not present in Send Location map view screen");
        TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C003_Verify_the_Send_Location_Push2Car_and_Pick_Me_Up_Options_Screen_After_Tapping_Current_location/" + "Verification_Send location_Push2Car_PickMeUp_ICONS" + ".jpg");
        TestBase.driver.navigate().back();

    }

    @Test(priority = 4)


    public void C004_Verify_the_Share_Menu_Icon_In_Send_Location_screen_From_current_Location() throws Exception {

        TestBase.getObject("Share_Icon_Menu_Button").click();
        TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C005_Verify_the_Share_Menu_Button_And_After_Tapping_Verify_the_Send_Location_Push2Car_Pick_Me_Up_Options_Screen/" + "Verification_Send_Location_Push2Car_Pick_Me_Up_Features_Icons_After_Tapping_share_Menu_Icon" + ".jpg");

    }

    @Test(priority = 5)

    public void C005_Verify_the_Send_Location_Push2Car_Pick_Me_Up_Options_Screen_After_Tapping_On_Share_Menu_Icon_From_Current_Location() throws Exception {

        Assert.assertTrue(TestBase.isElementPresent("Send_Location_Button"), "C002:Send_Location_text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Pick_Me_Up_Button"), "C002:Pick_Me_Up__text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Push2Car_Button"), "C002:Push2Car_Text is not present in Send Location map view screen");
        TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C004_Verify_the_Share_Menu_Icon_In_Send_Location_screen_From_current_Location/" + "Verification_Send location_Push2Car_PickMeUp_ICONS" + ".jpg");
    }


    @Test(priority = 6)

    public void C006_Verify_the_Various_Share_Options_In_Send_Location_Screen_From_Current_location() throws Exception {

        //Click on mobile screen
        TestBase.getObject("Send_Location_Button").click();
        Assert.assertTrue(TestBase.isElementPresent("Send_location_text"), "C005:Send_location_text is not present in the header of Send Location screen");
        Assert.assertTrue(TestBase.isElementPresent("Ready_to_Share_Text"), "C005:Ready_to_Share_Text is not present in Send Location screen");
        Assert.assertTrue(TestBase.isElementPresent("Choose_Recipients"), "C005:Share_Location_text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Facebook_Text"), "C005:Facebook_Text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Email_Text"), "C005:Email_Text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Copy_Link_Text"), "C005:Copy_Link_Text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("More_Text"), "C005:More_Text is not present in Send Location map view screen");
        TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C006_Verify_the_Send_Location_Screen_From_Current_location/" + "Verification_Various_Share_Options_Icons" + ".jpg");

    }

    @Test(priority = 7)

    public void C007_Verify_The_Send_Location_Features_From_Email_For_Current_location() throws Exception {
        //String Current_Location_email_content = "Hi,I'm using Galactio GPS Navigation App and I have sent you a location!";
        TestBase.getObject("Email_Text").click();

        Assert.assertTrue(TestBase.isElementPresent("Gmail_Text"), "C007:Gmail_Text is not present in after Email option content");
        TestBase.getObject("Gmail_Text").click();
        Thread.sleep(2000);
        TestBase.driver.navigate().back();
        if ((TestBase.getObject("Email_Content_Box").getText().contains("I'm using Galactio GPS Navigation App and I have sent you a location!"))&&(TestBase.getObject("Email_Content_Box").getText().contains("Hi,"))) {
            TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C007_Verify_The_Send_Location_Features_From_Email_For_Current_location/" + "Verify_current_location_email_content" + ".jpg");
            TestBase.getObject("Mail_To_Send_Edit_field_Intex_Device").click();
            TestBase.getObject("Mail_To_Send_Edit_field_Intex_Device").sendKeys(TestBase.TestData.getProperty("Mail_to_send_email_id"));
            TestBase.getObject("Mail_Id_Send_button_Intex_Device").click();
        } else {
            System.out.println("error");
       }

    }

    @Test(priority = 8)

    public void C008_Verify_The_Send_Location_post_Feature_from_Facebook_for_Current_Location() throws Exception {

        //Verify and Click on Send icon option button and share location button

        TestBase.getObject("Facebook_Text").click();
        Thread.sleep(3000);
        TestBase.driver.navigate().back();
        TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C008_Verify_The_Send_Location_post_Feature_from_Facebook_for_Current_Location/" + "Share To facebook screen1" + ".jpg");
        TestBase.driver.navigate().back();
        TestBase.getObject("Facebook_Keep_Button").click();
        TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C008_Verify_The_Send_Location_post_Feature_from_Facebook_for_Current_Location/" + "Share To facebook screen2" + ".jpg");
        TestBase.driver.navigate().back();
        TestBase.getObject("Facebook_Discard_button").click();

        TestBase.getObject("Back_button").click();
        for (int i = 0; i < 2; i++) {
            TestBase.getObject("Verify_Zoom_In_button").click();
        }
        for (int i = 0; i <= 9; i++) {
            TestBase.getObject("Verify_Zoom_Out_button").click();
        }

        TestBase.getObject("Back_button").click();
    }



     @Test(priority = 9)

    public void C009_Verify_The_Recent_Screen_UI_message_From_Send_Location_Option_When_No_Recents_Are_Added() throws Exception {

         Assert.assertTrue(TestBase.isElementPresent("Recent_button"), "C004:Recent text is not present in the Search_Destination_screen");
         TestBase.getObject("Recent_button").click();

         //Verify no recent location in recent screen
         Assert.assertTrue(TestBase.isElementPresent("Recent_button"), "C004: Recent  text is not present in the header of Recent_screen");
         Assert.assertTrue(TestBase.isElementPresent("No_Recent_Yet_Text"), "C004:No Recent yet text is not present in the Recent_screen");
         TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C009_Verify_The_Recent_Screen_UI_message_From_Send_Location_Option_When_No_Recents_Are_Added/" + "Recent_Screen" + ".jpg");
         TestBase.getObject("Back_button").click();
         TestBase.getObject("Back_button").click();
     }

         @Test(priority = 10)

         public void C0010_Verify_Successfully_Addition_Of_Location_In_Recent_Screen() throws Exception {

             //Add one recent from Fuel POI
             TestBase.getObject("Search_option").click();
             TestBase.getObject("POI").click();
             TestBase.getObject("Fuel").click();
             TestBase.getObject("Esso").click();
             TestBase.getObject("EssoSubcategories1").click();
             String POI1 = TestBase.getObject("verify_POI").getText();

             //Back four times to go to dashboard screen
             for (int i = 0; i <= 4; i++) {
                 TestBase.getObject("Back_button").click();
             }

             //Tap on recent button from send location option
             TestBase.getObject("Option_Button").click();
             TestBase.getObject("Send_Location_Button").click();

             //click Recent button and verify that one location added as Recent is same or not
             TestBase.getObject("Recent_button").click();
             if (TestBase.getObject("EssoSubcategories1").getText().equals(POI1)) {
                 System.out.println("Right recent added in Recent screen");
             } else {
                 Assert.assertTrue(1 > 2, "Wrong Recent are added in Recent screen");
             }
         }


    @Test(priority = 11)
    public void C0011_Verify_the_Send_Location_Push2Car_And_Pick_Me_Up_Options_Screen_After_Tapping_Recent() throws Exception {

        //verify share location, pick me up and push2car options in send location map view from Recent
        TestBase.getObject("EssoSubcategories1").click();
        Assert.assertTrue(TestBase.isElementPresent("Send_Location_Button"), "C002:Send_Location_text is not present in Send Location map view screen");//this_need_to_be_change_as_send_location
        Assert.assertTrue(TestBase.isElementPresent("Pick_Me_Up_Button"), "C002:Pick_Me_Up__text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Push2Car_Button"), "C002:Push2Car_Text is not present in Send Location map view screen");
        TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C0011_Verify_the_Send_Location_Push2Car_And_Pick_Me_Up_Options_Screen_After_Tapping_Recent/" + "Verification_Send_Location_Push2Car_Pick_Me_Up_Features_Icons" + ".jpg");
    }

    @Test(priority = 12)

    public void C0012_Verify_the_Various_Share_Options_In_Send_Location_Screen_From_Recent() throws Exception {

        //Click on mobile screen
        TestBase.getObject("Send_Location_Button").click();
        Assert.assertTrue(TestBase.isElementPresent("Send_location_text"), "C005:Send_location_text is not present in the header of Send Location screen");
        Assert.assertTrue(TestBase.isElementPresent("Ready_to_Share_Text"), "C005:Ready_to_Share_Text is not present in Send Location screen");
        Assert.assertTrue(TestBase.isElementPresent("Choose_Recipients"), "C005:Share_Location_text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Facebook_Text"), "C005:Facebook_Text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Email_Text"), "C005:Email_Text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Copy_Link_Text"), "C005:Copy_Link_Text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("More_Text"), "C005:More_Text is not present in Send Location map view screen");
        TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C0012_Verify_the_Various_Share_Options_In_Send_Location_Screen_From_Recent/" + "Verification_Various_Share_Options_Icons" + ".jpg");

    }

    @Test(priority = 13)

    public void C0013_Verify_Successfully_Send_The_Recent_location_Through_Email() throws Exception {

         TestBase.getObject("Email_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("Gmail_Text"),"C008:Gmail_Text is not present in after Email option content");
        TestBase.getObject("Gmail_Text").click();
        Thread.sleep(2000);
        TestBase.driver.navigate().back();
        if ((TestBase.getObject("Email_Content_Box").getText().contains("I'm using Galactio GPS Navigation App and I have sent you a location!"))&&(TestBase.getObject("Email_Content_Box").getText().contains("Hi,"))) {

            TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C0013_Verify_Successfully_Send_The_Recent_location_Through_Email/" + "Verify_Recent_location_email_content" + ".jpg");TestBase.getObject("Mail_To_Send_Edit_field_Intex_Device").click();
        TestBase.getObject("Mail_To_Send_Edit_field_Intex_Device").sendKeys(TestBase.TestData.getProperty("Mail_to_send_email_id"));
        TestBase.getObject("Mail_Id_Send_button_Intex_Device").click();}
            else {
                System.out.println("error");
            }
     }

         @Test(priority = 14)

         public void C0014_Verify_The_Send_Location_Features_For_Copy_Link_From_Recent() throws Exception {
             TestBase.getObject("Copy_Link_Text").click();
             TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C0014_Verify_The_Send_Location_Features_For_Copy_Link_From_Recent/" + "CopyLink Toast message screenshot" + ".jpg");

        TestBase.getObject("Back_button").click();
        for (int i = 0; i < 2; i++) {
            TestBase.getObject("Verify_Zoom_In_button").click();
        }
        for (int i = 0; i <= 9; i++) {
            TestBase.getObject("Verify_Zoom_Out_button").click();
        }

        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();

         }


    @Test(priority = 15)

    public void C0015_Verify_Send_Location_Push2Car_Pick_Me_Up_Features_After_Tapping_Any_POI() throws Exception {

        TestBase.getObject("POI").click();
        TestBase.getObject("Transport").click();
        TestBase.getObject("Taxi_Stand").click();
        TestBase.getObject("Taxi_Stand_Subcategories1").click();

        //verify share location, pick me up and push2car options in send location map view from Recent
        Assert.assertTrue(TestBase.isElementPresent("Send_Location_Button"), "C002:Send_Location_text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Pick_Me_Up_Button"), "C002:Pick_Me_Up__text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Push2Car_Button"), "C002:Push2Car_Text is not present in Send Location map view screen");
        TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C0015_Verify_Send_Location_Push2Car_Pick_Me_Up_Features_After_Tapping_Any_POI/" + "Verification_Send_Location_Push2Car_Pick_Me_Up_Features_Icons" + ".jpg");
    }


    @Test(priority = 16)

    public void Verify_the_Various_Share_Options_In_Send_Location_Screen_From_Any_POI() throws Exception {
        //Click on mobile screen
        TestBase.getObject("Send_Location_Button").click();
        Assert.assertTrue(TestBase.isElementPresent("Send_location_text"), "C005:Send_location_text is not present in the header of Send Location screen");
        Assert.assertTrue(TestBase.isElementPresent("Ready_to_Share_Text"), "C005:Ready_to_Share_Text is not present in Send Location screen");
        Assert.assertTrue(TestBase.isElementPresent("Choose_Recipients"), "C005:Share_Location_text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Facebook_Text"), "C005:Facebook_Text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Email_Text"), "C005:Email_Text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Copy_Link_Text"), "C005:Copy_Link_Text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("More_Text"), "C005:More_Text is not present in Send Location map view screen");
        TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\Verify_the_Various_Share_Options_In_Send_Location_Screen_From_Any_POI/" + "Verification_Various_Share_Options_Icons" + ".jpg");

    }

    @Test(priority = 17)

    public void C0017_Verify_Successfully_Send_Any_POI_location_Through_Email() throws Exception {

        TestBase.getObject("Send_Location_Button").click();
        TestBase.getObject("Email_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("Gmail_Text"),"C0017:Gmail_Text is not present in after Email option content");
        TestBase.getObject("Gmail_Text").click();
        Thread.sleep(2000);
        TestBase.driver.navigate().back();
        if ((TestBase.getObject("Email_Content_Box").getText().contains("I'm using Galactio GPS Navigation App and I have sent you a location!"))&&(TestBase.getObject("Email_Content_Box").getText().contains("Hi,"))) {
            TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C0017_Verify_Successfully_Send_Any_POI_location_Through_Email/" + "Verify_POI_location_email_content" + ".jpg");
            TestBase.getObject("Mail_To_Send_Edit_field_Intex_Device").click();
            TestBase.getObject("Mail_To_Send_Edit_field_Intex_Device").sendKeys(TestBase.TestData.getProperty("Mail_to_send_email_id"));
            TestBase.getObject("Mail_Id_Send_button_Intex_Device").click();}
        else {
            System.out.println("error");
        }

        for (int i = 0; i <= 4; i++) {
            TestBase.getObject("Back_button").click();
        }
    }


        @Test(priority = 18)

        public void C0018_Verify_Blank_Favourite_Screen_From_Send_location() throws Exception {

            TestBase.getObject("Favourites").click();
            Assert.assertTrue(TestBase.isElementPresent("No_Favourites_Yet_Text"), "C006:No_Favourites_Yet_Text is not present in Favourites screen from Send Location");
            for (int i = 0; i <= 1; i++) {
                TestBase.getObject("Back_button").click();
                TestBase.getObject("Back_button").click();
            }
        }

    @Test(priority = 19)

    public void C0019_Verify_Successfully_Addition_Of_Any_Location_In_Favourites_Screen() throws Exception {

       TestBase.getObject("Search_option").click();

        //Add one location of Transport as Favourite
        TestBase.getObject("POI").click();
        TestBase.getObject("Transport").click();
        TestBase.getObject("Taxi_Stand").click();
        TestBase.getObject("Taxi_Stand_Subcategories1").click();
        TestBase.getObject("Favourite_Star_Button").click();
        TestBase.getObject("Ok_Button").click();
        String POI_1 = TestBase.getObject("verify_POI").getText();

        for (int i = 0; i <= 4; i++) {
            TestBase.getObject("Back_button").click();
        }

        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Send_Location_Button").click();

        //click Favourites button and verify that one location added as Recent is same or not
        TestBase.getObject("Favourites").click();
        if (TestBase.getObject("Taxi_Stand_Subcategories1").getText().equals(POI_1)) {
            System.out.println("Right Favourite added in Favourite screen");
        } else {
            Assert.assertTrue(1 > 2, "Wrong Recent are added in Recent screen");
        }
    }

    @Test(priority = 20)

    public void C0020_Verify_Send_Location_Push2Car_And_Pick_Me_Up_Features_After_Tapping_Any_Favourites_Location() throws Exception {

        //verify send location, pick me up and push2car options in send location map view from Recent
        TestBase.getObject("Taxi_Stand_Subcategories1").click();
        Assert.assertTrue(TestBase.isElementPresent("Send_Location_Button"), "C002:Send_Location_text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Pick_Me_Up_Button"), "C002:Pick_Me_Up__text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Push2Car_Button"), "C002:Push2Car_Text is not present in Send Location map view screen");
        TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C0020_Verify_Send_Location_Push2Car_Pick_Me_Up_Features_After_Tapping_Any_Favourites_Location/" + "Verification_Send_Location_Push2Car_Pick_Me_Up_Features_Icons" + ".jpg");

    }

    @Test(priority = 21)
    public void  C0021_Verify_the_Various_Share_Options_In_Send_Location_Screen_From_Any_Favourites_Location() throws Exception {
        //Click on mobile screen
        TestBase.getObject("Send_Location_Button").click();
        Assert.assertTrue(TestBase.isElementPresent("Send_location_text"), "C005:Send_location_text is not present in the header of Send Location screen");
        Assert.assertTrue(TestBase.isElementPresent("Ready_to_Share_Text"), "C005:Ready_to_Share_Text is not present in Send Location screen");
        Assert.assertTrue(TestBase.isElementPresent("Choose_Recipients"), "C005:Share_Location_text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Facebook_Text"), "C005:Facebook_Text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Email_Text"), "C005:Email_Text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Copy_Link_Text"), "C005:Copy_Link_Text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("More_Text"), "C005:More_Text is not present in Send Location map view screen");
        TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C0021_Verify_the_Various_Share_Options_In_Send_Location_Screen_From_Any_Favourites_Location/" + "" + ".jpg");

    }

    @Test(priority = 22)
    public void  C0022_Verify_Successfully_Send_Any_Favourites_location_Through_Email() throws Exception {

        TestBase.getObject("Send_Location_Button").click();
        TestBase.getObject("Email_Text").click();

        Assert.assertTrue(TestBase.isElementPresent("Gmail_Text"), "C0021:Gmail_Text is not present in after Email option content");
        TestBase.getObject("Gmail_Text").click();
        Thread.sleep(2000);
        TestBase.driver.navigate().back();
        if ((TestBase.getObject("Email_Content_Box").getText().contains("I'm using Galactio GPS Navigation App and I have sent you a location!")) && (TestBase.getObject("Email_Content_Box").getText().contains("Hi,"))) {
            TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C0022_Verify_Successfully_Send_Any_Favourites_location_Through_Email/" + "Verify_Favourite_location_email_content" + ".jpg");
            TestBase.getObject("Mail_To_Send_Edit_field_Intex_Device").click();
            TestBase.getObject("Mail_To_Send_Edit_field_Intex_Device").sendKeys(TestBase.TestData.getProperty("Mail_to_send_email_id"));
            TestBase.getObject("Mail_Id_Send_button_Intex_Device").click();
        } else {
            System.out.println("erroe");
        }
    }

        @Test(priority = 23)
        public void  C0023_Verify_The_Send_Location_Post_Feature_from_Facebook_For_Favourite() throws Exception {
            TestBase.getObject("Facebook_Text").click();
            Thread.sleep(3000);
            TestBase.driver.navigate().back();
            TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C0023_Verify_The_Send_Location_Post_Feature_from_Facebook_For_Favourite/" + "Share To facebook screen" + ".jpg");
            TestBase.driver.navigate().back();
            TestBase.getObject("Facebook_Discard_button").click();

            TestBase.getObject("Back_button").click();
            for (int i = 0; i < 2; i++) {
                TestBase.getObject("Verify_Zoom_In_button").click();
            }
            for (int i = 0; i <= 9; i++) {
                TestBase.getObject("Verify_Zoom_Out_button").click();
            }
            TestBase.getObject("Back_button").click();
            TestBase.getObject("Back_button").click();
        }

//Coordinates
    @Test(priority = 24)

    public void C0024_Verify_Send_Location_Push2Car_And_Pick_Me_Up_Features_After_Any_Coordinates_Location() throws Exception {

        TestBase.getObject("Coordinates").click();
        TestBase.getObject("Latitude").click();
        TestBase.getObject("Latitude").sendKeys(TestBase.TestData.getProperty("Latitude2"));
        TestBase.getObject("Longitude").click();
        TestBase.getObject("Longitude").sendKeys(TestBase.TestData.getProperty("Longitude2"));
        TestBase.getObject("Ok_Button").click();
        Thread.sleep(2000);

        //verify send location, pick me up and push2car options in send location map view from Recent
        Assert.assertTrue(TestBase.isElementPresent("Send_Location_Button"), "C002:Send_Location_text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Pick_Me_Up_Button"), "C002:Pick_Me_Up__text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Push2Car_Button"), "C002:Push2Car_Text is not present in Send Location map view screen");
        TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C0024_Verify_Send_Location_Push2Car_And_Pick_Me_Up_Features_After_Any_Coordinates_Location/" + "Verification_Send_Location_Push2Car_Pick_Me_Up_Features_Icons" + ".jpg");
    }

    @Test(priority = 25)

    public void C0025_Verify_the_Various_Share_Options_In_Send_Location_Screen_From_Any_Coordinates_Location() throws Exception {

        TestBase.getObject("Send_Location_Button").click();
        Assert.assertTrue(TestBase.isElementPresent("Send_location_text"), "C005:Send_location_text is not present in the header of Send Location screen");
        Assert.assertTrue(TestBase.isElementPresent("Ready_to_Share_Text"), "C005:Ready_to_Share_Text is not present in Send Location screen");
        Assert.assertTrue(TestBase.isElementPresent("Choose_Recipients"), "C005:Share_Location_text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Facebook_Text"), "C005:Facebook_Text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Email_Text"), "C005:Email_Text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Copy_Link_Text"), "C005:Copy_Link_Text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("More_Text"), "C005:More_Text is not present in Send Location map view screen");
        TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C0025_Verify_the_Various_Share_Options_In_Send_Location_Screen_From_Any_Coordinates_Location/" + "Various_Share_Options_Icons" + ".jpg");
    }

    @Test(priority = 26)

    public void C0026_Verify_Successfully_Send_Any_Coordinates_location_Through_Email() throws Exception {

        TestBase.getObject("Email_Text").click();

        Assert.assertTrue(TestBase.isElementPresent("Gmail_Text"), "C0025:Gmail_Text is not present in after Email option content");
        TestBase.getObject("Gmail_Text").click();
        Thread.sleep(2000);
        TestBase.driver.navigate().back();
        if ((TestBase.getObject("Email_Content_Box").getText().contains("I'm using Galactio GPS Navigation App and I have sent you a location!")) && (TestBase.getObject("Email_Content_Box").getText().contains("Hi,"))) {
            TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C0026_Verify_Successfully_Send_Any_Coordinates_location_Through_Email/" + "Verify_Coordinates_location_email_content" + ".jpg");
            TestBase.getObject("Mail_To_Send_Edit_field_Intex_Device").click();
            TestBase.getObject("Mail_To_Send_Edit_field_Intex_Device").sendKeys(TestBase.TestData.getProperty("Mail_to_send_email_id"));
            TestBase.getObject("Mail_Id_Send_button_Intex_Device").click();
        } else {
            System.out.println("error");
        }

        for (int i = 0; i <= 2; i++) {
            TestBase.getObject("Back_button").click();
        }
    }

//Home
    @Test(priority = 27)

    public void C0027_Verify_Send_Location_Push2Car_And_Pick_Me_Up_Features_After_Tapping_My_Home() throws Exception {

        TestBase.getObject("My_Home_text_1").click();
        TestBase.getObject("Yes_Button").click();
        TestBase.getObject("Current_Location_Button").click();
        TestBase.getObject("Save_Only_Button").click();
        TestBase.getObject("My_Home_text_1").click();

        //verify send location, pick me up and push2car options in send location map view from Recent
        Assert.assertTrue(TestBase.isElementPresent("Send_Location_Button"), "C002:Send_Location_text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Pick_Me_Up_Button"), "C002:Pick_Me_Up__text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Push2Car_Button"), "C002:Push2Car_Text is not present in Send Location map view screen");
        TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C0027_Verify_Send_Location_Push2Car_And_Pick_Me_Up_Features_After_Tapping_My_Home/" + "Verification_Send_Location_Push2Car_Pick_Me_Up_Features_Icons" + ".jpg");
    }

    @Test(priority = 28)

    public void C0028_Verify_the_Various_Share_Options_In_Send_Location_Screen_From_My_Home_Location() throws Exception {

        TestBase.getObject("Send_Location_Button").click();
        Assert.assertTrue(TestBase.isElementPresent("Send_location_text"), "C005:Send_location_text is not present in the header of Send Location screen");
        Assert.assertTrue(TestBase.isElementPresent("Ready_to_Share_Text"), "C005:Ready_to_Share_Text is not present in Send Location screen");
        Assert.assertTrue(TestBase.isElementPresent("Choose_Recipients"), "C005:Share_Location_text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Facebook_Text"), "C005:Facebook_Text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Email_Text"), "C005:Email_Text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("Copy_Link_Text"), "C005:Copy_Link_Text is not present in Send Location map view screen");
        Assert.assertTrue(TestBase.isElementPresent("More_Text"), "C005:More_Text is not present in Send Location map view screen");
        TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C0028_Verify_the_Various_Share_Options_In_Send_Location_Screen_From_My_Home_Location/" + "Various_Share_Options_Icons" + ".jpg");
    }


    @Test(priority = 29)

    public void C0029_Verify_Successfully_Send_My_Home_location_Through_Email() throws Exception {

        TestBase.getObject("Email_Text").click();

        Assert.assertTrue(TestBase.isElementPresent("Gmail_Text"),"C0028:Gmail_Text is not present in after Email option content");
        TestBase.getObject("Gmail_Text").click();
        Thread.sleep(2000);
        TestBase.driver.navigate().back();
        if ((TestBase.getObject("Email_Content_Box").getText().contains("http://www.mapsynq.com/sg/maps?ll=1.016265,103.581756"))&&(TestBase.getObject("Email_Content_Box").getText().contains("Hi,"))) {
            TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C0029_Verify_Successfully_Send_My_Home_location_Through_Email/" + "Verify_My_Home_location_email_content" + ".jpg");
            TestBase.getObject("Mail_To_Send_Edit_field_Intex_Device").click();
            TestBase.getObject("Mail_To_Send_Edit_field_Intex_Device").sendKeys(TestBase.TestData.getProperty("Mail_to_send_email_id"));
            TestBase.getObject("Mail_Id_Send_button_Intex_Device").click();}
        else {
            System.out.println("error");
        }

    }

    @Test(priority = 30)

    public void C0030_Verify_Successfully_Send_My_Home_location_Through_More() throws Exception {
        TestBase.getObject("More_Text").click();
        Assert.assertTrue(TestBase.isElementPresent("Share_Galactio_Text"), "C0029:Share_Galactio_Text is not present in Send Location More sharing options screen");
        Assert.assertTrue(TestBase.isElementPresent("Messages_Text"), "C0029:Messages_Text is not present in More sharing options screen");
        Assert.assertTrue(TestBase.isElementPresent("Hangout_Text"), "C0013:Hangout_Text is not present in More sharing options screen");
        TestBase.getObject("Messages_Text").click();
        TestBase.driver.navigate().back();
        TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C0030_Verify_Successfully_Send_My_Home_location_Through_More/" + "Verify_My_Home_location_In_Message_content" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Ok_Button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.driver.navigate().back();

    }

    //Office
        @Test(priority = 31)

    public void C0031_Verify_Send_Location_Push2Car_And_Pick_Me_Up_Features_After_Tapping_My_Office() throws Exception {


            TestBase.getObject("My_Office_text_1").click();
            TestBase.getObject("Yes_Button").click();
            TestBase.getObject("Recent_button").click();
            TestBase.getObject("EssoSubcategories1").click();
            TestBase.getObject("Save_Only_Button").click();
            TestBase.driver.navigate().back();
            TestBase.getObject("My_Office_text_1").click();

            //verify send location, pick me up and push2car options in send location map view from Recent
            Assert.assertTrue(TestBase.isElementPresent("Send_Location_Button"), "C002:Send_Location_text is not present in Send Location map view screen");
            Assert.assertTrue(TestBase.isElementPresent("Pick_Me_Up_Button"), "C002:Pick_Me_Up__text is not present in Send Location map view screen");
            Assert.assertTrue(TestBase.isElementPresent("Push2Car_Button"), "C002:Push2Car_Text is not present in Send Location map view screen");
            TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C0031_Verify_Send_Location_Push2Car_And_Pick_Me_Up_Features_After_Tapping_My_Office/" + "Verification_Send_Location_Push2Car_Pick_Me_Up_Features_Icons" + ".jpg");
        }


        @Test(priority = 32)

            public void C0032_Verify_the_Various_Share_Options_In_Send_Location_Screen_From_My_Home() throws Exception {

                TestBase.getObject("Send_Location_Button").click();
                Assert.assertTrue(TestBase.isElementPresent("Send_location_text"), "C005:Send_location_text is not present in the header of Send Location screen");
                Assert.assertTrue(TestBase.isElementPresent("Ready_to_Share_Text"), "C005:Ready_to_Share_Text is not present in Send Location screen");
                Assert.assertTrue(TestBase.isElementPresent("Choose_Recipients"), "C005:Share_Location_text is not present in Send Location map view screen");
                Assert.assertTrue(TestBase.isElementPresent("Facebook_Text"), "C005:Facebook_Text is not present in Send Location map view screen");
                Assert.assertTrue(TestBase.isElementPresent("Email_Text"), "C005:Email_Text is not present in Send Location map view screen");
                Assert.assertTrue(TestBase.isElementPresent("Copy_Link_Text"), "C005:Copy_Link_Text is not present in Send Location map view screen");
                Assert.assertTrue(TestBase.isElementPresent("More_Text"), "C005:More_Text is not present in Send Location map view screen");
                TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C0032_Verify_the_Various_Share_Options_In_Send_Location_Screen_From_My_Home/" + "Various_Share_Options_Icons" + ".jpg");
            }


    @Test(priority = 33)

    public void C0033_Verify_Successfully_Send_My_Office_Location_Through_Email() throws Exception {

        TestBase.getObject("Email_Text").click();

        Assert.assertTrue(TestBase.isElementPresent("Gmail_Text"), "C0032:Gmail_Text is not present in after Email option content");
        TestBase.getObject("Gmail_Text").click();
        Thread.sleep(2000);
        TestBase.driver.navigate().back();
        if ((TestBase.getObject("Email_Content_Box").getText().contains("I'm using Galactio GPS Navigation App and I have sent you a location!")) && (TestBase.getObject("Email_Content_Box").getText().contains("Hi,"))) {
            TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C0033_Verify_Successfully_Send_My_Office_Location_Through_Email/" + "Verify_My_Office_location_email_content" + ".jpg");
            TestBase.getObject("Mail_To_Send_Edit_field_Intex_Device").click();
            TestBase.getObject("Mail_To_Send_Edit_field_Intex_Device").sendKeys(TestBase.TestData.getProperty("Mail_to_send_email_id"));
            TestBase.getObject("Mail_Id_Send_button_Intex_Device").click();
        } else {
            System.out.println("error");
        }

        for (int i = 0; i <= 2; i++) {
            TestBase.getObject("Back_button").click();
        }
    }
        @Test(priority = 34)

        public void C0034_Verify_Successfully_Send_Any_POI_Along_Route_location_Through_Email() throws Exception {


                TestBase.getObject("Search_option").click();
                TestBase.getObject("POI").click();

                //Add Policestation  POI as a favourite and take screenshots for saved to favourite toast message
                TestBase.getObject("Emergency_Text").click();
                TestBase.getObject("Policestation").click();
            TestBase.advertisement_cancel();

                TestBase.getObject("Go_Button").click();
                TestBase.driver.pressKeyCode(AndroidKeyCode.BACK);

            Thread.sleep(2000);
            TestBase.getObject("Option_Button").click();
            TestBase.getObject("Option_Button").click();
            scrollToa(TestBase.OR.getProperty("Send_Location_Button"));
                TestBase.getObject("Send_Location_Button").click();
            TestBase.getObject("POI_Along_Route").click();
            TestBase.advertisement_cancel();
            TestBase.getObject("Send_Location_Button").click();

            TestBase.getObject("Email_Text").click();
            TestBase.getObject("Gmail_Text").click();
            Thread.sleep(2000);
            TestBase.driver.pressKeyCode(AndroidKeyCode.BACK);
            if ((TestBase.getObject("Email_Content_Box").getText().contains("I'm using Galactio GPS Navigation App and I have sent you a location!")) && (TestBase.getObject("Email_Content_Box").getText().contains("Hi,"))) {
                TestBase.get_Screenshot("src\\test\\resource\\SEND_LOCATION_FUNCTION\\C0033_Verify_Successfully_Send_My_Office_Location_Through_Email/" + "Verify_My_Office_location_email_content" + ".jpg");
                TestBase.getObject("Mail_To_Send_Edit_field_Intex_Device").click();
                TestBase.getObject("Mail_To_Send_Edit_field_Intex_Device").sendKeys(TestBase.TestData.getProperty("Mail_to_send_email_id"));
                TestBase.getObject("Mail_Id_Send_button_Intex_Device").click();
            } else {
                System.out.println("error");
            }
            TestBase.getObject("Back_button").click();
            TestBase.driver.pressKeyCode(AndroidKeyCode.BACK);
            TestBase.getObject("Back_button").click();
            TestBase.getObject("Back_button").click();
            TestBase.getObject("Back_button").click();

            TestBase.clear_data();
        }



    @AfterTest
    public void quit(){

        if((TestBase.driver)!=null){
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        TestBase.driver.quit();
        System.out.println("exit");
    }
}