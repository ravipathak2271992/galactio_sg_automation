import io.appium.java_client.MobileBy;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by QI-37 on 23-06-2016.
 */
public class CALENDER {

    public static void scrollToa(String selector) {
        String selectorString = String.format("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView("+ selector +")");

        TestBase.driver.findElement(MobileBy.AndroidUIAutomator(selectorString));
    }

    @Test(priority = 1)
    public void C001_Verify_the_Calender_options_when_not_LoggedIn() throws Exception {

        TestBase util = new TestBase();
        try {
            util.initialize();
        } catch (Exception e) {
            e.printStackTrace();
        }

        TestBase.getObject("Option_Button").click();

        Assert.assertTrue(TestBase.isElementPresent("CALENDER"), "CALENDER label is not there");
        TestBase.get_Screenshot("src\\test\\resource\\CALENDER\\C001_Verify_the_Calender_options_when_not_LoggedIn/" + "screen1" + ".jpg");

        TestBase.getObject("CALENDER").click();

        TestBase.getScreenshotForParticularPart("mapsynq_toast", "src\\test\\resource\\CALENDER\\C001_Verify_the_Calender_options_when_not_LoggedIn/" + "calender_toast" + ".jpg");
        String result = TestBase.TextFromImage("src\\test\\resource\\CALENDER\\C001_Verify_the_Calender_options_when_not_LoggedIn/" + "calender_toast" + ".jpg");
        System.out.println(result);
        String exp = "Please lug in la mapSVNQ to View Calendar";
        if (result.toLowerCase().contains(exp.toLowerCase())) {
            System.out.println("passed");
        } else {
            Assert.assertTrue(1 > 2, "The toast notification is not displayed correctly for incorrect password");
        }

    }

    @Test(priority = 2)
    public void C002_Verify_Calender_Page_when_user_login_is_not_register_with_Mapsynq() throws Exception {

        //Click on Option button and login into mapSYNQ account
        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Account_Button").click();


        TestBase.getObject("email_field").clear();
        TestBase.getObject("email_label").sendKeys(TestBase.TestData.getProperty("not_sync_user_name"));

        TestBase.getObject("pwd_field").clear();
        TestBase.getObject("pwd_field").sendKeys(TestBase.TestData.getProperty("not_sync_pwd"));

        TestBase.driver.navigate().back();
        TestBase.getObject("log_in_btn").click();
        Thread.sleep(5000);

        TestBase.getObject("Back_Button").click();

        //Go to Calendar page and verify the page
        TestBase.getObject("Option_Button").click();
        TestBase.getObject("CALENDER").click();
        Thread.sleep(2000);
        Assert.assertTrue(TestBase.isElementPresent("no_data_image"), "no_data_image is not there");
        Assert.assertTrue(TestBase.isElementPresent("not_sync_calendar_msg"), "no_data_image is not there");
        Thread.sleep(2000);

        //Verify the Header
        Assert.assertTrue(TestBase.isElementPresent("CALENDER"), "no_data_image is not there");
        Assert.assertTrue(TestBase.isElementPresent("Back_Button"), "no_data_image is not there");
        Assert.assertTrue(TestBase.isElementPresent("Refresh_Button_Calendar"), "no_data_image is not there");


        TestBase.get_Screenshot("src\\test\\resource\\CALENDER\\C002_Verify_Calender_Page_when_user_login_not_Sync_with_Mapsynq/" + "screen1" + ".jpg");
        TestBase.getObject("calender_back_btn").click();


        //Verify the Login and logout from the MapSYNQ
        TestBase.getObject("Option_Button").click();
        TestBase.logout_function();
        TestBase.getObject("Back_Button").click();
    }

    @Test(priority = 3)
    public void C003_Verify_Calender_Page_when_user_login_is_registered_with_Mapsynq_acccount() throws Exception {

        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Account_Button").click();
        TestBase.getObject("email_field").clear();
        TestBase.getObject("email_label").sendKeys(TestBase.TestData.getProperty("sync_user_name"));

        TestBase.getObject("pwd_field").clear();
        TestBase.getObject("pwd_field").sendKeys(TestBase.TestData.getProperty("sync_pwd"));

        TestBase.driver.navigate().back();
        TestBase.getObject("log_in_btn").click();

        Assert.assertTrue(TestBase.isElementPresent("Account_display_after_login"), "Account_display_after_login is not there");

        TestBase.getObject("calender_back_btn").click();
        TestBase.getObject("Option_Button").click();
        TestBase.getObject("CALENDER").click();

        Assert.assertTrue(TestBase.isElementPresent("calendar_refresh"), "calendar_refresh is not there");
        Assert.assertTrue(TestBase.isElementPresent("meeting_label"), "meeting_label is not there");
        Assert.assertTrue(TestBase.isElementPresent("meeting_place"), "meeting_place is not there");

        TestBase.get_Screenshot("src\\test\\resource\\CALENDER\\C003_Verify_Calender_Page_when_user_login_Sync_with_Mapsynq_event_already_Created/" + "screen1" + ".jpg");

    }

    @Test(priority = 4)
    public void C004_Verify_the_Results_Page_display() throws Exception {

        TestBase.getObject("meeting_place").click();

        Assert.assertTrue(TestBase.isElementPresent("calendar_results"), "calendar_results is not there");
        Assert.assertTrue(TestBase.isElementPresent("calendar_location"), "calendar_location is not there");

        if (TestBase.getObject("Event_Km").getText().contains("KM")) {

            System.out.println("passed");
        } else {
            Assert.assertTrue(1 > 2, "Location is not there");
        }

        Assert.assertTrue(TestBase.isElementPresent("calendar_filter"), "calendar_filter is not there");

        TestBase.get_Screenshot("src\\test\\resource\\CALENDER\\C005_Verify_the_Results_Page_display/" + "screen1" + ".jpg");
    }

    @Test(priority = 5)
    public void C005_Verify_the_Map_View_display_for_any_Location() throws Exception {

        TestBase.getObject("calendar_location").click();
        Assert.assertTrue(TestBase.isElementPresent("calendar_map_view"), "calendar_map_view is not there");

        TestBase.get_Screenshot("src\\test\\resource\\CALENDER\\C006_Verify_the_Map_View_display_for_any_Location/" + "screen1" + ".jpg");
    }

    @Test(priority = 6)
    public void C006_Click_on_the_Back_button_Verify_navigation() throws Exception {

        TestBase.getObject("calender_back_btn").click();

        Assert.assertTrue(TestBase.isElementPresent("calendar_results"), "calendar_results is not there");
        Assert.assertTrue(TestBase.isElementPresent("calendar_location"), "calendar_location is not there");

        TestBase.getObject("calender_back_btn").click();

        Assert.assertTrue(TestBase.isElementPresent("meeting_label"), "meeting_label is not there");
        Assert.assertTrue(TestBase.isElementPresent("meeting_place"), "meeting_place is not there");
        TestBase.get_Screenshot("src\\test\\resource\\CALENDER\\C008_Click_on_the_Back_button_Verify_navigation/" + "screen1" + ".jpg");
        /*TestBase.getObject("calender_back_btn").click();
        Assert.assertTrue(TestBase.isElementPresent("Option_Button"), "Option_Button is not there");
*/
    }


    @Test(priority = 7)
    public void C007_Verify_the_SearchFilter_Verification_in_Calender_Results_Page() throws Exception {

        //Click on the Event and open in Results page
        TestBase.getObject("meeting_label").click();
        Thread.sleep(2000);


        //Click on Filter option and verify
        TestBase.getObject("calendar_filter").click();


        //Verify all the Options
        //All Options verification for search filter options
        Assert.assertTrue(TestBase.isElementPresent("BY_CATEGORY_Category"), "BY_CATEGORY_Category is not present in screen");
        Assert.assertTrue(TestBase.isElementPresent("POI_Option"), "POI_Option is not present in screen");
        Assert.assertTrue(TestBase.isElementPresent("Address_Option"), "Address_Option is not present in screen");
        Assert.assertTrue(TestBase.isElementPresent("Roads_Option"), "Roads_Option is not present in screen");
        Assert.assertTrue(TestBase.isElementPresent("City_Options"), "City_Options is not present in screen");
        Assert.assertTrue(TestBase.isElementPresent("State_Option"), "State_Option is not present in screen");
        Assert.assertTrue(TestBase.isElementPresent("Country_Option"), "Country_Option is not present in screen");
        Assert.assertTrue(TestBase.isElementPresent("BY_LOCATION_Category"), "BY_LOCATION_Category is not present in screen");

        TestBase.get_Screenshot("src\\test\\resource\\CALENDER\\C009_Verify_the_SearchFilter_Verification_in_Calender_Results_Page/" + "Filter_Options" + ".jpg");

        //Verify the Cross Button present
        Assert.assertTrue(TestBase.isElementPresent("Cross_button"), "Cross_button is present in screen");
        TestBase.getObject("Cross_button").click();

        //Click for Back(<)
        for (int i = 0; i < 2; i++) {
            TestBase.getObject("Back_Button").click();
        }

    }

    @Test(priority = 8)
    public void C008_Verify_Refresh_button_funtionality() throws Exception {

        SoftAssert softAssert = new SoftAssert();

        TestBase util = new TestBase();
        try {
            util.initialize();
        } catch (Exception e) {
            e.printStackTrace();
        }
        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Account_Button").click();
        TestBase.logout_function();
        TestBase.getObject("Email_Edit_Field").click();
        TestBase.getObject("Email_Edit_Field").sendKeys(TestBase.TestData.getProperty("Email"));
        TestBase.getObject("Password_Edit_field").click();
        TestBase.getObject("Password_Edit_field").sendKeys(TestBase.TestData.getProperty("pass"));
        TestBase.getObject("Login_Button").click();

        TestBase.getObject("Back_Button").click();

        TestBase.getObject("Option_Button").click();
        scrollToa(TestBase.OR.getProperty("CALENDER"));

        TestBase.getObject("CALENDER").click();
        softAssert.assertTrue(TestBase.isElementPresent("not_sync_calendar_msg"), "not_sync_calendar_msg text is not displying in Calendar screen");
        TestBase.getObject("Refresh_Button").click();
        softAssert.assertTrue(TestBase.isElementPresent("meeting_place"), "meeting_place is not displaying in Calendar screen");

    }


    @Test(priority = 9)
    public void C009_Verify_for_the_sequential_back_button_Functionality() throws Exception {

        TestBase.getObject("meeting_place").click();
        Assert.assertTrue(TestBase.isElementPresent("calendar_results"), "calendar_results is not there");
        Assert.assertTrue(TestBase.isElementPresent("calendar_location"), "calendar_location is not there");

        TestBase.getObject("calendar_location").click();
        Assert.assertTrue(TestBase.isElementPresent("calendar_map_view"), "calendar_map_view is not there");

        for(int i=0;i<3;i++){

            TestBase.getObject("Back_Button").click();
        }


    }
}