import Negative_Test_Cases.*;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import javax.print.attribute.SetOfIntegerSyntax;

/**
 * Created by QI 33 on 22-06-2016.
 */
public class MAIN_MENU_USER_INTERFACE {


//method for scroll



    //Verifying the splash screen
    @Test(priority = 1)
    public void C001_Galactio_Splash_Screen_Verification() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();
        Thread.sleep(5000);
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C001_Splash_Screen_Verification/" + "Splash screen" + ".jpg");
    }


    // Verifiying Dashboard_UI through screenshot
    @Test(priority = 2)
    public void C002_Verify_the_Dashboard_UI() throws Exception {

        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C002_Verify_the_Dashboard_UI/" + "Dashboard_Screen" + ".jpg");
    }


    // Verifiying All section under SET_DESTINATION section through assert and screenshot
    @Test(priority = 3)
    public void C003_Verify_SET_DESTINATION_Section_In_Application_Dashboard() throws Exception {

        SoftAssert softAssert= new SoftAssert();
        softAssert.assertTrue(TestBase.isElementPresent("Set_Destination_Text"), "C001: Set destination text is not present in the dashboard screen");
        softAssert.assertTrue(TestBase.isElementPresent("Home_Text"), "C001: Home text is not present in the Set Home popup");
        softAssert.assertTrue(TestBase.isElementPresent("Office_Text"), "C001: Office_Textis not present under SET_DESTINATION section on Dashboard screen");
        softAssert.assertTrue(TestBase.isElementPresent("Favourite_Text"), "C001: Favourite_text is not present under SET_DESTINATION section on Dashboard screen");
        softAssert.assertTrue(TestBase.isElementPresent("Search_option"), "C001: Search_option is not present under SET_DESTINATION section on Dashboard screen");
        //TestBase.getScreenshotForParticularPart("Set_Destination_Box", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C003_Verify_SET_DESTINATION_Section_In_Application_Dashboard/" + "Verify Icons under SET DESTINATION sections" + ".jpg");
        softAssert.assertAll();

    }


    // Verifiying AAll section under SET_DESTINATION section through assert and screenshot
    @Test(priority = 4)
    public void C004_Verify_INCIDENTS_Section_In_Application_Dashboard() throws Exception {

        SoftAssert softAssert= new SoftAssert();

        softAssert.assertTrue(TestBase.isElementPresent("Incidents_field"), "C001: Favourites_text is not present in the Set Home popup");

        //Verify the incident box is blank or not
        String Incident = TestBase.getObject("Dashboard_Incidents").getText();
        System.out.println("Incident");
        if ((TestBase.getObject("Dashboard_Incidents").getText().equals(Incident))) {
            System.out.println("Incidents are present");
        } else {
            softAssert.assertTrue(1>2,"Incidents are not present");
        }
        softAssert.assertAll();
    }


    @Test(priority = 5)

    public void C005_Verify_SUGGESTED_FOR_YOU_Section_In_Application_Dashboard() throws Exception {

        SoftAssert softAssert= new SoftAssert();
        softAssert.assertTrue(TestBase.isElementPresent("Suggested_For_You_field"), "C001: Suggested_For_You_box is not present in the dashboard");
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C005_Verify_SUGGESTED_FOR_YOU_Section_In_Application_Dashboard/" + "Verify first card is present or not" + ".jpg");
        softAssert.assertAll();
    }


    @Test(priority = 6)

    public void C006_Verify_All_the_Cards_in_Suggested_for_you_Section() throws Exception {

        SoftAssert softAssert= new SoftAssert();
        //MobileElement element=TestBase.driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(resourceId(\"com.galactio.mobile.sg:id/ignore\"))");

        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card1" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card2" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card3" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card4" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card5" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card6" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card7" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card8" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card9" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card10" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card11" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
        TestBase.getScreenshotForParticularPart("Suggested_For_You_field", "src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Suggested_For_You_Section/" + "Card12" + ".jpg");
        TestBase.getObject("Suggested_For_You_Text_Button").click();
    }

    @Test(priority = 7)

    public void C007_Verify_Navi_Button_In_Application_Dashboard() throws Exception {
        SoftAssert softAssert= new SoftAssert();
        softAssert.assertTrue(TestBase.isElementPresent("Navi_Button"), "C001: Navi_Button is not present in the dashboard screen");
        TestBase.getObject("Navi_Button").click();
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C007_Verify_Navi_Button_In_Application_Dashboard/" + "Navigation_screen_from dashboard" + ".jpg");
        TestBase.getObject("Back_button").click();
        softAssert.assertAll();
    }

    @Test(priority = 8)

    public void C008_Verify_Option_Button_In_Application_Dashboard() throws Exception {

        SoftAssert softAssert= new SoftAssert();
        softAssert.assertTrue(TestBase.isElementPresent("Option_Button"), "C001: Option_Button is not present in the dashboard screen");
        TestBase.getObject("Option_Button").click();
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Account_Button"), "C001: Verify_Account_Button is not present in the option section screen");
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Log_In_Register_Text"), "C001: Verify_Log_In/Register_Text is not present in the option section screen");
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Incidents_Button"), "C001: Verify_Incidents_Button is not present in the option section screen");
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Calendar_Button"), "C001: Verify_Calendar_Button is not present in the option section screen");
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Camera_Button"), "C001: Verify_Camera_Button is not present in the option section screen");
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Parking_Button"), "C001: Verify_Parking_Button is not present in the option section screen");
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Fuel_Button"), "C001: Verify_Fuel_Button is not present in the option section screen");
        softAssert.assertTrue(TestBase.isElementPresent("Prayer_Time_Button"), "C001: Prayer_Time_Button is not present in the option section screen");
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Weather_Button"), "C001: Verify_Weather_Button is not present in the option section screen");
        softAssert.assertTrue(TestBase.isElementPresent("Verify_Reported_Incidents"), "C001: Verify_Reported_Incidents is not present in the dashboard screen");
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C001_Checking_display_of_Options_menu/" + "Verify_Icons_In_Option_Menu_Screen1" + ".jpg");
        TestBase.scrollToa(TestBase.OR.getProperty("Verify_About_Button"));
        softAssert.assertTrue(TestBase.isElementPresent("Share_Position_Button"), "C001: Share_Position_Button is not present in the dashboard screen");
        softAssert.assertTrue(TestBase.isElementPresent("Send_Location_Button"), "C001: Send_Location_Button is not present in the dashboard screen");
        softAssert.assertTrue(TestBase.isElementPresent("Saved_Ads_Button"), "C001: Saved_Ads_Button is not present in the dashboard screen");
        softAssert.assertTrue(TestBase.isElementPresent("Settings_Text"), "C001: Settings_Text is not present in the dashboard screen");
        softAssert.assertTrue(TestBase.isElementPresent("Set_Origin_Button"), "C001: Set_Origin_Button is not present in the dashboard screen");
        softAssert.assertTrue(TestBase.isElementPresent("Verify_About_Button"), "C001: Verify_About_Button is not present in the dashboard screen");
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C001_Checking_display_of_Options_menu/" + "Verify_Icons_In_Option_Menu_Screen2" + ".jpg");
        TestBase.driver.navigate().back();
        softAssert.assertAll();
    }

    @Test(priority = 9)

    public void C009_Verify_Home_Button_In_Application_Dashboard() throws Exception {
        SoftAssert softAssert= new SoftAssert();

        softAssert.assertTrue(TestBase.isElementPresent("Home_Text"), "C001: Home text is not present in the Set Home popup");
        TestBase.getObject("Home_Text").click();

        //Verify the home set popup message along with each text
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C009_Verify_Home_Button_In_Application_Dashboard/" + "Home Set Popup" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("SetNow_Home_Text"), "C001: SetNow_Text is not present in the Set Home popup");
        Assert.assertTrue(TestBase.isElementPresent("No_Button"), "C001: No Button is not present in the Set Home popup");
        Assert.assertTrue(TestBase.isElementPresent("Yes_Button"), "C001: Yes Button is not present in the Set Home popup");
        TestBase.getObject("No_Button").click();
        softAssert.assertAll();
    }

    @Test(priority = 10)

    public void C010_Verify_Office_Button_In_Application_Dashboard() throws Exception {
        SoftAssert softAssert= new SoftAssert();
        softAssert.assertTrue(TestBase.isElementPresent("Office_Text"), "C001: Office text is not present in the Set Home popup");
        TestBase.getObject("Office_Text").click();

        //Verify the office set popup message along with each text
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C010_Verify_Office_Button_In_Application_Dashboard/" + "Office Set Popup" + ".jpg");
        Assert.assertTrue(TestBase.isElementPresent("SetNow_Office_Text"), "C001: SetNow_Text is not present in the Set Home popup");
        Assert.assertTrue(TestBase.isElementPresent("No_Button"), "C001: No Button is not present in the Set Home popup");
        Assert.assertTrue(TestBase.isElementPresent("Yes_Button"), "C001: Yes Button is not present in the Set Home popup");
        TestBase.getObject("No_Button").click();
        softAssert.assertAll();
    }


    @Test(priority = 11)

    public void C011_Verify_Favourite_Button_In_Application_Dashboard() throws Exception {
        SoftAssert softAssert= new SoftAssert();
        softAssert.assertTrue(TestBase.isElementPresent("Favourites_button"), "C001: Favourites_text is not present in the Set Home popup");
        TestBase.getObject("Favourites_button").click();
        Thread.sleep(2000);
        Assert.assertTrue(TestBase.isElementPresent("no_favourites_msg1"), "C001: No Favourites yet... is not present in the Set Home popup");
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C011_Verify_Favourite_Button_In_Application_Dashboard/" + "Blank_Favourite screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        softAssert.assertAll();
    }

    @Test(priority = 12)

    public void C012_Verify_Search_Button_In_Application_Dashboard() throws Exception {

        SoftAssert softAssert= new SoftAssert();
        softAssert.assertTrue(TestBase.isElementPresent("Search_option"), "C001: Search text is not present in the Daashboard screen");
        TestBase.getObject("Search_option").click();

        //Click on phone back button to verify the each option in search destination screen and verify each option
        TestBase.driver.pressKeyCode(AndroidKeyCode.BACK);
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C002_Verify_the_Set_Destination_Page/" + "Search_Destination_screen" + ".jpg");
        softAssert.assertTrue(TestBase.isElementPresent("Recent_button"), "C002:Recent text is not present in the Search_Destination_screen");
        softAssert.assertTrue(TestBase.isElementPresent("POI"), "C003: POI text is not present in the Daashboard screen");
        softAssert.assertTrue(TestBase.isElementPresent("Favourites"), "C004: Favourites text is not present in the Daashboard screen");
        softAssert.assertTrue(TestBase.isElementPresent("Coordinates"), "C005: Coordinates text is not present in the Daashboard screen");
        softAssert.assertTrue(TestBase.isElementPresent("My_Home_text_1"), "C006: My_Home text is not present in the Search_Location_Screen");
        softAssert.assertTrue(TestBase.isElementPresent("My_Home_text_2"), "C006: Not set text is not present in the Search_Location_Screen");
        softAssert.assertTrue(TestBase.isElementPresent("My_Office_text_1"), "C006: My_Office_text is not present in the Search_Location_Screen");
        softAssert.assertTrue(TestBase.isElementPresent("My_Office_text_2"), "C007: Not set text is not present in the Search_Location_Screen");
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C012_Verify_Search_Button_In_Application_Dashboard/" + "All Icons in Search destination screen" + ".jpg");
        TestBase.getObject("Back_button").click();
        softAssert.assertAll();

    }
    @Test(priority = 13)

    public void C013_Verify_Favourite_and_Recent_Location_is_displaying_in_Application_dashboard_Screen() throws Exception {

        TestBase.getObject("Search_option").click();
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Location_1"));
        TestBase.selectFavourite();
        Thread.sleep(1000);
        String POI_1=TestBase.getObject("verify_POI").getText();
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C013_Verify_Favourite_and_Recent_Location_is_displaying_in_Application_dashboard_Screen/" + "Saved to favourite toast message for coordinates location_2" + ".jpg");

       for(int i=0;i<=1;i++) {
           TestBase.getObject("Back_button").click();
       }
        TestBase.getObject("Clear_Data_button").click();

        //Add favourite by search destination for location_2
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Location_2"));
        TestBase.selectFavourite();
        Thread.sleep(1000);
        String POI_2=TestBase.getObject("verify_POI").getText();
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C013_Verify_Favourite_and_Recent_Location_is_displaying_in_Application_dashboard_Screen/" + "Saved to favourite toast message for coordinates location_2" + ".jpg");
        for(int i=0;i<=1;i++) {
            TestBase.getObject("Back_button").click();
        }
        TestBase.getObject("Clear_Data_button").click();

        //Add favourite by search destination for location_3
        TestBase.getObject("Search_destination_field").click();
        TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Location_3"));
        TestBase.getObject("Search_button").click();
        TestBase.getObject("Location_Id").click();
        Thread.sleep(1000);
        String POI_3=TestBase.getObject("verify_POI").getText();
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C013_Verify_Favourite_and_Recent_Location_is_displaying_in_Application_dashboard_Screen/" + "Saved to favourite toast message for coordinates location_3" + ".jpg");
        for(int i=0;i<=1;i++) {
            TestBase.getObject("Back_button").click();
        }
        TestBase.getObject("Clear_Data_button").click();

        //Add favourite by search destination for location_3
        TestBase.getObject("Search_destination_field").click();
        TestBase.getObject("Search_destination_field").sendKeys(TestBase.TestData.getProperty("Location_4"));
        TestBase.getObject("Search_button").click();
        TestBase.getObject("Location_Id").click();
        Thread.sleep(1000);
        String POI_4=TestBase.getObject("verify_POI").getText();
        TestBase.get_Screenshot("src\\test\\resource\\MAIN_MENU_USER_INTERFACE\\C013_Verify_Favourite_and_Recent_Location_is_displaying_in_Application_dashboard_Screen/" + "Saved to favourite toast message for coordinates location_4" + ".jpg");

        for(int i=0;i<=3;i++) {
            TestBase.getObject("Back_button").click();
        }
        MobileElement element=TestBase.driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(resourceId(\"com.galactio.mobile.sg:id/image\"))");

        if(TestBase.getObject("Location_1_text").getText().equals(POI_1)&&TestBase.getObject("Location_2_text").getText().equals(POI_2)){

            System.out.println("Location_1 and Location_2 are successfully seen in dashboard screen as a favourite ");}
        else{
            Assert.assertTrue(1>2, "Wrong Favourites are added in Favourites section");}


        if(TestBase.getObject("Location_3_text").getText().equals(POI_3)&&TestBase.getObject("Location_4_text").getText().equals(POI_4)){

            System.out.println("Location_3 and Location_4 are successfully seen in dashboard screen as a recent ");}
        else{
            Assert.assertTrue(1>2, "Wrong Favourites are added in Favourites section");}


    }

    @Test(priority = 14)

    public void C014_Verify_for_the_elements_under_SET_DESTINATION_Label() throws Exception {

        //Verify the Details
        SoftAssert softAssert=new SoftAssert();
        softAssert.assertTrue(TestBase.isElementPresent("Set_Destination_Text"), "C18301: Set_Destination_Text is not present in the Dashboard screen");
        softAssert.assertTrue(TestBase.isElementPresent("Home_Text"), "C18301: Home text is not present in the Set Home popup");
        softAssert.assertTrue(TestBase.isElementPresent("Office_Text"), "C18301: Office_Textis not present under SET_DESTINATION section on Dashboard screen");
        softAssert.assertTrue(TestBase.isElementPresent("Favourite_Text"), "C18301: Favourite_text is not present under SET_DESTINATION section on Dashboard screen");
        softAssert.assertTrue(TestBase.isElementPresent("Search_option"), "C18301: Search_option is not present under SET_DESTINATION section on Dashboard screen");
        softAssert.assertAll();

    }

    @Test(priority = 15)
    public void C015_Verify_for_the_details_under_INCIDENTS_Label_when_Traffic_details_not_available() throws Exception {

        //Verify the Traffic Data
        Assert.assertTrue(TestBase.isElementPresent("Incidents_field"), "C001: Favourites_text is not present in the Set Home popup");

        //Verify the incident box is blank or not
        String Incident = TestBase.getObject("Dashboard_Incidents").getText();
        try {
            if ((TestBase.getObject("Dashboard_Incidents").getText().equals(Incident))) {
                System.out.println("Incidents are present");
            }
        }
        catch(Exception e) {

            System.out.println("Incidents are not present");
        }

    }


    @Test(priority = 16)
    public void C016_Verify_for_the_items_under_SUGGESTED_FOR_YOU_Label_when_no_Favourite_Recent_available() throws Exception {

        SoftAssert softAssert=new SoftAssert();
        //Verify the sUGGESTED_fOR yOU
        TestBase.getObject("Option_Button").click();
        TestBase.scrollToa(TestBase.OR.getProperty("Verify_About_Button"));
        TestBase.clear_data();

    }



    @AfterTest
        public void quit(){

            if((TestBase.driver)!=null){
                try {
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            TestBase.driver.quit();
            System.out.println("exit");
        }
    }



