import io.appium.java_client.TouchAction;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

/**
 * Created by QI 33 on 29-06-2016.
 */
public class REPORT_INCIDENT {

    @Test(priority = 1)
    public void C001_Verify_the_Report_Incident_from_Options_When_User_is_Not_Login() throws Exception {
        TestBase utill = new TestBase();
        utill.initialize();
        // Click on Option Button
        TestBase.getObject("Option_Button").click();
        // Click on Reported_Incidents Button
        TestBase.getObject("Verify_Reported_Incidents").click();
        Thread.sleep(2000);
        //Screenshot for Favourites_Weather_Screen
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C001_Verify_the_Report_Incident_from_Options_When_User_is_Not_Login/" + "Pop Up Login map synq account" + ".jpg");//Before login into Map synq account
    }

    @Test(priority = 2)
    public void C002_Verify_the_Report_Incident_from_Options_When_User_is_Login() throws Exception {

        TestBase.getObject("Option_Button").click();

        //Call login_function from TesBase class for login into mapsynq account
        TestBase.login_function();
        TestBase.getObject("Back_button").click();

         TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C002_Verify_the_Report_Incident_from_Options_When_User_is_Login/" + "Report_Screen" + ".jpg");
        TestBase.getObject("Back_button").click();
    }


    @Test(priority = 3)
    public void C003_Verify_the_Report_Incident_Page_UI() throws Exception {

        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        Assert.assertTrue(TestBase.isElementPresent("Verify_Reported_Incidents"), "C003:Reported_Incidents_Text is not present in header");
        //Verify  for first three options under TYPE section
        Assert.assertTrue(TestBase.isElementPresent("Type_Text"), "C002:Type_Text is not present in Report screen page");

        Assert.assertTrue(TestBase.isElementPresent("Verify_Congestion_Text"), "C002:Congestion_text is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Accident_Text"), "C002:Accident_Text is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Closure_Text"), "C002:Closure_Text is not present in Report screen page");

        //screenshot for mainly verify the under Type options logo for first three options
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C003_Verify_the_Report_Incident_Page_UI/" + "Report_Screen_1" + ".jpg");

        //swipe function from closure to congestion
        TouchAction action = new TouchAction(TestBase.driver);
        WebElement element1 = TestBase.getObject("Verify_Closure_Text");
        int startY = element1.getLocation().getY() + (element1.getSize().getHeight() / 2);
        int startX = element1.getLocation().getX() + (element1.getSize().getWidth() / 2);
        WebElement element2 = TestBase.getObject("Verify_Congestion_Text");
        int endX = element2.getLocation().getX() + (element2.getSize().getWidth() / 2);
        int endY = element2.getLocation().getY() + (element2.getSize().getHeight() / 2);
        action.press(startX, startY).waitAction(2000).moveTo(endX, endY).release().perform();

        //Verify the next two options under TYPE section
        Assert.assertTrue(TestBase.isElementPresent("Verify_Breakdown_Text"), "C002:Breakdown_Text is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Flashflood_Text"), "C002:Flashflood_Text is not present in Report screen page");

        //screenshot for mainly verify the under Type options logo for next two options
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C003_Verify_the_Report_Incident_Page_UI/" + "Report_Screen_1" + ".jpg");

        //swipe function from  flashfoood to closure
        WebElement element3 = TestBase.getObject("Verify_Flashflood_Text");
        int start_Y = element3.getLocation().getY() + (element3.getSize().getHeight() / 2);
        int start_X = element3.getLocation().getX() + (element3.getSize().getWidth() / 2);
        WebElement element4 = TestBase.getObject("Verify_Closure_Text");
        int end_X = element4.getLocation().getY() + (element4.getSize().getHeight() / 2);
        int end_Y = element4.getLocation().getX() + (element4.getSize().getWidth() / 2);
        action.press(start_X, start_Y).waitAction(2000).moveTo(end_X, end_Y).release().perform();

        //Verify last two options
        Assert.assertTrue(TestBase.isElementPresent("Verify_Roadwork_Text"), "C002:Roadwork_Text is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Others_Text"), "C002:Others_Text is not present in Report screen page");

        //screenshot for mainly verify the under Type options logo for last two options
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C003_Verify_the_Report_Incident_Page_UI/" + "Report_Screen_1" + ".jpg");

        //Verify Lane Type options
        Assert.assertTrue(TestBase.isElementPresent("Lane_Text"), "C002:Lane_Text is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Current_Text"), "C002:Current_Text is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Opposite_Text"), "C002:Opposite_Text is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Others_Roads_Text"), "C002:Others_Roads_Text is not present in Report screen page");

        //Verify How Far options
        Assert.assertTrue(TestBase.isElementPresent("How_Far_Text"), "C002:How_Far_Text is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Behind_Text"), "C002:Behind_Text is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Ahead_Text"), "C002:Ahead_Text is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Distance_1"), "C002:Distance_1 is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Distance_2"), "C002:Distance_2 not present in Report screen page");

        // Screenshot for full report screen page
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C003_Verify_the_Report_Incident_Page_UI/" + "Full_Report_Screen_1" + ".jpg");

        // scroll to details field and Verify Details options
        TestBase.driver.scrollTo("Describe the incident here ...");
        Assert.assertTrue(TestBase.isElementPresent("Details_Text"), "C002:Details_Text is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Describe_the_incidents_here_text"), "C002:Describe_the_incidents_here_text is not present in Report screen page");

        //Verify Add photo and submit button
        Assert.assertTrue(TestBase.isElementPresent("Add_Photo_Button"), "C002:Add_Photo_Button is not present in Report screen page");
        Assert.assertTrue(TestBase.isElementPresent("Submit_Button"), "C002:Submit_Button is not present in Report screen page");
        // Screenshot for full report screen page
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C003_Verify_the_Report_Incident_Page_UI/" + "Full_Report_Screen_2" + ".jpg");
    }


    @Test(priority = 4)
    public void C004_Checking_The_ADD_PHOTO_Function() throws Exception {

        //Add Photo through mobile camera
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C005_Checking_the_Report_Incident_Functionality_For_Second_Accident/" + "Details text and image " + ".jpg");

        //tapping on photo which is taken
        TestBase.getObject("Camera_Image").click();

        //verify the text and and three buttons after tapping on photo
        Assert.assertTrue(TestBase.isElementPresent("Do_You_Want_Image_Text"), "C002:Do_You_Want_Image_Text is not present in Report incident screen ");
        Assert.assertTrue(TestBase.isElementPresent("Cancel_Button"), "C002:Cancel_Button is not present in Report incident screen");
        Assert.assertTrue(TestBase.isElementPresent("Do_You_Want_Image_Text"), "C002:Submit_Button is not present in Report incident screen");

        //click on cancel button and verify that existing photo is present or not
        TestBase.getObject("Cancel_Button").click();
        Assert.assertTrue(TestBase.isElementPresent("Camera_Image"), "C002:Photo is not present in Report incident screen");

        //tapping on photo which is already taken and take new photo
        TestBase.getObject("Camera_Image").click();
        TestBase.getObject("Take_New_Button").click();
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
        Thread.sleep(7000);
        TestBase.get_Screenshot("src\\test\\resource\\REPORTED_INCIDENTS_FUNCTION\\C008_Checking_the_Report_Incident_Functionality_For_Breakdown/" + "Take new image and replace the older one " + ".jpg");

        //Again tap on new photo and check that photo is deleted successfully or not
        TestBase.getObject("Camera_Image").click();
        TestBase.getObject("Delete_Button").click();
        Assert.assertFalse(TestBase.isElementPresent("Camera_Image"), "C002:Photo is not present in Report incident screen");
        TestBase.getObject("Back_button").click();

    }


    @Test(priority = 5)
    public void C005_Checking_the_Report_Incident_Functionality_For_Congestion_Using_Other_Road_Lane() throws Exception {


        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();

        //Report an incident for congestion using other road lane
        TestBase.getObject("Verify_Congestion_Text").click();
        TestBase.getObject("Others_Roads_Text").click();
        TestBase.getObject("Far_Behind_Button").click();
        Thread.sleep(1000);
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter1"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
       // Thread.sleep(2000);
        /*TestBase.getObject("Submit_Button").click();
        Assert.assertTrue(TestBase.isElementPresent("Successfully_Submitted_The_Report_text"), "C005:Submit_Button is not present in Report screen page");
        TestBase.getObject("Close_Button").click();*/ TestBase.getObject("Back_button").click();
    }


    @Test(priority = 6)
    public void C006_Checking_the_Report_Incident_Functionality_For_Congestion_Using_Opposite_Lane() throws Exception {


        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TestBase.getObject("Verify_Congestion_Text").click();
        TestBase.getObject("Opposite_Text").click();
        TestBase.getObject("Far_Behind_Button").click();
        Thread.sleep(1000);
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter2"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
       /* Thread.sleep(2000);
        TestBase.getObject("Submit_Button").click();
        Assert.assertTrue(TestBase.isElementPresent("Successfully_Submitted_The_Report_text"), "C006:Submit_Button is not present in Report screen page");
        TestBase.getObject("Close_Button").click();*/ TestBase.getObject("Back_button").click();

    }

    @Test(priority = 7)
    public void C007_Checking_the_Report_Incident_Functionality_For_Congestion_Using_Current_Lane() throws Exception {

        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TestBase.getObject("Verify_Congestion_Text").click();
        TestBase.getObject("Current_Text").click();
        Thread.sleep(1000);
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter3"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
      /*  Thread.sleep(2000);
        TestBase.getObject("Submit_Button").click();
        Assert.assertTrue(TestBase.isElementPresent("Succesfully_Submitted_The_Report_text"), "C007:Submit_Button is not present in Report screen page");
        TestBase.getObject("Close_Button").click();*/ TestBase.getObject("Back_button").click();

    }

    @Test(priority = 8)
    public void C008_Checking_the_Report_Incident_Functionality_For_Accident_Using_Other_Road_Lane() throws Exception {


        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TestBase.getObject("Verify_Accident_Text").click();
        TestBase.getObject("Others_Roads_Text").click();
        TestBase.getObject("Far_Ahead_Button").click();
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter4"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
       /* TestBase.getObject("Tick_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Submit_Button").click();
        Thread.sleep(7000);
        Assert.assertTrue(TestBase.isElementPresent("Succesfully_Submitted_The_Report_text"), "C008:Submit_Button is not present in Report screen page");
        TestBase.getObject("Close_Button").click();*/ TestBase.getObject("Back_button").click();
    }

    @Test(priority = 9)
    public void C009_Checking_the_Report_Incident_Functionality_For_Accident_Using_Opposite_Lane() throws Exception {


        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TestBase.getObject("Verify_Accident_Text").click();
        TestBase.getObject("Opposite_Text").click();
        TestBase.getObject("Far_Behind_Button").click();
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter5"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
       /* Thread.sleep(2000);
        TestBase.getObject("Submit_Button").click();
        Thread.sleep(7000);
        Assert.assertTrue(TestBase.isElementPresent("Succesfully_Submitted_The_Report_text"), "C009:Submit_Button is not present in Report screen page");
        TestBase.getObject("Close_Button").click();*/ TestBase.getObject("Back_button").click();
    }


    @Test(priority = 10)
    public void C010_Checking_the_Report_Incident_Functionality_For_Accident_Using_Current_Lane() throws Exception {


        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TestBase.getObject("Verify_Accident_Text").click();
        TestBase.getObject("Current_Text").click();
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter6"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
       /* Thread.sleep(2000);
        TestBase.getObject("Submit_Button").click();
        Thread.sleep(7000);
        Assert.assertTrue(TestBase.isElementPresent("Successfully_Submitted_The_Report_text"), "C010:Successfully_Submitted_The_Report_text is not present in Report screen page");
        TestBase.getObject("Close_Button").click();*/ TestBase.getObject("Back_button").click();
    }


    @Test(priority = 11)
    public void C011_Checking_the_Report_Incident_Functionality_For_Closure_Using_Other_Road_Lane() throws Exception {


        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TestBase.getObject("Verify_Closure_Text").click();

        TestBase.getObject("Others_Roads_Text").click();
        TestBase.getObject("Far_Behind_Button").click();
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter7"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
       /* Thread.sleep(2000);
        TestBase.getObject("Submit_Button").click();
        Thread.sleep(7000);
        Assert.assertTrue(TestBase.isElementPresent("Successfully_Submitted_The_Report_text"), "C011:Successfully_Submitted_The_Report_text is not present in Report screen page");
        TestBase.getObject("Close_Button").click();*/ TestBase.getObject("Back_button").click();
    }

    @Test(priority = 12)
    public void C012_Checking_the_Report_Incident_Functionality_For_Closure_Using_Opposite_Lane() throws Exception {

        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TestBase.getObject("Verify_Closure_Text").click();
        TestBase.getObject("Opposite_Text").click();
        TestBase.getObject("Far_Ahead_Button").click();
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter8"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
       /* Thread.sleep(2000);
        TestBase.getObject("Submit_Button").click();
        Thread.sleep(7000);
        Assert.assertTrue(TestBase.isElementPresent("Successfully_Submitted_The_Report_text"), "C012:Successfully_Submitted_The_Report_text is not present in Report screen page");
        TestBase.getObject("Close_Button").click();*/ TestBase.getObject("Back_button").click();
    }

    @Test(priority = 13)
    public void C013_Checking_the_Report_Incident_Functionality_For_Closure_Using_Current_Lane() throws Exception {

        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TestBase.getObject("Verify_Closure_Text").click();
        TestBase.getObject("Current_Text").click();
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter9"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
      /*  Thread.sleep(2000);
        TestBase.getObject("Submit_Button").click();
        Thread.sleep(7000);
        Assert.assertTrue(TestBase.isElementPresent("Successfully_Submitted_The_Report_text"), "C013:Successfully_Submitted_The_Report_text is not present in Report screen page");
        TestBase.getObject("Close_Button").click();*/ TestBase.getObject("Back_button").click();
    }

    @Test(priority = 14)
    public void C014_Checking_the_Report_Incident_Functionality_For_Breakdown_Using_Other_Road_lane() throws Exception {


        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TouchAction action = new TouchAction(TestBase.driver);
        WebElement element1 = TestBase.getObject("Verify_Closure_Text");
        int startY = element1.getLocation().getY() + (element1.getSize().getHeight() / 2);
        int startX = element1.getLocation().getX() + (element1.getSize().getWidth() / 2);
        WebElement element2 = TestBase.getObject("Verify_Congestion_Text");
        int endX = element2.getLocation().getX() + (element2.getSize().getWidth() / 2);
        int endY = element2.getLocation().getY() + (element2.getSize().getHeight() / 2);
        action.press(startX, startY).waitAction(2000).moveTo(endX, endY).release().perform();
        TestBase.getObject("Verify_Breakdown_Text").click();
        TestBase.getObject("Others_Roads_Text").click();
        TestBase.getObject("Far_Behind_Button").click();
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter10"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
        /*Thread.sleep(2000);
        TestBase.getObject("Submit_Button").click();
        Thread.sleep(7000);
        Assert.assertTrue(TestBase.isElementPresent("Successfully_Submitted_The_Report_text"), "C014:Successfully_Submitted_The_Report_text is not present in Report screen page");
        TestBase.getObject("Close_Button").click();*/ TestBase.getObject("Back_button").click();

    }


    @Test(priority = 15)
    public void C015_Checking_the_Report_Incident_Functionality_For_Breakdown_Using_Opposite_lane() throws Exception {


        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TouchAction action = new TouchAction(TestBase.driver);
        WebElement element1 = TestBase.getObject("Verify_Closure_Text");
        int startY = element1.getLocation().getY() + (element1.getSize().getHeight() / 2);
        int startX = element1.getLocation().getX() + (element1.getSize().getWidth() / 2);
        WebElement element2 = TestBase.getObject("Verify_Congestion_Text");
        int endX = element2.getLocation().getX() + (element2.getSize().getWidth() / 2);
        int endY = element2.getLocation().getY() + (element2.getSize().getHeight() / 2);
        action.press(startX, startY).waitAction(2000).moveTo(endX, endY).release().perform();
        TestBase.getObject("Verify_Breakdown_Text").click();
        TestBase.getObject("Opposite_Text").click();
        TestBase.getObject("Far_Behind_Button").click();
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter11"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
       /* Thread.sleep(2000);
        TestBase.getObject("Submit_Button").click();
        Thread.sleep(7000);
        Assert.assertTrue(TestBase.isElementPresent("Successfully_Submitted_The_Report_text"), "C015:Successfully_Submitted_The_Report_text is not present in Report screen page");
        TestBase.getObject("Close_Button").click();*/ TestBase.getObject("Back_button").click();
    }


    @Test(priority = 16)
    public void C016_Checking_the_Report_Incident_Functionality_For_Breakdown_Using_Current_lane() throws Exception {


        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TouchAction action = new TouchAction(TestBase.driver);
        WebElement element1 = TestBase.getObject("Verify_Closure_Text");
        int startY = element1.getLocation().getY() + (element1.getSize().getHeight() / 2);
        int startX = element1.getLocation().getX() + (element1.getSize().getWidth() / 2);
        WebElement element2 = TestBase.getObject("Verify_Congestion_Text");
        int endX = element2.getLocation().getX() + (element2.getSize().getWidth() / 2);
        int endY = element2.getLocation().getY() + (element2.getSize().getHeight() / 2);
        action.press(startX, startY).waitAction(2000).moveTo(endX, endY).release().perform();
        TestBase.getObject("Verify_Breakdown_Text").click();
        TestBase.getObject("Current_Text").click();
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter12"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
       /* Thread.sleep(2000);
        TestBase.getObject("Submit_Button").click();
        Thread.sleep(7000);
        Assert.assertTrue(TestBase.isElementPresent("Successfully_Submitted_The_Report_text"), "C016:Successfully_Submitted_The_Report_text is not present in Report screen page");
        TestBase.getObject("Close_Button").click();
*/ TestBase.getObject("Back_button").click();
    }


    @Test(priority = 17)
    public void C017_Checking_the_Report_Incident_Functionality_For_Flashflood_Using_Others_Road_Lane() throws Exception {


        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TouchAction action = new TouchAction(TestBase.driver);
        WebElement element1 = TestBase.getObject("Verify_Closure_Text");
        int startY = element1.getLocation().getY() + (element1.getSize().getHeight() / 2);
        int startX = element1.getLocation().getX() + (element1.getSize().getWidth() / 2);
        WebElement element2 = TestBase.getObject("Verify_Congestion_Text");
        int endX = element2.getLocation().getX() + (element2.getSize().getWidth() / 2);
        int endY = element2.getLocation().getY() + (element2.getSize().getHeight() / 2);
        action.press(startX, startY).waitAction(2000).moveTo(endX, endY).release().perform();
        TestBase.getObject("Verify_Flashflood_Text").click();
        TestBase.getObject("Others_Roads_Text").click();
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter13"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
        /*Thread.sleep(2000);
        TestBase.getObject("Submit_Button").click();
        Thread.sleep(7000);
        Assert.assertTrue(TestBase.isElementPresent("Successfully_Submitted_The_Report_text"), "C017:Successfully_Submitted_The_Report_text is not present in Report screen page");
        TestBase.getObject("Close_Button").click();
*/ TestBase.getObject("Back_button").click();
    }

    @Test(priority = 18)
    public void C018_Checking_the_Report_Incident_Functionality_For_Flashflood_Using_Opposite_Lane() throws Exception {


        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TouchAction action = new TouchAction(TestBase.driver);
        WebElement element1 = TestBase.getObject("Verify_Closure_Text");
        int startY = element1.getLocation().getY() + (element1.getSize().getHeight() / 2);
        int startX = element1.getLocation().getX() + (element1.getSize().getWidth() / 2);
        WebElement element2 = TestBase.getObject("Verify_Congestion_Text");
        int endX = element2.getLocation().getX() + (element2.getSize().getWidth() / 2);
        int endY = element2.getLocation().getY() + (element2.getSize().getHeight() / 2);
        action.press(startX, startY).waitAction(2000).moveTo(endX, endY).release().perform();
        TestBase.getObject("Verify_Flashflood_Text").click();
        TestBase.getObject("Opposite_Text").click();
        TestBase.getObject("Far_Ahead_Button").click();
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter14"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
        /*Thread.sleep(2000);
        TestBase.getObject("Submit_Button").click();
        Thread.sleep(7000);
        Assert.assertTrue(TestBase.isElementPresent("Successfully_Submitted_The_Report_text"), "C018:Successfully_Submitted_The_Report_text is not present in Report screen page");
        TestBase.getObject("Close_Button").click();*/
        TestBase.getObject("Back_button").click();
    }

    @Test(priority = 19)
    public void C019_Checking_the_Report_Incident_Functionality_For_Flashflood_Using_Current_Lane() throws Exception {


        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TouchAction action = new TouchAction(TestBase.driver);
        WebElement element1 = TestBase.getObject("Verify_Closure_Text");
        int startY = element1.getLocation().getY() + (element1.getSize().getHeight() / 2);
        int startX = element1.getLocation().getX() + (element1.getSize().getWidth() / 2);
        WebElement element2 = TestBase.getObject("Verify_Congestion_Text");
        int endX = element2.getLocation().getX() + (element2.getSize().getWidth() / 2);
        int endY = element2.getLocation().getY() + (element2.getSize().getHeight() / 2);
        action.press(startX, startY).waitAction(2000).moveTo(endX, endY).release().perform();
        TestBase.getObject("Verify_Flashflood_Text").click();
        TestBase.getObject("Current_Text").click();
        TestBase.getObject("Far_Ahead_Button").click();
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter15"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
       /* Thread.sleep(2000);
        TestBase.getObject("Submit_Button").click();
        Thread.sleep(7000);
        Assert.assertTrue(TestBase.isElementPresent("Successfully_Submitted_The_Report_text"), "C019:Successfully_Submitted_The_Report_text is not present in Report screen page");
        TestBase.getObject("Close_Button").click();*/
        TestBase.getObject("Back_button").click();
    }

    @Test(priority = 20)
    public void C020_Checking_the_Report_Incident_Functionality_For_Roadwork_Using_Other_Road_Lane() throws Exception {


        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TouchAction action = new TouchAction(TestBase.driver);
        WebElement element1 = TestBase.getObject("Verify_Closure_Text");
        int startY = element1.getLocation().getY() + (element1.getSize().getHeight() / 2);
        int startX = element1.getLocation().getX() + (element1.getSize().getWidth() / 2);
        WebElement element2 = TestBase.getObject("Verify_Congestion_Text");
        int endX = element2.getLocation().getX() + (element2.getSize().getWidth() / 2);
        int endY = element2.getLocation().getY() + (element2.getSize().getHeight() / 2);
        action.press(startX, startY).waitAction(2000).moveTo(endX, endY).release().perform();

        //swipe function from  flashfoood to closure
        WebElement element3 = TestBase.getObject("Verify_Flashflood_Text");
        int start_Y = element3.getLocation().getY() + (element3.getSize().getHeight() / 2);
        int start_X = element3.getLocation().getX() + (element3.getSize().getWidth() / 2);
        WebElement element4 = TestBase.getObject("Verify_Closure_Text");
        int end_X = element4.getLocation().getY() + (element4.getSize().getHeight() / 2);
        int end_Y = element4.getLocation().getX() + (element4.getSize().getWidth() / 2);
        action.press(start_X, start_Y).waitAction(2000).moveTo(end_X, end_Y).release().perform();

        TestBase.getObject("Verify_Breakdown_Text").click();
        TestBase.getObject("Others_Roads_Text").click();
        TestBase.getObject("Far_Ahead_Button").click();
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter16"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
       /* Thread.sleep(2000);
        TestBase.getObject("Submit_Button").click();
        Thread.sleep(7000);
        Assert.assertTrue(TestBase.isElementPresent("Successfully_Submitted_The_Report_text"), "C020:Successfully_Submitted_The_Report_text is not present in Report screen page");
        TestBase.getObject("Close_Button").click();*/
        TestBase.getObject("Back_button").click();
    }


    @Test(priority = 21)
    public void C021_Checking_the_Report_Incident_Functionality_For_Roadwork_Using_Opposite_Lane() throws Exception {


        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TouchAction action = new TouchAction(TestBase.driver);
        WebElement element1 = TestBase.getObject("Verify_Closure_Text");
        int startY = element1.getLocation().getY() + (element1.getSize().getHeight() / 2);
        int startX = element1.getLocation().getX() + (element1.getSize().getWidth() / 2);
        WebElement element2 = TestBase.getObject("Verify_Congestion_Text");
        int endX = element2.getLocation().getX() + (element2.getSize().getWidth() / 2);
        int endY = element2.getLocation().getY() + (element2.getSize().getHeight() / 2);
        action.press(startX, startY).waitAction(2000).moveTo(endX, endY).release().perform();

        //swipe function from  flashfoood to closure
        WebElement element3 = TestBase.getObject("Verify_Flashflood_Text");
        int start_Y = element3.getLocation().getY() + (element3.getSize().getHeight() / 2);
        int start_X = element3.getLocation().getX() + (element3.getSize().getWidth() / 2);
        WebElement element4 = TestBase.getObject("Verify_Closure_Text");
        int end_X = element4.getLocation().getY() + (element4.getSize().getHeight() / 2);
        int end_Y = element4.getLocation().getX() + (element4.getSize().getWidth() / 2);
        action.press(start_X, start_Y).waitAction(2000).moveTo(end_X, end_Y).release().perform();

        TestBase.getObject("Verify_Roadwork_Text").click();
        TestBase.getObject("Opposite_Text").click();
        TestBase.getObject("Far_Behind_Button").click();
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter17"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
       /* Thread.sleep(2000);
        TestBase.getObject("Submit_Button").click();
        Thread.sleep(7000);
        Assert.assertTrue(TestBase.isElementPresent("Successfully_Submitted_The_Report_text"), "C021:Successfully_Submitted_The_Report_text is not present in Report screen page");
        TestBase.getObject("Close_Button").click();*/
        TestBase.getObject("Back_button").click();
    }


    @Test(priority = 22)
    public void C022_Checking_the_Report_Incident_Functionality_For_Roadwork_Using_Current_Lane() throws Exception {


        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TouchAction action = new TouchAction(TestBase.driver);
        WebElement element1 = TestBase.getObject("Verify_Closure_Text");
        int startY = element1.getLocation().getY() + (element1.getSize().getHeight() / 2);
        int startX = element1.getLocation().getX() + (element1.getSize().getWidth() / 2);
        WebElement element2 = TestBase.getObject("Verify_Congestion_Text");
        int endX = element2.getLocation().getX() + (element2.getSize().getWidth() / 2);
        int endY = element2.getLocation().getY() + (element2.getSize().getHeight() / 2);
        action.press(startX, startY).waitAction(2000).moveTo(endX, endY).release().perform();

        //swipe function from  flashfoood to closure
        WebElement element3 = TestBase.getObject("Verify_Flashflood_Text");
        int start_Y = element3.getLocation().getY() + (element3.getSize().getHeight() / 2);
        int start_X = element3.getLocation().getX() + (element3.getSize().getWidth() / 2);
        WebElement element4 = TestBase.getObject("Verify_Closure_Text");
        int end_X = element4.getLocation().getY() + (element4.getSize().getHeight() / 2);
        int end_Y = element4.getLocation().getX() + (element4.getSize().getWidth() / 2);
        action.press(start_X, start_Y).waitAction(2000).moveTo(end_X, end_Y).release().perform();
        TestBase.getObject("Verify_Roadwork_Text").click();
        TestBase.getObject("Current_Text").click();
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter18"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
        /*Thread.sleep(2000);
        TestBase.getObject("Submit_Button").click();
        Thread.sleep(7000);
        Assert.assertTrue(TestBase.isElementPresent("Successfully_Submitted_The_Report_text"), "C022:Successfully_Submitted_The_Report_text is not present in Report screen page");
        TestBase.getObject("Close_Button").click();*/
        TestBase.getObject("Back_button").click();
    }

    @Test(priority = 23)
    public void C023_Checking_the_Report_Incident_Functionality_For_Others_Using_Other_Road_Lane() throws Exception {


        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TouchAction action = new TouchAction(TestBase.driver);
        WebElement element1 = TestBase.getObject("Verify_Closure_Text");
        int startY = element1.getLocation().getY() + (element1.getSize().getHeight() / 2);
        int startX = element1.getLocation().getX() + (element1.getSize().getWidth() / 2);
        WebElement element2 = TestBase.getObject("Verify_Congestion_Text");
        int endX = element2.getLocation().getX() + (element2.getSize().getWidth() / 2);
        int endY = element2.getLocation().getY() + (element2.getSize().getHeight() / 2);
        action.press(startX, startY).waitAction(2000).moveTo(endX, endY).release().perform();

        //swipe function from  flashfoood to closure
        WebElement element3 = TestBase.getObject("Verify_Flashflood_Text");
        int start_Y = element3.getLocation().getY() + (element3.getSize().getHeight() / 2);
        int start_X = element3.getLocation().getX() + (element3.getSize().getWidth() / 2);
        WebElement element4 = TestBase.getObject("Verify_Closure_Text");
        int end_X = element4.getLocation().getY() + (element4.getSize().getHeight() / 2);
        int end_Y = element4.getLocation().getX() + (element4.getSize().getWidth() / 2);
        action.press(start_X, start_Y).waitAction(2000).moveTo(end_X, end_Y).release().perform();
        TestBase.getObject("Verify_Others_Text").click();
        TestBase.getObject("Others_Roads_Text").click();
        TestBase.getObject("Far_Behind_Button").click();
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter19"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
       /* Thread.sleep(2000);
        TestBase.getObject("Submit_Button").click();
        Thread.sleep(7000);
        Assert.assertTrue(TestBase.isElementPresent("Successfully_Submitted_The_Report_text"), "C023:Successfully_Submitted_The_Report_text is not present in Report screen page");
        TestBase.getObject("Close_Button").click();
*/ TestBase.getObject("Back_button").click();
    }

    @Test(priority = 24)
    public void C024_Checking_the_Report_Incident_Functionality_For_Others_Using_Opposite_Lane() throws Exception {


        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TouchAction action = new TouchAction(TestBase.driver);
        WebElement element1 = TestBase.getObject("Verify_Closure_Text");
        int startY = element1.getLocation().getY() + (element1.getSize().getHeight() / 2);
        int startX = element1.getLocation().getX() + (element1.getSize().getWidth() / 2);
        WebElement element2 = TestBase.getObject("Verify_Congestion_Text");
        int endX = element2.getLocation().getX() + (element2.getSize().getWidth() / 2);
        int endY = element2.getLocation().getY() + (element2.getSize().getHeight() / 2);
        action.press(startX, startY).waitAction(2000).moveTo(endX, endY).release().perform();

        //swipe function from  flashfoood to closure
        WebElement element3 = TestBase.getObject("Verify_Flashflood_Text");
        int start_Y = element3.getLocation().getY() + (element3.getSize().getHeight() / 2);
        int start_X = element3.getLocation().getX() + (element3.getSize().getWidth() / 2);
        WebElement element4 = TestBase.getObject("Verify_Closure_Text");
        int end_X = element4.getLocation().getY() + (element4.getSize().getHeight() / 2);
        int end_Y = element4.getLocation().getX() + (element4.getSize().getWidth() / 2);
        action.press(start_X, start_Y).waitAction(2000).moveTo(end_X, end_Y).release().perform();
        TestBase.getObject("Verify_Others_Text").click();
        TestBase.getObject("Opposite_Text").click();
        TestBase.getObject("Far_Ahead_Button").click();
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter20"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
       /* Thread.sleep(2000);
        TestBase.getObject("Submit_Button").click();
        Thread.sleep(7000);
        Assert.assertTrue(TestBase.isElementPresent("Successfully_Submitted_The_Report_text"), "C024:Successfully_Submitted_The_Report_text is not present in Report screen page");
        TestBase.getObject("Close_Button").click();*/
        TestBase.getObject("Back_button").click();
    }

    @Test(priority = 25)
    public void C025_Checking_the_Report_Incident_Functionality_For_Others_Using_Current_Lane() throws Exception {


        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Reported_Incidents").click();
        TouchAction action = new TouchAction(TestBase.driver);
        WebElement element1 = TestBase.getObject("Verify_Closure_Text");
        int startY = element1.getLocation().getY() + (element1.getSize().getHeight() / 2);
        int startX = element1.getLocation().getX() + (element1.getSize().getWidth() / 2);
        WebElement element2 = TestBase.getObject("Verify_Congestion_Text");
        int endX = element2.getLocation().getX() + (element2.getSize().getWidth() / 2);
        int endY = element2.getLocation().getY() + (element2.getSize().getHeight() / 2);
        action.press(startX, startY).waitAction(2000).moveTo(endX, endY).release().perform();

        //swipe function from  flashfoood to closure
        WebElement element3 = TestBase.getObject("Verify_Flashflood_Text");
        int start_Y = element3.getLocation().getY() + (element3.getSize().getHeight() / 2);
        int start_X = element3.getLocation().getX() + (element3.getSize().getWidth() / 2);
        WebElement element4 = TestBase.getObject("Verify_Closure_Text");
        int end_X = element4.getLocation().getY() + (element4.getSize().getHeight() / 2);
        int end_Y = element4.getLocation().getX() + (element4.getSize().getWidth() / 2);
        action.press(start_X, start_Y).waitAction(2000).moveTo(end_X, end_Y).release().perform();
        TestBase.getObject("Verify_Others_Text").click();
        TestBase.getObject("Current_Text").click();
        TestBase.driver.scrollTo("Describe the incident here ...");
        TestBase.getObject("Describe_the_incidents_here_text").click();
        TestBase.getObject("Describe_the_incidents_here_text").sendKeys(TestBase.TestData.getProperty("Details_Enter21"));
        TestBase.getObject("Add_Photo_Button").click();
        Thread.sleep(2000);
        TestBase.getObject("Phone_Camera_Button").click();
        TestBase.getObject("Tick_Button").click();
        TestBase.getObject("Back_button").click();
       /* Thread.sleep(2000);
        TestBase.getObject("Submit_Button").click();
        Thread.sleep(7000);
        Assert.assertTrue(TestBase.isElementPresent("Successfully_Submitted_The_Report_text"), "C025:Successfully_Submitted_The_Report_text is not present in Report screen page");
        TestBase.getObject("Close_Button").click();*/

    }

    @AfterTest
    public void quit(){

        if((TestBase.driver)!=null){
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        TestBase.driver.quit();
        System.out.println("exit");
    }
}






