import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by QI 33 on 05-05-2016.
 */
public class COORDINATE_FUNCTION {

    //Checking_Coordinate Button on search screen page
    @Test(priority = 1)

    public void C001_Verify_Coordinate_Button_In_Search_Destination_Page() throws Exception {

                SoftAssert softAssert=new SoftAssert();
                TestBase utill = new TestBase();
                utill.initialize();
                TestBase.getObject("Search_option").click();
                softAssert.assertTrue(TestBase.isElementPresent("Coordinates"), "C001:Coordinates text is not present in the Search_Destination_screen");
                TestBase.get_Screenshot("src\\test\\resource\\COORDINATE_FUNCTION\\C001_Accessing_from_Search_page/" + "Verify Coordinates Button Icon" + ".jpg");
    }

    // Checking the display of Coordinate window
    @Test(priority = 2)

    public void C002_Checking_the_Display_of_Coordinate_Screen() throws Exception {

                SoftAssert softAssert=new SoftAssert();
                TestBase.getObject("Coordinates").click();
                softAssert.assertTrue(TestBase.isElementPresent("Coordinates"), "C002:Coordinates text is not present at top of the header");
                softAssert.assertTrue(TestBase.isElementPresent("Verify_Degrees_Text"), "C002:Degrees_Text is not present in the Coordinates Screen Page");
                softAssert.assertTrue(TestBase.isElementPresent("Verify_Latitude_Text"), "C002:Latitude_Text is not present in the Coordinates Screen Page");
                softAssert.assertTrue(TestBase.isElementPresent("Verify_Longitude_Text"), "C002:Longitude_Text is not present in the Coordinates Screen Page");
                softAssert.assertTrue(TestBase.isElementPresent("OK_button"), "C002:OK_button is not present in the Coordinates Screen Page");
                TestBase.get_Screenshot("src\\test\\resource\\COORDINATE_FUNCTION\\C002_Checking_the_display_of_Coordinate_window/" + " UI_For_Degree " + ".jpg");


                softAssert.assertTrue(TestBase.isElementPresent("Verify_Deg_Min_Sec_Text"), "C002:Degree_Min_Sec text is not present in the Coordinates Screen Page");
                TestBase.getObject("Verify_Deg_Min_Sec_Text").click();
                softAssert.assertTrue(TestBase.isElementPresent("Verify_Latitude_Text"), "C007:Coordinates text is not present in the Coordinates Screen Page");
                softAssert.assertTrue(TestBase.isElementPresent("Verify_Longitude_Text"), "C008:Coordinates text is not present in the Coordinates Screen Page");
                softAssert.assertTrue(TestBase.isElementPresent("OK_button"), "C002:OK_button is not present in the Coordinates Screen Page");
                TestBase.get_Screenshot("src\\test\\resource\\COORDINATE_FUNCTION\\C002_Checking_the_display_of_Coordinate_window/" + " UI_For_Degree_Min_Sec " + ".jpg");
                TestBase.getObject("Back_button").click();
                softAssert.assertAll();
    }

    @Test(priority = 3)

    public void C003_Checking_The_Validation_PopUp_Message_For_Coordinates_Degree_Tab_When_No_Data_Is_Entered() throws Exception {

                SoftAssert softAssert=new SoftAssert();
                TestBase.getObject("Coordinates").click();
                TestBase.getObject("Latitude").click();
                TestBase.getObject("Latitude").sendKeys(TestBase.TestData.getProperty(" "));
                TestBase.getObject("Longitude").click();
                TestBase.getObject("Longitude").sendKeys(TestBase.TestData.getProperty(" "));
                TestBase.getObject("Ok_Button").click();
                TestBase.get_Screenshot("src\\test\\resource\\COORDINATE_FUNCTION\\C005_Checking_The_Validation_Message_For_Coordinate/" + "Pop up box with invalid input from Degree" + ".jpg");
                softAssert.assertTrue(TestBase.isElementPresent("Verify_Invalid_Input_Text_Degree"), "C001:Invalid_Input_Text text is not present in the Degrees_Pop up box");
                softAssert.assertTrue(TestBase.isElementPresent("Verify_Please_Entry_Valid_Coordinates_Text_Degree"), "C002:_Valid_Coordinates_Text text is not present in the Degrees_Pop up box");
                softAssert.assertTrue(TestBase.isElementPresent("Ok_Button"), "C003: Ok text is not present in the Coordinates Screen Page");
                TestBase.getObject("Ok_Button").click();
                softAssert.assertAll();
    }


        @Test(priority = 4)

        public void C004_Checking_The_Validation_PopUp_Message_For_Coordinates_Degree_Tab_When_One_Data_Is_Entered() throws Exception {

                SoftAssert softAssert=new SoftAssert();
                TestBase.getObject("Latitude").click();
                TestBase.getObject("Latitude").sendKeys(TestBase.TestData.getProperty("Latitude2"));
                TestBase.getObject("Longitude").click();
                TestBase.getObject("Longitude").sendKeys(TestBase.TestData.getProperty(""));
                TestBase.getObject("Ok_Button").click();
                TestBase.get_Screenshot("src\\test\\resource\\COORDINATE_FUNCTION\\C004_Checking_The_Validation_PopUp_Message_For_Coordinates_Degree_Tab_When_One_Data_Is_Entered/" + "Pop up box with invalid input from Degree" + ".jpg");
                softAssert.assertTrue(TestBase.isElementPresent("Verify_Invalid_Input_Text_Degree"), "C004:Invalid_Input_Text text is not present in the Degrees_Pop up box");
                softAssert.assertTrue(TestBase.isElementPresent("Verify_Please_Entry_Valid_Coordinates_Text_Degree"), "C004:_Valid_Coordinates_Text text is not present in the Degrees_Pop up box");
                softAssert.assertTrue(TestBase.isElementPresent("Ok_Button"), "C004:Ok text is not present in the Deg_Min_Sec_Pop up box ");
                TestBase.getObject("Ok_Button").click();
                TestBase.getObject("Back_button").click();
                softAssert.assertAll();
        }


            @Test(priority = 5)

            public void C005_Checking_Checking_the_Functionality_For_DEGREE_Tab() throws Exception {
                TestBase.getObject("Coordinates").click();
                TestBase.getObject("Latitude").clear();
                TestBase.getObject("Latitude").click();
                TestBase.getObject("Latitude").sendKeys(TestBase.TestData.getProperty("Latitude3"));
                TestBase.getObject("Longitude").clear();
                TestBase.getObject("Longitude").sendKeys(TestBase.TestData.getProperty("Longitude3"));
                TestBase.getObject("Ok_Button").click();
                Thread.sleep(2000);
                TestBase.get_Screenshot("src\\test\\resource\\COORDINATE_FUNCTION\\C003_Checking_the_Functionality_For_DEGREES_Tab/" + "Map view screen with coordinates2" + ".jpg");
                TestBase.getObject("Back_button").click();
                Thread.sleep(2000);
                TestBase.get_Screenshot("src\\test\\resource\\COORDINATE_FUNCTION\\C005_Checking_Checking_the_Functionality_For_DEGREE_Tab/" + "Checking Degree_screen with old data " + ".jpg");

    }


    @Test(priority = 6)

    public void C006_Checking_The_Validation_PopUp_Message_For_Coordinates_DEG_MIN_SEC_Tab_When_No_Data_Is_Entered() throws Exception{

                SoftAssert softAssert=new SoftAssert();

                TestBase.getObject("Verify_Deg_Min_Sec_Text").click();
                TestBase.getObject("Deg_Latitude_Edit_Field").click();
                TestBase.getObject("Deg_Latitude_Edit_Field").sendKeys(TestBase.TestData.getProperty("Deg_Latitude_1"));
                TestBase.getObject("Min_Latitude_Edit_Field").click();
                TestBase.getObject("Min_Latitude_Edit_Field").sendKeys(TestBase.TestData.getProperty("Min_Latitude_1"));
                TestBase.getObject("Sec_Latitude_Edit_Field").click();
                TestBase.getObject("Sec_Latitude_Edit_Field").sendKeys(TestBase.TestData.getProperty("Sec_Latitude_1"));
                TestBase.getObject("Deg_Longitude_Edit_Field").click();
                TestBase.getObject("Deg_Longitude_Edit_Field").sendKeys(TestBase.TestData.getProperty(""));
                TestBase.getObject("Min_Longitude_Edit_Field").click();
                TestBase.getObject("Min_Longitude_Edit_Field").sendKeys(TestBase.TestData.getProperty("Min_Longitude_1"));
                TestBase.getObject("Sec_Longitude_Edit_Field").click();
                TestBase.getObject("Sec_Longitude_Edit_Field").sendKeys(TestBase.TestData.getProperty("Sec_Longitude_1"));
                TestBase.getObject("Ok_Button").click();
                TestBase.get_Screenshot("src\\test\\resource\\COORDINATE_FUNCTION\\C006_Checking_The_Validation_PopUp_Message_For_Coordinates_DEG_MIN_SEC_Tab_When_No_Data_Is_Entered/" + "Pop up box with invalid input from Deg_Min_Sec" + ".jpg");
                softAssert.assertTrue(TestBase.isElementPresent("Verify_Invalid_Input_Text_Deg_Min_Sec"), "C002:Invalid_Input_Text text is not present in the Deg_Min_Sec_Pop up box ");
                softAssert.assertTrue(TestBase.isElementPresent("Verify_Please_Entry_Valid_Coordinates_Text_Deg_Min_Sec"), "C002:_Valid_Coordinates_Text text is not present in the Deg_Min_Sec_Pop up box ");
                softAssert.assertTrue(TestBase.isElementPresent("Ok_Button"), "C006:Ok text is not present in the Deg_Min_Sec_Pop up box ");
                TestBase.getObject("Ok_Button").click();
                softAssert.assertAll();
    }




    @Test(priority = 7)

    public void C007_Checking_the_Functionality_For_DEG_MIN_SEC_Tab() throws Exception {

                SoftAssert softAssert=new SoftAssert();
                TestBase.getObject("Deg_Latitude_Edit_Field").click();
                TestBase.getObject("Deg_Latitude_Edit_Field").sendKeys(TestBase.TestData.getProperty("Deg_Latitude_1"));
                TestBase.getObject("Min_Latitude_Edit_Field").click();
                TestBase.getObject("Min_Latitude_Edit_Field").sendKeys(TestBase.TestData.getProperty("Min_Latitude_1"));
                TestBase.getObject("Sec_Latitude_Edit_Field").click();
                TestBase.getObject("Sec_Latitude_Edit_Field").sendKeys(TestBase.TestData.getProperty("Sec_Latitude_1"));
                TestBase.getObject("Deg_Longitude_Edit_Field").click();
                TestBase.getObject("Deg_Longitude_Edit_Field").sendKeys(TestBase.TestData.getProperty("Deg_Longitude_1"));
                TestBase.getObject("Min_Longitude_Edit_Field").click();
                TestBase.getObject("Min_Longitude_Edit_Field").sendKeys(TestBase.TestData.getProperty("Min_Longitude_1"));
                TestBase.getObject("Sec_Longitude_Edit_Field").click();
                TestBase.getObject("Sec_Longitude_Edit_Field").sendKeys(TestBase.TestData.getProperty("Sec_Longitude_1"));
                TestBase.getObject("Ok_Button").click();
                Thread.sleep(2000);
                TestBase.get_Screenshot("src\\test\\resource\\COORDINATE_FUNCTION\\C006_Checking_The_Validation_PopUp_Message_For_Coordinates_DEG_MIN_SEC_Tab_When_No_Data_Is_Entered/" + "Map view screen with coordinates" + ".jpg");
                TestBase.getObject("Back_button").click();
                softAssert.assertAll();
    }

    @Test(priority = 8)

    public void C008_Checking_the_Functionality_For_DEG_MIN_SEC_Tab_When_One_Data_Is_Entered() throws Exception {

                SoftAssert softAssert=new SoftAssert();
                TestBase.getObject("Deg_Latitude_Edit_Field").click();
                TestBase.getObject("Deg_Latitude_Edit_Field").clear();
                TestBase.getObject("Ok_Button").click();
                softAssert.assertTrue(TestBase.isElementPresent("Verify_Invalid_Input_Text_Deg_Min_Sec"), "C008:Invalid_Input_Text text is not present in the Deg_Min_Sec_Pop up box ");
                softAssert.assertTrue(TestBase.isElementPresent("Verify_Please_Entry_Valid_Coordinates_Text_Deg_Min_Sec"), "C008:_Valid_Coordinates_Text text is not present in the Deg_Min_Sec_Pop up box ");
                softAssert.assertTrue(TestBase.isElementPresent("Ok_Button"), "C008:Ok text is not present in the Deg_Min_Sec_Pop up box ");
                TestBase.getObject("Ok_Button").click();
                softAssert.assertAll();
    }

    @Test(priority = 9)

    public void C009_Verify_the_toast_message_displayed_for_Inappropriate_data_entered_in_the_text_field() throws Exception {

        TestBase.getObject("Deg_Latitude_Edit_Field").click();
        TestBase.getObject("Deg_Latitude_Edit_Field").sendKeys(TestBase.TestData.getProperty("Deg_Latitude_2"));
        TestBase.getObject("Min_Latitude_Edit_Field").click();
        TestBase.getObject("Min_Latitude_Edit_Field").sendKeys(TestBase.TestData.getProperty("Min_Latitude_2"));
        TestBase.getObject("Sec_Latitude_Edit_Field").click();
        TestBase.getObject("Sec_Latitude_Edit_Field").sendKeys(TestBase.TestData.getProperty("Sec_Latitude_2"));
        TestBase.getObject("Deg_Longitude_Edit_Field").click();
        TestBase.getObject("Deg_Longitude_Edit_Field").sendKeys(TestBase.TestData.getProperty("Deg_Longitude_2"));
        TestBase.getObject("Min_Longitude_Edit_Field").click();
        TestBase.getObject("Min_Longitude_Edit_Field").sendKeys(TestBase.TestData.getProperty("Min_Longitude_2"));
        TestBase.getObject("Sec_Longitude_Edit_Field").click();
        TestBase.getObject("Sec_Longitude_Edit_Field").sendKeys(TestBase.TestData.getProperty("Sec_Longitude_2"));
        TestBase.getObject("Ok_Button").click();
        Thread.sleep(2000);
        TestBase.get_Screenshot("src\\test\\resource\\COORDINATE_FUNCTION\\C006_Checking_The_Validation_PopUp_Message_For_Coordinates_DEG_MIN_SEC_Tab_When_No_Data_Is_Entered/" + "Map view screen with inappropriate message" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        TestBase.clear_data();
    }

    @AfterTest
    public void quit(){

        if((TestBase.driver)!=null){
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        TestBase.driver.quit();
        System.out.println("exit");
    }

}