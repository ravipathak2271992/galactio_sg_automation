//import Negative_Test_Cases.*;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import sun.awt.windows.ThemeReader;

//import static Negative_Test_Cases.TestBase.driver;

/**
 * Created by Soumik Mazumder on 9/14/2016.
 */
public class DEMO {

    @Test(priority = 1)
    public void GLOBAL_SEARCH() throws Exception {
        SoftAssert softAssert = new SoftAssert();

        //Start the Application
        TestBase utill = new TestBase();
        utill.initialize();

        TestBase.getObject("Search_option").click();

        //Click on search destination field and entering location1
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Search_Proper_Sequence_Location_1"));

        TestBase.getObject("Search_button").click();
        Thread.sleep(2000);


        //Take screenshot for location_1 result
        TestBase.get_Screenshot("src\\test\\resource\\DEMO" + "location_1_Result" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Clear_Data_button").click();

        //Go to OUT OF SEQUENCE Search
        //Click on search destination field and check for Address result with screenshot

        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Search_OutOf_Sequence_Location_1"));
        Thread.sleep(2000);
        TestBase.getObject("Search_button").click();
        Thread.sleep(2000);

        //Take screenshot for out of sequence result
        TestBase.get_Screenshot("src\\test\\resource\\DEMO" + "Out_Of_Sequence_Search_result" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Clear_Data_button").click();


        //Postal code
        TestBase.getObject("Search_destination_edit_field").click();
        TestBase.getObject("Search_destination_edit_field").sendKeys(TestBase.TestData.getProperty("Postal_Code_Data"));
        Thread.sleep(1000);
        TestBase.getObject("Search_button").click();
        Thread.sleep(2000);

        //Take screenshot for out of sequence result
        TestBase.get_Screenshot("src\\test\\resource\\DEMO" + "Postal_Code_result" + ".jpg");
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Clear_Data_button").click();
        Thread.sleep(1000);
        TestBase.getObject("Back_button").click();
        TestBase.getObject("Back_button").click();
        Thread.sleep(1000);

        softAssert.assertAll();
    }

    @Test(priority = 2)
    public void FAVOURITE() throws Exception {
        SoftAssert softAssert = new SoftAssert();

        //Open any Location in Map View and Select Favourite for Recent & Favourite Page
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("POI_Options").click();
        TestBase.getObject("Emergency").click();
        TestBase.getObject("All_Emergency").click();
        Thread.sleep(3000);
        TestBase.getObject("Location_Name").click();
        Thread.sleep(2000);
        //Select favourite
        TestBase.getObject("Favourite_Star_Button").click();
        TestBase.getObject("Ok_Button").click();
        Thread.sleep(2000);

        //  Creating Loop to back from the screen
        for (int i = 0; i < 5; i++) {
            TestBase.getObject("Back_Button").click();
        }
        Thread.sleep(1000);


        //Click on Search > "Favourite" Options
        TestBase.getObject("Search_Button").click();
        TestBase.getObject("Favourite_Options").click();
        softAssert.assertTrue(TestBase.isElementPresent("Location_Name"), "Favourite is not added successfully");
        TestBase.get_Screenshot("src\\test\\resource\\DEMO" + "Favourite_List" + ".jpg");
        Thread.sleep(1000);
        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();

        softAssert.assertAll();
    }

    @Test(priority = 3)
    public void RECENT() throws Exception {
        SoftAssert softAssert = new SoftAssert();

        TestBase.getObject("Search_Button").click();
        TestBase.getObject("Recent_Options").click();
        softAssert.assertTrue(TestBase.isElementPresent("Location_Name"), "Recent is not added successfully");
        TestBase.get_Screenshot("src\\test\\resource\\DEMO" + "Recent_List" + ".jpg");

        TestBase.getObject("Back_Button").click();
        TestBase.getObject("Back_Button").click();
        softAssert.assertAll();

    }

    @Test(priority = 4)
    public void CLEAR_DATA_FUNCTION() throws Exception {
        SoftAssert softAssert = new SoftAssert();
        //Clear all the data Functionality Flow:
        TestBase.clear_data();
        TestBase.getObject("Back_Button").click();
        Thread.sleep(1000);
        softAssert.assertAll();

    }

    @Test(priority = 5)
    public void EXIT_FUNCTION() throws Exception {
        SoftAssert softAssert = new SoftAssert();

        TestBase.driver.navigate().back();
        Thread.sleep(2000);

        //Tap on YES button to close the application
        TestBase.getObject("Yes_Button").click();

        softAssert.assertAll();

    }


    @AfterTest
    public void quit(){

        if((TestBase.driver)!=null){
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        TestBase.driver.quit();
        System.out.println("exit");
    }

    }
