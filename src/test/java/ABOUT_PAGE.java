import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidKeyCode;
import org.openqa.selenium.interactions.touch.LongPressAction;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static Negative_Test_Cases.TestBase.TestData;

/**
 * Created by Soumik on 5/17/2016.
 */
public class ABOUT_PAGE {
    @Test(priority = 1)
    public void C001_Verify_the_About_Screen_from_Options_Page() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();
        Thread.sleep(5000);

        // Click on Option Button
        TestBase.getObject("Option_Button").click();
        //Scroll to About button
        TestBase.driver.scrollTo("About");
        //Click on About button
        TestBase.getObject("Verify_About_Button").click();
        Thread.sleep(2000);

        //  Screenshot for About screen
        TestBase.get_Screenshot("src\\test\\resource\\ABOUT_PAGE\\C001_Verify_the_About_Screen_from_Options_Page/" + "About_Screen" + ".jpg");
    }

    @Test(priority = 2)
    public void C002_Verify_the_About_Screen_UI_and_Verification() throws Exception {

        //Verify the all options present or not
        Assert.assertTrue(TestBase.isElementPresent("Verify_Rate_and_Review_Text"), "C002:Rate_and_Review_Text is not present in About screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Welcome_Message_Text"), "C002:Welcome_Message_Text is not present in About screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Contact_Support_Text_1"), "C002:Terms_and_Conditions_Text is not present in About screen");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Contact_Support_Text_2"), "C002:Contact_Support_Text is not present in About screen");
        TestBase.driver.scrollTo("Share App");
        Assert.assertTrue(TestBase.isElementPresent("Verify_Share_App_Text"), "C002:Share_App_Text is not present in About screen");

        //Take a screen shots for UI verification
        TestBase.get_Screenshot("src\\test\\resource\\ABOUT_PAGE\\C002_Verify_the_About_Screen_UI_and_Verification/" + "About_Screen_UI" + ".jpg");
    }

    @Test(priority = 3)
    public void C003_Verify_Rate_Review_Options() throws Exception {
        TestBase.getObject("Verify_Rate_and_Review_Text").click();

        //Click on Play Store Options for checking
       // TestBase.getObject("Play_Store_Options").click();

        Thread.sleep(5000);

        //Verify the PlayStore Screen
        TestBase.get_Screenshot("src\\test\\resource\\ABOUT_PAGE\\C003_Verify_Rate_Review_Options/" + "PlayStore_Screen_Rate&Review" + ".jpg");

        //Click on Phone back button
        TestBase.driver.pressKeyCode(AndroidKeyCode.BACK);
    }

    @Test(priority = 4)
    public void C004_Verify_Welcome_Message() throws Exception {

        TestBase.getObject("Verify_Welcome_Message_Text").click();
        Thread.sleep(1000);

        TestBase.get_Screenshot("src\\test\\resource\\ABOUT_PAGE\\C004_Verify_Welcome_Message/" + "Welcome1" + ".jpg");
        Thread.sleep(9000);

        TestBase.get_Screenshot("src\\test\\resource\\ABOUT_PAGE\\C004_Verify_Welcome_Message/" + "Welcome2" + ".jpg");
        Thread.sleep(9000);

        TestBase.get_Screenshot("src\\test\\resource\\ABOUT_PAGE\\C004_Verify_Welcome_Message/" + "Welcome3" + ".jpg");
        Thread.sleep(9000);

        TestBase.get_Screenshot("src\\test\\resource\\ABOUT_PAGE\\C004_Verify_Welcome_Message/" + "Welcome4" + ".jpg");
        Thread.sleep(9000);

        TestBase.get_Screenshot("src\\test\\resource\\ABOUT_PAGE\\C004_Verify_Welcome_Message/" + "Welcome5" + ".jpg");
        Thread.sleep(9000);

        TestBase.get_Screenshot("src\\test\\resource\\ABOUT_PAGE\\C004_Verify_Welcome_Message/" + "Welcome6" + ".jpg");
        Thread.sleep(9000);

        TestBase.get_Screenshot("src\\test\\resource\\ABOUT_PAGE\\C004_Verify_Welcome_Message/" + "Welcome7" + ".jpg");
        Thread.sleep(9000);

        TestBase.get_Screenshot("src\\test\\resource\\ABOUT_PAGE\\C004_Verify_Welcome_Message/" + "Welcome8" + ".jpg");
        Thread.sleep(9000);

        TestBase.get_Screenshot("src\\test\\resource\\ABOUT_PAGE\\C004_Verify_Welcome_Message/" + "Welcome9" + ".jpg");
        Thread.sleep(9000);

        //Click on back button
        TestBase.driver.pressKeyCode(AndroidKeyCode.BACK);
    }

    @Test(priority = 5)
    public void C005_Verify_the_Terms_and_Conditions_Options() throws Exception {
        //Click on Terms and Condition
        TestBase.getObject("Verify_Terms_and_Conditions_Text").click();
        Thread.sleep(9000);

        TestBase.get_Screenshot("src\\test\\resource\\ABOUT_PAGE\\C005_Verify_the_Terms_and_Conditions_Options/" + "Terms&Condition_Page" + ".jpg");

        //Click on Phone back button
        TestBase.driver.navigate().back();
    }

    @Test(priority = 6)
    public void C006_Verify_the_Contact_Support_Options() throws Exception {

        TestBase.getObject("Verify_Contact_Support_Text_1").click();
        Thread.sleep(2000);

        //Select Email option

       // TestBase.getObject("Email_Option").click();
        TestBase.driver.navigate().back();
        //TestBase.driver.navigate().back();

        Thread.sleep(1000);

        TestBase.get_Screenshot("src\\test\\resource\\ABOUT_PAGE\\C006_Verify_the_Contact_Support_Options/" + "Contact&Support_Email_Page" + ".jpg");
        Thread.sleep(1000);
        TestBase.driver.navigate().back();
    }

    @Test(priority = 7)
    public void C007_Verify_The_Scroll_Functionality_in_About_Page() throws Exception {

        TestBase.driver.scrollTo("Share App");
    }

    @Test(priority = 8)
    public void C008_Verify_the_ShareApp_Option() throws Exception {

        TestBase.getObject("Verify_Share_App_Text").click();
        Thread.sleep(2000);

        //Verify the share App page
        TestBase.get_Screenshot("src\\test\\resource\\ABOUT_PAGE\\C008_Verify_the_ShareApp_Option/" + "Share_APP_PAGE" + ".jpg");

    }

    @Test(priority = 9)
    public void C009_Verify_the_Referral_Options() throws Exception {

        // Verify the Referrals feature
        Assert.assertTrue(TestBase.isElementPresent("Referral_Button"), "C009:Referral_Button is not present in Share App screen");

        //Click on back button
        TestBase.getObject("Back_button").click();
    }

    @Test(priority = 10)
    public void C010_Check_the_Licence_Page_verification () throws Exception {
        TestBase.getObject("Open_Source_License").click();

        TestBase.get_Screenshot("src\\test\\resource\\ABOUT_PAGE\\C010_Check_the_Licence_Page_verification/" + "Open_Source_License_Page" + ".jpg");
        TestBase.getObject("Back_button").click();
        //TestBase.getObject("Back_button").click();


    }


    @Test(priority = 11)
    public void C011_Verify_for_the_content_of_the_mail_for_Contact_Support() throws Exception {



        SoftAssert softAssert = new SoftAssert();

        String SW_Version_Verification = TestBase.getObject("Galactio_SW_Version").getText();
        String Map_Version_Verification = TestBase.getObject("Galactio_MAP_Version").getText();
        System.out.println(SW_Version_Verification + Map_Version_Verification);

        TestBase.getObject("Verify_Contact_Support_Text_1").click();
        TestBase.driver.pressKeyCode(AndroidKeyCode.BACK);

        if (TestBase.getObject("Email_Content_Box").getText().contains(SW_Version_Verification) && TestBase.getObject("Email_Content_Box").getText().contains(Map_Version_Verification)) {

            System.out.println("SW vesrion and map vesrion is displying in email body");


        } else {
            softAssert.assertTrue(1 > 2, "SW vesrion and map vesrion is not displying in email body");

        }
        TestBase.driver.pressKeyCode(AndroidKeyCode.BACK);
        softAssert.assertAll();

    }

        @Test(priority = 12)
        public void C012_Verify_for_the_copy_link_feature_of_Share_App() throws Exception {

            TestBase.getObject("Verify_Share_App_Text").click();
            TestBase.getObject("Copy_Link").click();
            Thread.sleep(1000);
            TestBase.get_Screenshot("src\\test\\resource\\ABOUT_PAGE\\C012_Verify_for_the_content_of_the_mail_for_Contact_Support/" + "Copy_link toast message" + ".jpg");

            TestBase.getObject("More_Options").click();
            TestBase.getObject("Messages_Text").click();
            TestBase.getObject("Message_Content_box").click();
            TestBase.get_Screenshot("src\\test\\resource\\ABOUT_PAGE\\C012_Verify_for_the_content_of_the_mail_for_Contact_Support/" + "Link in massage body" + ".jpg");

            TestBase.driver.pressKeyCode(AndroidKeyCode.BACK);
            TestBase.driver.pressKeyCode(AndroidKeyCode.BACK);
            TestBase.getObject("OK_button").click();

        }

    @Test(priority = 13)
    public void C013_Verify_The_Share_App_Feature_from_Facebook() throws Exception {

        TestBase.getObject("Facebook_Text").click();
        Thread.sleep(3000);
        TestBase.driver.pressKeyCode(AndroidKeyCode.BACK);
        TestBase.get_Screenshot("src\\test\\resource\\ABOUT_PAGE\\C013_Verify_The_Share_App_Feature_from_Facebook/" + "Share To facebook screen1" + ".jpg");
        TestBase.driver.pressKeyCode(AndroidKeyCode.BACK);
        TestBase.getObject("Facebook_Discard_button").click();

        }

    @Test(priority = 14)
    public void C014_Verify_that_user_is_able_to_Share_app_through_Email_option() throws Exception {

        TestBase utill = new TestBase();
        utill.initialize();
        TestBase.getObject("Email_Text").click();
        TestBase.getObject("Gmail_Text").click();
        Thread.sleep(2000);
        TestBase.driver.pressKeyCode(AndroidKeyCode.BACK);
        if(TestBase.getObject("mail_body").getText().contains("Hey,\n" +
                "\n" +
                "I'm driving with Galactio GPS Navigation App everyday." )){
            System.out.println("passed");
            TestBase.getObject("Mail_To_Send_Edit_field_Intex_Device").sendKeys(TestBase.TestData.getProperty("Mail_to_send_email_id"));
            TestBase.getObject("Mail_Id_Send_button_Intex_Device").click();}

        else{
            Assert.assertTrue(1>2, "mail body shows wrong content");
        }
    }



    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }
}
