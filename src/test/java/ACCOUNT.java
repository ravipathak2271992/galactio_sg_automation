import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.sql.Time;

/**
 * Created by QI-37 on 07-06-2016.
 */
public class ACCOUNT {

    SoftAssert softAssert= new SoftAssert();

    @Test(priority = 1)
    public void C001_Verify_the_Account_Label_from_Options_Menu_when_not_loggedIn() throws Exception {
        TestBase util = new TestBase();
        try {
            util.initialize();
        } catch (Exception e) {
            e.printStackTrace();
        }

        TestBase.getObject("Option_Button").click();

        softAssert.assertTrue(TestBase.isElementPresent("account_text1"), "Account label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("account_text2"), "Log In / Register is not there");
        TestBase.get_Screenshot("src\\test\\resource\\account\\C001_Verify_the_Account_Label_from_Options_Menu_when_not_loggedIn/" + "screen1" + ".jpg");
        softAssert.assertAll();


    }

    @Test(priority = 2)
    public void C002_Verify_the_Account_Log_In_Page_UI() throws Exception {

        TestBase.getObject("Verify_Account_Button").click();
        softAssert.assertTrue(TestBase.isElementPresent("account_text1"), "Account label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("log_in_to_your_mapSYNQ_acct"), "log_in_to_your_mapSYNQ_acct label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("email_label"), "email_label label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("pwd_field"), "pwd_field label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("log_in_btn"), "log_in_btn label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("fyp"), "forget your pwd label is not there");
        TestBase.driver.navigate().back();
        Assert.assertTrue(TestBase.isElementPresent("new_to_galactio"), "new to galactio? label is not there");

        Assert.assertTrue(TestBase.isElementPresent("create_new_acct"), "create new acct label is not there");
        //TestBase.driver.navigate().back();
        TestBase.get_Screenshot("src\\test\\resource\\account\\C002_Verify_the_Account_Log_In_Page_UI/" + "screen1" + ".jpg");
        softAssert.assertAll();
    }

    @Test(priority = 3)
    public void C003_Verify_the_Forgot_Password_link_in_Account_Log_In_Page() throws Exception {
        softAssert.assertTrue(TestBase.isElementPresent("fyp"), "forget your pwd label is not there");
        TestBase.get_Screenshot("src\\test\\resource\\account\\C003_Verify_the_Forgot_Password_link_in_Account_Log_In_Page/" + "screen1" + ".jpg");
        softAssert.assertAll();

    }

    @Test(priority = 4)
    public void C004_Verify_the_UI_messages_in_dialogue_box_after_clicking_on_Forgot_Password_link() throws Exception {

        TestBase.getObject("fyp").click();

        softAssert.assertTrue(TestBase.isElementPresent("reset_pwd_text"), "reset_pwd_text label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("reset_pwd_msg"), "reset_pwd_msg label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("ok_btn"), "ok_btn is not there");
        TestBase.get_Screenshot("src\\test\\resource\\account\\C004_Verify_the_UI_messages_in_dialogue_box_after_clicking_on_Forgot_Password_link/" + "screen1" + ".jpg");
        softAssert.assertAll();

    }

    @Test(priority = 5)
    public void C005_Verify_the_Account_Log_In_Page_is_displayed_after_we_click_Ok_Forgot_Password_link_dialogue_box() throws Exception {

        TestBase.getObject("ok_btn").click();

        softAssert.assertTrue(TestBase.isElementPresent("account_text1"), "Account label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("log_in_to_your_mapSYNQ_acct"), "log_in_to_your_mapSYNQ_acct label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("email_label"), "email_label label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("pwd_field"), "pwd_field label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("log_in_btn"), "log_in_btn label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("fyp"), "forget your pwd label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("new_to_galactio"), "new to galactio? label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("create_new_acct"), "create new acct label is not there");

        TestBase.get_Screenshot("src\\test\\resource\\account\\C005_Verify_the_Account_Log_In_Page_is_displayed_after_we_click_Ok_Forgot_Password_link_dialogue_box/" + "screen1" + ".jpg");
        softAssert.assertAll();

    }

    @Test(priority = 6)
    public void C006_Verify_the_Mapsynq_navigation_when_clicked_on_link_from_Forgot_Password_link_dialogue_box() throws Exception {

        TestBase.getObject("fyp").click();
        TestBase.getObject("mapsynq_link").click();
        try {
            TestBase.getObject("chrome").click();
        }catch(Exception e){
            e.getStackTrace();
        }
        softAssert.assertTrue(TestBase.isElementPresent("mapsynq_url"), "mapsynq_url is not there");
        Thread.sleep(4000);
        TestBase.get_Screenshot("src\\test\\resource\\account\\C006_Verify_the_Mapsynq_navigation_when_clicked_on_link_from_Forgot_Password_link_dialogue_box/" + "screen1" + ".jpg");
        softAssert.assertAll();

    }

    @Test(priority = 7)
    public void C007_Click_on_the_system_back_button_from_Mapsynq_verify_the_Forgot_Password_dialogue_box_is_displayed() throws Exception {

        TestBase.driver.navigate().back();

        Thread.sleep(4000);
        TestBase.get_Screenshot("src\\test\\resource\\account\\C007_Click_on_the_system_back_button_from_Mapsynq_verify_the_Forgot_Password_dialogue_box_is_displayed/" + "screen1" + ".jpg");

        TestBase.getObject("ok_btn").click();
        softAssert.assertAll();

    }

    @Test(priority = 8)
    public void C008_Click_on_CREATE_NEW_ACCOUNT_link_and_Verify_the_CREATE_NEW_ACCOUNT_page_UI() throws Exception {

        TestBase.getObject("create_new_acct").click();

        softAssert.assertTrue(TestBase.isElementPresent("create_new_acct_page_header"), "create_new_acct_page_header is not there");
        softAssert.assertTrue(TestBase.isElementPresent("mapsynq_setup_msg"), "Let's setup your mapSYNQ account is not there");
        softAssert.assertTrue(TestBase.isElementPresent("autofill_wid_fb"), "autofill_wid_fb is not there");
        softAssert.assertTrue(TestBase.isElementPresent("cna_first_name"), "cna_first_name is not there");
        softAssert.assertTrue(TestBase.isElementPresent("cna_last_name"), "cna_last_name is not there");
        softAssert.assertTrue(TestBase.isElementPresent("cna_email"), "cna_email is not there");
        softAssert.assertTrue(TestBase.isElementPresent("cna_pwd"), "cna_pwd is not there");
        softAssert.assertTrue(TestBase.isElementPresent("cna_dob"), "cna_dob is not there");
        softAssert.assertTrue(TestBase.isElementPresent("cna_male"), "cna_male is not there");

        softAssert.assertTrue(TestBase.isElementPresent("cna_female"), "cna_female is not there");
        softAssert.assertTrue(TestBase.isElementPresent("cna_signup"), "cna_signup is not there");
        TestBase.driver.scrollTo("Terms and Conditions");
        softAssert.assertTrue(TestBase.isElementPresent("cna_footer1"), "cna_footer1 is not there");
        softAssert.assertTrue(TestBase.isElementPresent("cna_footer2"), "cna_footer2 is not there");
        softAssert.assertTrue(TestBase.isElementPresent("cna_tnc"), "cna_tnc is not there");


        TestBase.get_Screenshot("src\\test\\resource\\account\\C008_Click_on_CREATE_NEW_ACCOUNT_link_and_Verify_the_CREATE_NEW_ACCOUNT_page_UI/" + "screen1" + ".jpg");
        softAssert.assertAll();

    }

    @Test(priority = 9)
    public void C009_Verify_the_TermsAndConditions_page_from_CREATE_NEW_ACCOUNT_page() throws Exception {
        TestBase.getObject("cna_tnc").click();
        try {
            TestBase.getObject("chrome").click();
        }catch(Exception e){
            e.getMessage();
        }
        softAssert.assertTrue(TestBase.isElementPresent("tnc_url"), "tnc_url is not there");

        Thread.sleep(3000);
        TestBase.get_Screenshot("src\\test\\resource\\account\\C009_Verify_the_TermsAndConditions_page_from_CREATE_NEW_ACCOUNT_page/" + "screen1" + ".jpg");
        softAssert.assertAll();

    }

    @Test(priority = 10)
    public void C010_Verify_after_clicking_on_system_back_button_from_TermsAndConditions_user_navigates_CREATE_NEW_ACCOUNT_page() throws Exception {

        TestBase.driver.navigate().back();
        TestBase.driver.scrollTo("Let's setup your mapSYNQ account");

        softAssert.assertTrue(TestBase.isElementPresent("create_new_acct_page_header"), "create_new_acct_page_header is not there");
        softAssert.assertTrue(TestBase.isElementPresent("mapsynq_setup_msg"), "Let's setup your mapSYNQ account is not there");
        softAssert.assertTrue(TestBase.isElementPresent("autofill_wid_fb"), "autofill_wid_fb is not there");
        softAssert.assertTrue(TestBase.isElementPresent("cna_first_name"), "cna_first_name is not there");
        softAssert.assertTrue(TestBase.isElementPresent("cna_last_name"), "cna_last_name is not there");
        softAssert.assertTrue(TestBase.isElementPresent("cna_email"), "cna_email is not there");
        softAssert.assertTrue(TestBase.isElementPresent("cna_pwd"), "cna_pwd is not there");
        softAssert.assertTrue(TestBase.isElementPresent("cna_dob"), "cna_dob is not there");
        softAssert.assertTrue(TestBase.isElementPresent("cna_male"), "cna_male is not there");

        softAssert.assertTrue(TestBase.isElementPresent("cna_female"), "cna_female is not there");
        softAssert.assertTrue(TestBase.isElementPresent("cna_signup"), "cna_signup is not there");
        TestBase.driver.scrollTo("Terms and Conditions");
        softAssert.assertTrue(TestBase.isElementPresent("cna_footer1"), "cna_footer1 is not there");
        softAssert.assertTrue(TestBase.isElementPresent("cna_footer2"), "cna_footer2 is not there");
        softAssert.assertTrue(TestBase.isElementPresent("cna_tnc"), "cna_tnc is not there");


        TestBase.get_Screenshot("src\\test\\resource\\account\\C010_Verify_after_clicking_on_system_back_button_from_TermsAndConditions_user_navigates_CREATE_NEW_ACCOUNT_page/" + "screen1" + ".jpg");
        softAssert.assertAll();
    }

    @Test(priority = 11)
    public void C011_Verify_the_Autofill_Facebook_functionality_check_prior_Facebook_should_be_loggedIn() throws Exception {

        TestBase.getObject("autofill_wid_fb").click();

        softAssert.assertTrue(TestBase.isElementPresent("autofill_logout"), "autofill_logout is not there");
        /*Assert.assertFalse(TestBase.isElementPresent("autofill_wid_fb"), "autofill_wid_fb is there");
        Assert.assertFalse(TestBase.isElementPresent("cna_first_name"), "cna_first_name is there");
        Assert.assertFalse(TestBase.isElementPresent("cna_last_name"), "cna_last_name is there");
        Assert.assertFalse(TestBase.isElementPresent("cna_email"), "cna_email is there");
*/
        TestBase.get_Screenshot("src\\test\\resource\\account\\C011_Verify_the_Autofill_Facebook_functionality_check_prior_Facebook_should_be_loggedIn/" + "screen1" + ".jpg");
        softAssert.assertAll();

    }

    @Test(priority = 12)
    public void C012_After_Autofill_Facebook_click_on_SignUp_without_Filling_Password_DOB_and_validate_error_message() throws Exception {

        TestBase.getObject("cna_signup").click();
        Thread.sleep(1000);
        TestBase.get_Screenshot("src\\test\\resource\\account\\C012_After_Autofill_Facebook_click_on_SignUp_without_Filling_Password_DOB_and_validate_error_message/" + "screen1" + ".jpg");

        TestBase.getObject("cna_pwd").click();
        Thread.sleep(1000);
        TestBase.getScreenshotForParticularPart("autofill_tooltip_error_pwd", "src\\test\\resource\\account\\C012_After_Autofill_Facebook_click_on_SignUp_without_Filling_Password_DOB_and_validate_error_message/" + "autofill_tooltip_error_pwd" + ".jpg");
        String result = TestBase.TextFromImage("src\\test\\resource\\account\\C012_After_Autofill_Facebook_click_on_SignUp_without_Filling_Password_DOB_and_validate_error_message/" + "autofill_tooltip_error_pwd" + ".jpg");
        System.out.println(result);
        String exp = "Field cannot be empty";
        if(result.toLowerCase().contains(exp.toLowerCase())){
            System.out.println("passed");
        }else{
            softAssert.assertTrue(1>2, "The tool tip error message is not displayed correctly for incorrect password");
        }
        TestBase.get_Screenshot("src\\test\\resource\\account\\C012_After_Autofill_Facebook_click_on_SignUp_without_Filling_Password_DOB_and_validate_error_message/" + "screen2" + ".jpg");
        softAssert.assertAll();
    }

    @Test(priority = 13)
    public void C013_After_Autofill_Facebook_click_on_SignUp_without_Filling_DOB_and_validate_error_message() throws Exception {

        TestBase.getObject("cna_pwd").sendKeys("12345678");
        TestBase.driver.navigate().back();
        TestBase.getObject("cna_signup").click();
        Thread.sleep(1000);
        TestBase.get_Screenshot("src\\test\\resource\\account\\C013_After_Autofill_Facebook_click_on_SignUp_without_Filling_DOB_and_validate_error_message/" + "screen1" + ".jpg");
        TestBase.getObject("ok_btn").click();
        softAssert.assertAll();

    }

    @Test(priority = 14)
    public void C014_Verify_error_message_after_Autofill_Facebook_click_on_SignUp_by_filling_ExistingEmail_Password_DOB() throws Exception {

        //Click on DOB and set date
        //TestBase.driver.navigate().back();
        TestBase.driver.scrollTo("Sign up");

        TestBase.getObject("cna_dob").click();

       /* try {
            TestBase.getObject("select_DOB").click();
            TestBase.getObject("DOB_ok").click();
        }catch (Exception e){
            e.getStackTrace();
        }
*/
        try {
            TestBase.getObject("done").click();
        }catch (Exception e){
            e.getStackTrace();
        }


        TestBase.getObject("cna_signup").click();

        softAssert.assertTrue(TestBase.isElementPresent("failed_registration_msg1"), "failed registration msg is not there");
        softAssert.assertTrue(TestBase.isElementPresent("failed_registration_msg2"), "failed_registration_msg2 is not there");

        TestBase.get_Screenshot("src\\test\\resource\\account\\C014_After_Autofill_Facebook_click_on_SignUp_signUp_by_filling_Password_DOB/" + "screen1" + ".jpg");
        TestBase.getObject("DOB_ok").click();
        softAssert.assertTrue(TestBase.isElementPresent("autofill_logout"), "autofill_logout is not there");
        /*Assert.assertFalse(TestBase.isElementPresent("cna_dob"), "cna_dob is not there");*/
        TestBase.get_Screenshot("src\\test\\resource\\account\\C014_After_Autofill_Facebook_click_on_SignUp_signUp_by_filling_Password_DOB/" + "screen2" + ".jpg");
        softAssert.assertAll();

    }

    @Test(priority = 15)
    public void C015_Verify_logout_feature_after_Autofill_Facebook_login() throws Exception {

        TestBase.getObject("autofill_logout").click();
        softAssert.assertTrue(TestBase.isElementPresent("autofill_logout_msg"), "autofill_logout is not there");
        TestBase.getObject("autofill_cancel").click();
        softAssert.assertTrue(TestBase.isElementPresent("autofill_logout"), "autofill_logout is not there");
        TestBase.get_Screenshot("src\\test\\resource\\account\\C015_Verify_logout_feature_after_Autofill_Facebook_login/" + "screen1" + ".jpg");

        TestBase.getObject("autofill_logout").click();
        TestBase.getObject("autofill_fb_logout").click();
        Thread.sleep(4000);
        TestBase.driver.scrollTo("Let's setup your mapSYNQ account");
        softAssert.assertTrue(TestBase.isElementPresent("mapsynq_setup_msg"), "Let's setup your mapSYNQ account is not there");
        softAssert.assertTrue(TestBase.isElementPresent("autofill_wid_fb"), "autofill_wid_fb is not there");
            /*Assert.assertFalse(TestBase.isElementPresent("cna_first_name"), "cna_first_name is not there");
            Assert.assertFalse(TestBase.isElementPresent("cna_last_name"), "cna_last_name is not there");
            Assert.assertFalse(TestBase.isElementPresent("cna_email"), "cna_email is not there");
            Assert.assertFalse(TestBase.isElementPresent("cna_dob"), "cna_dob is not there");
            */
        softAssert.assertTrue(TestBase.isElementPresent("cna_male"), "cna_male is not there");

        TestBase.get_Screenshot("src\\test\\resource\\account\\C015_Verify_logout_feature_after_Autofill_Facebook_login/" + "screen2" + ".jpg");
        softAssert.assertAll();
    }

    @Test(priority = 16)
    public void C016_Verify_Account_screen_isDisplayed_when_clicked_on_back_btn_from_Create_New_Account() throws Exception {

        TestBase.getObject("back_btn_act").click();
        softAssert.assertTrue(TestBase.isElementPresent("account_text1"), "Account label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("log_in_to_your_mapSYNQ_acct"), "log_in_to_your_mapSYNQ_acct label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("email_label"), "email_label label is not there");

        TestBase.get_Screenshot("src\\test\\resource\\account\\C016_Verify_Account_screen_isDisplayed_when_clicked_on_back_btn_from_Create_New_Account/" + "screen1" + ".jpg");
        softAssert.assertAll();


    }

    @Test(priority = 17)
    public void C017_Verify_error_message_for_blank_Login_fields() throws Exception {


        TestBase.getObject("log_in_btn").click();

        TestBase.getObject("email_field").click();
        TestBase.driver.navigate().back();
        Thread.sleep(1000);

        //Tesseract
        //Tool Tip verification(ACCOUNT)
        /*TestBase.getScreenshotForParticularPart("blank_login_pwd_error", "src\\test\\resource\\account\\C017_Verify_error_message_for_blank_Login_fields/" + "blank_login_error" + ".png");
        String result = TestBase.TextFromImage("src\\test\\resource\\account\\C017_Verify_error_message_for_blank_Login_fields/" + "blank_login_pwd_error" + ".jpg");
        System.out.println(result);
        String exp = "Field cannot be empty";
        if(result.toLowerCase().contains(exp.toLowerCase())){
            System.out.println("passed");
        }else{
            Assert.assertTrue(1>2, "The tool tip error message is not displayed correctly for incorrect password");
        }*/
        TestBase.get_Screenshot("src\\test\\resource\\account\\C016_Verify_error_message_for_blank_Login_fields/" + "screen1" + ".jpg");

        //Tesseract
        //Tool Tip verification (PASSWORD)
        TestBase.getObject("pwd_field").click();
        Thread.sleep(1000);
        TestBase.driver.navigate().back();
        Thread.sleep(1000);


        /*TestBase.getScreenshotForParticularPart("blank_login_pwd_error", "src\\test\\resource\\account\\C017_Verify_error_message_for_blank_Login_fields/" + "blank_login_pwd_error" + ".png");
        String result1 = TestBase.TextFromImage("src\\test\\resource\\account\\C017_Verify_error_message_for_blank_Login_fields/" + "blank_login_pwd_error" + ".jpg");
        System.out.println(result1);
        String exp1 = "Field cannot be empty";
        if(result1.toLowerCase().contains(exp1.toLowerCase())){
            System.out.println("passed");
        }else{
            Assert.assertTrue(1>2, "The tool tip error message is not displayed correctly for incorrect password");
        }
         }*/

        TestBase.get_Screenshot("src\\test\\resource\\account\\C017_Verify_error_message_for_blank_Login_fields/" + "screen2" + ".jpg");
        softAssert.assertAll();

    }

    @Test(priority = 18)
    public void C018_Verify_error_message_for_blank_Password() throws Exception {

        //OPtion > ACCOUNT
        TestBase.getObject("Option_Button").click();
        TestBase.getObject("Verify_Account_Button").click();

        TestBase.getObject("email_field").sendKeys(TestBase.TestData.getProperty("valid_email"));

        TestBase.getObject("pwd_field").click();

        TestBase.driver.navigate().back();
        TestBase.getObject("log_in_btn").click();
        TestBase.getScreenshotForParticularPart("blank_login_pwd_error", "src\\test\\resource\\account\\C018_Verify_error_message_for_blank_Password/" + "blank_login_pwd_error" + ".jpg");
        String result1 = TestBase.TextFromImage("src\\test\\resource\\account\\C018_Verify_error_message_for_blank_Password/" + "blank_login_pwd_error" + ".jpg");
        System.out.println(result1);
        String exp1 = "Field cannot be empty";
        if(result1.toLowerCase().contains(exp1.toLowerCase())){
            System.out.println("passed");
        }else{
            softAssert.assertTrue(1>2, "The tool tip error message is not displayed correctly for incorrect password");
        }
        TestBase.get_Screenshot("src\\test\\resource\\account\\C018_Verify_error_message_for_blank_Password/" + "screen2" + ".jpg");
        softAssert.assertAll();

    }

    @Test(priority = 19)
    public void C019_Verify_error_message_for_valid_Email_and_invalid_Pwd() throws Exception {

        TestBase.getObject("pwd_field").sendKeys(TestBase.TestData.getProperty("invalid_pwd"));

        TestBase.driver.navigate().back();
        TestBase.getObject("log_in_btn").click();
        //softAssert.assertTrue(TestBase.isElementPresent("invalid_pwd_error_header"), "invalid_pwd_error_header is not there");
        //softAssert.assertTrue(TestBase.isElementPresent("invalid_pwd_msg"), "invalid_pwd_msg is not there");
        //softAssert.assertTrue(TestBase.isElementPresent("invalid_pwd_ok"), "invalid_pwd_ok is not there");
        TestBase.get_Screenshot("src\\test\\resource\\account\\C019_Verify_error_message_for_valid_Email_and_invalid_Pwd/" + "screen1" + ".jpg");
        //TestBase.getObject("invalid_pwd_ok").click();
        softAssert.assertAll();

    }

    @Test(priority = 20)
    public void C020_Verify_error_message_for_invalid_Email_invalid_Pwd() throws Exception {

        TestBase.getObject("email_field").clear();
        TestBase.getObject("email_label").sendKeys(TestBase.TestData.getProperty("invalid_email"));

        TestBase.getObject("pwd_field").clear();
        TestBase.getObject("pwd_field").sendKeys(TestBase.TestData.getProperty("CNA_invalid_pwd"));

        TestBase.driver.navigate().back();
        TestBase.getObject("log_in_btn").click();
        TestBase.getObject("email_field").click();
        Thread.sleep(6000);
        TestBase.getScreenshotForParticularPart("invaid_email_error", "src\\test\\resource\\account\\C020_Verify_error_message_for_invalid_Email_invalid_Pwd/" + "invaid_email_error" + ".jpg");
        String result1 = TestBase.TextFromImage("src\\test\\resource\\account\\C020_Verify_error_message_for_invalid_Email_invalid_Pwd/" + "invaid_email_error" + ".jpg");
        System.out.println(result1);
        String exp1 = "Please enter a valid email address";
        if(result1.toLowerCase().contains(exp1.toLowerCase())){
            System.out.println("passed");
        }else{
            softAssert.assertTrue(1>2, "The tool tip error message is not displayed correctly for incorrect password");
        }
        TestBase.get_Screenshot("src\\test\\resource\\account\\C020_Verify_error_message_for_invalid_Email_invalid_Pwd/" + "screen1" + ".jpg");
        softAssert.assertAll();

    }

    @Test(priority = 21)
    public void C021_Verify_successfull_Account_Log_In() throws Exception {

        TestBase.getObject("email_field").clear();
        TestBase.getObject("email_label").sendKeys(TestBase.TestData.getProperty("Email"));

        TestBase.getObject("pwd_field").clear();
        TestBase.getObject("pwd_field").sendKeys(TestBase.TestData.getProperty("Password"));

        TestBase.driver.navigate().back();
        TestBase.getObject("log_in_btn").click();

        softAssert.assertTrue(TestBase.isElementPresent("Account_display_after_login"), "Account_display_after_login is not there");

        if(TestBase.getObject("after_login_email_id_display").getText().contains(TestBase.TestData.getProperty("Email"))){
            System.out.println("passed");
        }else{
            softAssert.assertTrue(1>2, "wrong mail id displayed");
        }

        softAssert.assertTrue(TestBase.isElementPresent("logout_btn"), "logout_btn is not there");
        softAssert.assertTrue(TestBase.isElementPresent("sync_data_label"), "sync_data_label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("enable_sync_msg"), "sync_data_label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("last_synced_on_text"), "last_synced_on_text is not there");
        softAssert.assertTrue(TestBase.isElementPresent("registered_cars"), "registered_cars is not there");
        softAssert.assertTrue(TestBase.isElementPresent("list_of_cars"), "list_of_cars is not there");

        if(TestBase.getObject("sync_switch").getAttribute("checked").equals("true")){
            System.out.println("passed");
        }else{
            Assert.assertTrue(1>2, "unchecked");
        }


        TestBase.get_Screenshot("src\\test\\resource\\account\\C021_Verify_successfull_Account_Log_In/" + "screen1" + ".jpg");
        softAssert.assertAll();

    }

    @Test(priority = 22)
    public void C022_Verify_logout_feature_in_Account_Screen() throws Exception {

        TestBase.getObject("sync_switch").click();

        if(TestBase.getObject("sync_switch").getAttribute("checked").equals("false")){
            System.out.println("passed");
        }else{
            softAssert.assertTrue(1>2, "unchecked");
        }

        TestBase.getObject("Logout_Text").click();

        softAssert.assertTrue(TestBase.isElementPresent("Verify_Clear_your_Favourites_Text"), "Logout pop up box is not present in Account_Page screen");
        TestBase.getObject("Verify_Clear_your_Favourites_Text").click();
        TestBase.getObject("Logout_button").click();

        softAssert.assertTrue(TestBase.isElementPresent("account_text1"), "Account label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("log_in_to_your_mapSYNQ_acct"), "log_in_to_your_mapSYNQ_acct label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("email_label"), "email_label label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("pwd_field"), "pwd_field label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("log_in_btn"), "log_in_btn label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("fyp"), "forget your pwd label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("new_to_galactio"), "new to galactio? label is not there");
        softAssert.assertTrue(TestBase.isElementPresent("create_new_acct"), "create new acct label is not there");

        TestBase.get_Screenshot("src\\test\\resource\\account\\C022_Verify_logout_feature_in_Account_Screen/" + "screen1" + ".jpg");
        softAssert.assertAll();
    }

    @AfterTest
    public void quit() throws InterruptedException {

        if (TestBase.driver != null)
            Thread.sleep(4000);
        TestBase.driver.quit();
        System.out.println("exit");
    }


}
